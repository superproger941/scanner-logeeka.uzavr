<?php

use yii\db\Migration;

/**
 * Class m201110_114650_add_likes_and_comments_no_parse_flags
 */
class m201110_114650_add_likes_and_comments_no_parse_flags extends Migration
{
    public function safeUp()
    {
        $this->addColumn('posts', 'likes_parse', $this
            ->boolean()
            ->defaultValue(false)
        );
        $this->addColumn('posts', 'comments_parse', $this
            ->boolean()
            ->defaultValue(false)
        );
    }

    public function safeDown()
    {
        $this->dropColumn('posts', 'likes_parse');
        $this->dropColumn('posts', 'comments_parse');
    }
}
