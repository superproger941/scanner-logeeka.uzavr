<?php

use yii\db\Migration;

/**
 * Class m201111_040419_add_users_no_parse_flag_to_post_comment_likes
 */
class m201111_040419_add_users_no_parse_flag_to_post_comment_likes extends Migration
{
    public function safeUp()
    {
        $this->addColumn('post_comment_likes', 'user_parse', $this
            ->boolean()
            ->defaultValue(false)
        );
    }

    public function safeDown()
    {
        $this->dropColumn('post_comment_likes', 'user_parse');
    }
}
