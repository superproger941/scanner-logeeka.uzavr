<?php

use yii\db\Migration;

/**
 * Class m201119_120747_add_likes_comments_no_parse
 */
class m201119_120747_add_likes_comments_no_parse extends Migration
{
    public function safeUp()
    {
        $this->addColumn('posts_comments', 'comment_likes_parse',
            $this->boolean()->defaultValue(false)
        );
        $this->addColumn('posts_comments', 'comment_comment_parse',
            $this->boolean()->defaultValue(false)
        );
        $this->addColumn('posts_comments', 'user_parse',
            $this->boolean()->defaultValue(false)
        );
    }

    public function safeDown()
    {
        $this->dropColumn('posts_comments', 'comment_likes_parse');
        $this->dropColumn('posts_comments', 'comment_comment_parse');
        $this->dropColumn('posts_comments', 'user_parse');
    }
}
