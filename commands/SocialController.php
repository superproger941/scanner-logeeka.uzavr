<?php

namespace app\commands;

use yii\console\Controller;
use yii\helpers\Console;

abstract class SocialController extends Controller
{
    public $offset;
    public $count;

    public function options($actionID)
    {
        return ['offset', 'count'];
    }

    public function optionAliases()
    {
        return ['o' => 'offset', 'c' => 'count'];
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        $this->stdout("\nFinish\n", Console::BOLD, Console::FG_GREEN);

        return $result;
    }
}