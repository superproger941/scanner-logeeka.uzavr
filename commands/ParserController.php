<?php

namespace app\commands;

use app\components\ExcelParser;
use app\components\ParserComponent;
use app\components\SaveVkInfoToDb;
use app\models\data\OkGroups;
use app\models\data\PostCommentLikes;
use app\models\data\PostComments;
use app\models\data\Posts;
use app\models\data\VkGroups;
use app\models\data\VkUsers;
use SocialApiLibrary\api\SocialApi;
use SocialApiLibrary\SocialFactory;
use yii\console\Controller;
use League\Csv\Writer;

class ParserController extends Controller
{
    /**
     * @var ParserComponent
     */
    private $parser;

    /**
     * @var ExcelParser
     */
    private $excel;

    /**
     * @var SaveVkInfoToDb
     */
    private $vkInfoDbSaver;

    /**
     * @var
     */
    private $okSocial;

    /**
     * @var
     */
    private $vkSocial;

    public function init()
    {
        parent::init();
        $token = '1e9ed4fa6d80b46acde7b120a9e192566048a4521e769dfd6dfca002bb9727c131bfb1275b4c9dfca2aa7';
        $this->parser = new ParserComponent($token);
        $this->excel = new ExcelParser($token);
        $this->vkInfoDbSaver = new SaveVkInfoToDb();

        $this->okSocial = SocialFactory::factory(SocialApi::ID_OK, [
            'id' => 3,
            'app_id' => '512000051505',
            'app_public_key' => 'CCDKMFJGDIHBABABA',
            'app_secret_key' => 'CF213F6D9C94D89966E39558',
            'access_token' => 'tkn1YBPEYmBf2HJ7RKvoQgDkepMHovgEUcY3PgOORZbGQwgl9ec73ihykcwCduWFvLEZH',
            'refresh_token' => '0201875532046c5186f8e24a640667b0',
        ]);

        $this->vkSocial = SocialFactory::factory(
            SocialApi::ID_VK,
            [
                'offset' => 0,
                'access_token' => '1e9ed4fa6d80b46acde7b120a9e192566048a4521e769dfd6dfca002bb9727c131bfb1275b4c9dfca2aa7',
            ]
        );
    }

    public function actionParseExcelVk(string $filepath)
    {
        $generator = $this->excel->parseAndGetFunctionsWithData($filepath);
        foreach ($generator as $value) {
            $funcStrIndex = $value[0];
            $data = $value[1];

            if (!$data) {
                echo 'is podcast';
                continue;
            }

            $this->vkInfoDbSaver->selectFunction($funcStrIndex, $data);
        }
    }

    public function actionGetPostUnparsedLikesVk()
    {
        $posts = Posts::getUnparsedLikesPosts(SocialApi::ID_VK);

        foreach ($posts as $post) {
            $likes = $this->parser->getUnparsedPostLikesVk($post->owner_id, $post->post_id);
            PostCommentLikes::saveLikes($likes['result']['items'], $post->owner_id, $post->post_id);
            var_dump($likes);
            $post->setLikesParsed();
            sleep(1);
        }
    }

    public function actionGetUnparsedCommentsPostsVk()
    {
        $posts = Posts::getUnparsedCommentsPosts(SocialApi::ID_VK);
        foreach ($posts as $post) {
            $comments = $this->parser->getUnparsedPostCommentsVk($post->owner_id, $post->post_id);
            PostComments::saveComments($comments['result']['items'], $post->owner_id, $post->post_id);

            $post->setCommentsParsed();
            sleep(1);
        }
    }

    public function actionGetUnparsedCommentsLikesVk()
    {
        $posts = PostComments::getUncheckedCommentsWithLikes(SocialApi::ID_VK);

        foreach ($posts as $post) {
            $likes = $this->parser->getUnparsedPostCommentsLikesVk(
                $post->owner_id,
                $post->post_id,
                $post->comment_id
            );

            PostCommentLikes::saveLikes(
                $likes['result']['items'],
                $post->owner_id,
                $post->post_id,
                $post->comment_id,
                SocialApi::LIKES_TYPE_COMMENT
            );

            $post->setParsedCommentsLikes();
            sleep(1);
        }
    }

    public function actionGetUnparsedCommentsCommentsVk()
    {
        $comments = PostComments::getUnparsedCommentsComments(SocialApi::ID_VK);
        foreach ($comments as $comment) {
            $comments = $this->parser->getUnparsedPostCommentsLikesVk($comment->owner_id, $comment->post_id, $comment->comment_likes_parse);
            PostComments::saveComments($comments['result']['items'], $comment->owner_id, $comment->post_id);

            var_dump($comment->setCommentsCommentsParsed());
            var_dump($comment->getErrors());

            sleep(1);
        }
    }

    public function actionGetUnparsedLikesUsersVk()
    {
        $offset = 0;
        $limit = 100;
        while (1) {
            $users = PostCommentLikes::getUnparsedLikesUsers(
                SocialApi::ID_VK,
                true,
                ['offset' => $offset, 'limit' => $limit]
            );
            if (empty($users)) {
                break;
            }
            $users = array_column($users, 'from_user_id');
            $vkUsers = $this->parser->getUnparsedLikesUsers($users);
            VkUsers::saveUsers($vkUsers['result']);
            PostCommentLikes::setUsersParsed($users);
            $offset += 100;
            $limit += 100;
        }
    }

    public function actionGetUnparsedCommentsUsersVk()
    {
        $users = PostComments::getUnparsedCommentsUsers(
            SocialApi::ID_VK
        );
        $users = array_column($users, 'from_user_id');
        $vkUsers = $this->parser->getUnparsedLikesUsers($users);
        VkUsers::saveUsers($vkUsers['result']);
        PostCommentLikes::setUsersParsed($users);
    }

    public function actionGetCsvFromDb()
    {
        $posts = Posts::find()->asArray()->all();
        $csv = Writer::createFromPath(__DIR__ . '/../posts-csv.csv', 'w+');
        $csv->insertAll($posts);
    }

    public function actionGetVkGroupsUsers()
    {
        $groups = VkGroups::find()->where(['watch' => 0])->all();
        foreach ($groups as $group) {
            $groupsUsers = $this->vkSocial->getGroupUsers(
                ['group_id' => $group->vk_id]
            );
            $vkUsers = $this->parser->getUnparsedLikesUsers($groupsUsers['result']['items']);
            VkUsers::saveUsers($vkUsers['result']);
        }
    }

    public function actionGetOkGroupsUsers()
    {
        $groups = OkGroups::find()->where(['watch' => 0])->all();
        foreach ($groups as $group) {
            $groupsUsers = $this->okSocial->getGroupUsers(
                ['group_id' => $group->vk_id]
            );
            var_dump($groupsUsers);
        }
    }
}
