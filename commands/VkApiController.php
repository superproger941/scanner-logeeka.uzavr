<?php

namespace app\commands;

use SocialApiLibrary\api\SocialApi;
use SocialApiLibrary\SocialFactory;
use yii\console\ExitCode;

class VkApiController extends SocialController
{
    private $vkSocial;

    public function init()
    {
        parent::init();
        $this->vkSocial = SocialFactory::factory(
            SocialApi::ID_VK,
            [
                'offset' => $this->offset,
                'access_token' => '1e9ed4fa6d80b46acde7b120a9e192566048a4521e769dfd6dfca002bb9727c131bfb1275b4c9dfca2aa7',
            ]
        );
        $this->vkSocial->setOffsetAndCount($this->offset, $this->count);
    }

    public function actionGetGroupUsers(int $groupId)
    {
        $groupsUsers = $this->vkSocial->getGroupUsers(['group_id' => $groupId]);
        var_dump($groupsUsers);
        return $groupsUsers;
    }

    public function actionGetUserInfo(int $userId)
    {
        $users = $this->vkSocial->getUserInfo(['user_ids' => [$userId]]);
        var_dump($users);
        return $users;
    }

    public function actionGetUserGroups(int $userId)
    {
        $users = $this->vkSocial->getUserGroups(['user_id' => $userId]);
        var_dump($users);
        return $users;
    }

    public function actionGetUserFriends(int $userId)
    {
        $users = $this->vkSocial->getUserFriends(['user_id' => $userId]);
        var_dump($users);
        return $users;
    }

    public function actionGetUserFollowers(int $userId)
    {
        $users = $this->vkSocial->getUserFollowers(['user_id' => $userId]);
        var_dump($users);
        return $users;
    }

    public function actionGetGroupPosts(int $groupId)
    {
        $groupsUsers = $this->vkSocial->getGroupPosts([
            'owner_id' => '-' . $groupId,
        ]);
        var_dump($groupsUsers);
        return $groupsUsers;
    }

    public function actionGetGroupPostById(int $groupId, int $postId)
    {
        $posts = $this->vkSocial->getGroupPostsById([
            'posts' => '-' . $groupId . '_' . $postId,
        ]);
        var_dump($posts);
        return $posts;
    }

    public function actionGetPostLikes(int $groupId, int $postId)
    {
        $likes = $this->vkSocial->getPostLikes([
            'owner_id' => '-' . $groupId,
            'post_id' => $postId,
        ]);
        var_dump($likes);
        return $likes;
    }

    public function actionGetPostComments(int $groupId, int $postId)
    {
        $comments = $this->vkSocial->getPostComments([
            'owner_id' => '-' . $groupId,
            'post_id' => $postId,
        ]);
        var_dump($comments);
        return $comments;
    }

    public function actionGetPostAttachments(int $groupId, int $postId)
    {
        $attachments = $this->vkSocial->getPostAttachments([
            'owner_id' => '-' . $groupId,
            'post_id' => $postId,
        ]);
        var_dump($attachments);
        return $attachments;
    }

    public function actionGetAlbums(int $ownerId)
    {
        $albums = $this->vkSocial->getAlbums([
            'owner_id' => '-' . $ownerId,
        ]);
        var_dump($albums);
        return $albums;
    }

    public function actionGetPhotosByAlbumId(int $ownerId, int $albumId)
    {
        $photos = $this->vkSocial->getPhotos([
            'owner_id' => '-' . $ownerId,
            'album_id' => $albumId,
        ]);
        var_dump($photos);
        return $photos;
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        return $result['code'] === 200 ? ExitCode::OK : ExitCode::UNSPECIFIED_ERROR;
    }
}
