<?php /** @noinspection PhpUnused */

namespace app\commands;

use app\models\data\InternetUsersStatistics;
use app\models\data\Options;
use app\components\socialapi\SocialFactory;
use app\components\vkapi\VkontakteSDK;
use app\helpers\HelperFunctions;
use app\models\data\Galleries;
use app\models\data\InternetUser;
use yii\console\ExitCode;
use yii\console\Controller;
use Yii;
use yii\db\Query;
use app\models\data\Events;
use yii\helpers\Console;
use yii\db\Expression;

class VkController extends Controller
{
    public $processCount = 5;
    private $startTime = 0;

    private function execTime($action, $arArgs = [])
    {
        if (isset($arArgs) && is_array($arArgs) && array_search("exectime", $arArgs) !== false) {
            if ($action == 'start') {
                $this->startTime = microtime(true);
            } elseif ($action = 'end') {
                $etime = gmdate("H:i:s", microtime(true) - $this->startTime);
                $this->stdout("Execution time: " . $etime . PHP_EOL, Console::FG_GREEN);
            }
        }
    }

    /**
     * Only for test
     */
    public function actionIndex()
    {
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        $result = Yii::$app->db->createCommand("SELECT vk_users.vk_id FROM vk_users LEFT JOIN internet_users ON (internet_users.soc_network_id = 1 AND internet_users.soc_network_user_id = vk_users.vk_id) WHERE soc_network_user_id IS NULL ORDER BY vk_users.id LIMIT 10 OFFSET 405000")->queryAll();

        $res = [];

        foreach ($result as $item) {
            $res[] = $item['vk_id'];
        }

        print_r($res);

        foreach ($res as $user) {
            Yii::$app->db->createCommand("INSERT INTO temp_spec_internet_users (soc_network_id,soc_network_user_id) VALUES (1,{$user})")->execute();
        }

        $this->execTime('end', $arArgs);

        return ExitCode::OK;
    }

    public function actionGetInternetUsersSpec()
    {
        exec("ps aux|grep 'vk/get-internet-users-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/

        $itemsPerPage = 100;
        $arUsers = [];
        $index = 0;
        $groupsChunkSize = 1000;
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;

        /*Получаем настройку Оффсета*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'vk_get_spec_internet_users_index'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $index = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'vk_get_spec_internet_users_index';
            $newOption->option_value = '0';
            $newOption->save();
        }

        $makeQuery = (new Query())->select([
            '[[id]]',
            '[[soc_network_id]]',
            '[[soc_network_user_id]]'
        ])
            ->from('temp_spec_internet_users')
            ->where(['>', '[[id]]', $index])
            ->andWhere(['[[soc_network_id]]' => $socNetworkId])
            ->limit($itemsPerPage)
            ->orderBy('id')
            ->all();

        if (is_array($makeQuery) && count($makeQuery)) {
            foreach ($makeQuery as $oneUser) {
                $arUsers[$oneUser['soc_network_id']][] = $oneUser['soc_network_user_id'];
                $index = $oneUser['id'];
            }
        }

        Options::updateAll([
            '[[option_value]]' => $index
        ], [
            '[[option_name]]' => 'vk_get_spec_internet_users_index'
        ]);

        /*DEBUG*/
        if ($debug) {
            echo "arUsers\n";
            print_r($arUsers);
        }
        /*END DEBUG*/

        if (count($arUsers)) {
            $exp = new Expression($socNetworkId.' as snid');
            foreach ($arUsers as $socNetworkType => $socNetworkUsers) {
                try {
                    $successAddedIds = SocialFactory::factory($socNetworkType)->getInternetUsers($socNetworkUsers);

                    /*DEBUG*/
                    if ($debug) {
                        echo "successAddedIds\n";
                        print_r($successAddedIds);
                    }
                    /*END DEBUG*/

                    if (count($successAddedIds)) {
                        foreach ($successAddedIds as $oneUser => $internetUserId) {
                            (new Query())->createCommand()
                                ->update(
                                    '{{%temp_spec_internet_users}}',
                                    ['[[internet_user_id]]' => $internetUserId],
                                    [
                                        '[[soc_network_id]]' => $socNetworkType,
                                        '[[soc_network_user_id]]' => $oneUser
                                    ]
                                )->execute();

                            $groupsQuery = (new Query())->select([
                                    $exp,
                                    'vkg.[[vk_id]]',
                                    'vkg.[[id]]'
                                ])
                                ->from(['vkug' => 'vk_users_groups'])
                                ->innerJoin(['vkg' => 'vk_groups'], 'vkg.[[vk_id]] = vkug.[[vk_groups_id]]')
                                ->where(['vkug.[[vk_users_id]]' => $oneUser])
                                ->all();

                            $groupsQuery = array_chunk($groupsQuery, $groupsChunkSize);
                            foreach ($groupsQuery as $groupQuery) {
                                $insertArray = [];
                                $checkArray = [];
                                foreach ($groupQuery as $oneGroup) {
                                    $insertArray[$oneGroup['vk_id']] = '('.implode(',', $oneGroup).')';
                                    $checkArray[] = $oneGroup['vk_id'];
                                }
                                $checkIfExists = Yii::$app->db->createCommand("SELECT group_id FROM temp_spec_groups WHERE (soc_network_id = {$socNetworkId}) AND (group_id IN (".implode(',', $checkArray)."))")->queryAll();

                                /*DEBUG*/
                                if ($debug) {
                                    echo "PRESENT GROUPS".PHP_EOL;
                                    print_r($checkIfExists);
                                }
                                /*END DEBUG*/

                                foreach ($checkIfExists as $oneExistItem) {
                                    unset ($insertArray[$oneExistItem['group_id']]);
                                }
                                if (count($insertArray)) {
                                    Yii::$app->db->createCommand("INSERT INTO temp_spec_groups (soc_network_id,group_id,group_table_id) VALUES ".implode(',', $insertArray))->execute();
                                }
                            }
                        }
                    }
                } catch (\InvalidArgumentException $error) {
                    /*DEBUG*/
                    if ($debug) {
                        echo $error->getMessage()."\n";
                    }
                    /*END DEBUG*/
                }
            }
        }

        $this->execTime('end', $arArgs);

        return ExitCode::OK;
    }

    private function loadVkAttachment(\stdClass $item, int $item_id, int $owner_id, int $comment_id = null)
    {
        if (property_exists($item, 'attachments') && $item_id && $owner_id) {
            $attachements = $item->attachments;
            foreach ($attachements as $oneAttach) {
                $tempAttach = [];
                $tempAttach['post_id'] = $item_id;
                if ($comment_id)
                    $tempAttach['comment_id'] = $comment_id;
                $tempAttach['soc_network_id'] = 1;
                if (property_exists($oneAttach, 'type'))
                    $tempAttach['type'] = $oneAttach->type;

                if (is_object($oneAttach->{$tempAttach['type']})) {

                    if (property_exists($oneAttach->{$tempAttach['type']}, 'id'))
                        $tempAttach['attachment_id'] = $oneAttach->{$tempAttach['type']}->id;
                    if (property_exists($oneAttach->{$tempAttach['type']}, 'owner_id'))
                        $tempAttach['owner_id'] = $oneAttach->{$tempAttach['type']}->owner_id;
                    if (property_exists($oneAttach->{$tempAttach['type']}, 'title'))
                        $tempAttach['title'] = HelperFunctions::cleanString($oneAttach->{$tempAttach['type']}->title);
                    if (property_exists($oneAttach->{$tempAttach['type']}, 'date'))
                        $tempAttach['date'] = $oneAttach->{$tempAttach['type']}->date;
                    if (property_exists($oneAttach->{$tempAttach['type']}, 'access_key'))
                        $tempAttach['access_key'] = $oneAttach->{$tempAttach['type']}->access_key;

                    switch ($tempAttach['type']) {
                        case 'photo':
                            $tmpWidth = 0;
                            foreach ($oneAttach->{$tempAttach['type']}->sizes as $oneSize) {
                                if ($oneSize->width > $tmpWidth)
                                    $tempAttach['url'] = $oneSize->url;
                            }
                            break;
                        case 'poll':
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'question'))
                                $tempAttach['title'] = HelperFunctions::cleanString($oneAttach->{$tempAttach['type']}->question);
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'created'))
                                $tempAttach['date'] = $oneAttach->{$tempAttach['type']}->created;
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'answers'))
                                $tempAttach['attachment_content'] = json_encode($oneAttach->{$tempAttach['type']}->answers);
                            break;
                        case 'video':
                            //sleep(1);
                            $resultVideo = VkontakteSDK::makeQuery('video.get', [
                                'owner_id' => $tempAttach['owner_id'],
                                'videos' => $tempAttach['owner_id'] . '_' . $tempAttach['attachment_id'] . '_' . $tempAttach['access_key'],
                                'count' => 1,
                                'offset' => 0,
                            ]);
                            //usleep(400000);
                            if (is_object($resultVideo) && property_exists($resultVideo, 'items') && count($resultVideo->items)) {
                                $tempAttach['url'] = $resultVideo->items[0]->player;
                            }
                            break;
                        case 'sticker':
                            if (!property_exists($oneAttach->{$tempAttach['type']}, 'owner_id'))
                                $tempAttach['owner_id'] = $owner_id;
                            $tmpWidth = 0;
                            foreach ($oneAttach->{$tempAttach['type']}->images as $oneImage) {
                                if ($oneImage->width > $tmpWidth)
                                    $tempAttach['url'] = $oneImage->url;
                            }
                            break;
                        case 'audio':
                        case 'doc':
                        case 'link':
                            if (!property_exists($oneAttach->{$tempAttach['type']}, 'owner_id'))
                                $tempAttach['owner_id'] = $owner_id;
                            $tempAttach['url'] = $oneAttach->{$tempAttach['type']}->url;
                            break;
                        case 'page':
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'group_id'))
                                $tempAttach['owner_id'] = "-".$oneAttach->{$tempAttach['type']}->group_id;
                            else
                                $tempAttach['owner_id'] = $owner_id;
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'view_url'))
                                $tempAttach['url'] = $oneAttach->{$tempAttach['type']}->view_url;
                            break;
                        default:
                            if (property_exists($oneAttach->{$tempAttach['type']}, 'url'))
                                $tempAttach['url'] = $oneAttach->{$tempAttach['type']}->url;
                    }

                    if (!isset($tempAttach['attachment_id']) || empty($tempAttach['attachment_id'])) {
                        if (!isset($tempAttach['url']))
                            $tempAttach['url'] = "";
                        $tempAttach['attachment_id'] = HelperFunctions::numHash($owner_id.$item_id.$tempAttach['type'].$tempAttach['url'], 9);
                    }

                    Yii::$app->db->CreateCommand()->upsert('post_attachments', $tempAttach, false)->execute();
                }
            }
            return ExitCode::OK;
        } else {
            return false;
        }
    }

    /**
     * Get posts from special internet users groups
     */
    public function actionGetPostsFromVkGroupsSpec()
    {
        exec("ps aux|grep 'vk/get-posts-from-vk-groups-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
        /*Скрипт работает в 2х режимах, 1. прохождение ленты до конца с попыткой забрать все посты; 2. обновление ленты только новыми
        Режимы управляются флагом update_wall_mode*/
        $arGroups = $arGroupsPostsOffset = $arGroupsPostsCount = $arGroupsUpdateWall = [];
        $arCache = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;


        /*нужно получить все ИД групп, с которых наши спец польхователи*/
        $makeQuery = Yii::$app->db->createCommand("SELECT vk_groups.id, vk_groups.vk_id, vk_groups.posts, vk_groups.update_wall_mode FROM temp_spec_groups INNER JOIN vk_groups ON (vk_groups.id = temp_spec_groups.group_table_id) WHERE soc_network_id = {$socNetworkId} ORDER BY vk_groups.id")->queryAll();

        foreach ($makeQuery as $oneGroup) {
            $arGroups[] = $oneGroup['vk_id'];
            $arGroupsPostsCount[$oneGroup['vk_id']] = !empty($oneGroup['posts'])?$oneGroup['posts']:0;
            $arGroupsUpdateWall[$oneGroup['vk_id']] = intval($oneGroup['update_wall_mode'])?1:0;
        }

        /*Получаем настройку Оффсета*/
        $oneGroup = reset($arGroups);
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_get_posts_from_vk_groups_spec_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $oneGroup = $makeQuery['option_value'];
        } elseif (!isset($makeQuery['option_value'])) {
            Yii::$app->db->createCommand()->insert('options', [
                'option_name' => 'vk_get_posts_from_vk_groups_spec_offset',
                'option_value' => reset($arGroups)
            ])->execute();
        }

        /*Обновляем оффсет*/
        $currentGroupKey = array_search($oneGroup, $arGroups);
        if ($currentGroupKey == (count($arGroups)-1)) {
            $oneGroup = reset($arGroups);
        } else {
            $oneGroup = $arGroups[$currentGroupKey+1];
        }
        Yii::$app->db->createCommand()->update('options', ['option_value' => $oneGroup], ['option_name' => 'vk_get_posts_from_vk_groups_spec_offset'])->execute();

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        if($debug) {
            $postsGet = $postsGetParent = 0;
            $issetPosts = $newPosts = $issetParentPosts = $newParentPosts = [];
        }
        /*END DEBUG*/

        $itemsPerPage = 100; //максимальное значение для данного метода!!!!
        $offsetLimit = 200;
        //время в будущее для того чтобы скрипты забора комментов и лайков знали какие посты акруальны и какие обновлять
        $upTime = HelperFunctions::utimeAdd(time(), 0, 0, 0, 0, 30, 0);

        if ($oneGroup) {

            $makeQuery = Yii::$app->db->createCommand("SELECT posts_offset FROM vk_groups WHERE vk_id = ". $oneGroup)->queryOne();
            $arGroupsPostsOffset = !empty($makeQuery['posts_offset'])?$makeQuery['posts_offset']:0;

            $postsCount = $arGroupsPostsCount[$oneGroup];
            $offset = $arGroupsPostsOffset;
            $checkDate = 0;

            $maxOffset = $arGroupsPostsOffset + $offsetLimit;
            if ($maxOffset < $arGroupsPostsCount[$oneGroup] || ($arGroupsPostsCount[$oneGroup] == 0 && $arGroupsPostsOffset == 0))
                $saveCurrentOffset = $maxOffset;
            else
                $saveCurrentOffset = 0;
            Yii::$app->db->CreateCommand()->update('vk_groups', ['posts_offset' => $saveCurrentOffset], ['vk_id' => $oneGroup] )->execute();

            if ($arGroupsUpdateWall[$oneGroup]) {
                $lastDate = Yii::$app->db->createCommand("SELECT date FROM posts WHERE group_id = " . $oneGroup . " ORDER BY date DESC LIMIT 1")->queryOne();
                if (isset($lastDate['date'])) {
                    //предел времени по которому определяются актуальные посты
                    $checkDate = HelperFunctions::utimeAdd($lastDate['date'], 0, 0, 0, 0, -30, 0);
                }
            }

            do {

                $resultQuery = VkontakteSDK::makeQuery('wall.get', [
                    'owner_id' => '-'.$oneGroup,
                    'offset' => $offset,
                    'count' => $itemsPerPage,
                    'extended' => 1,
                    'fields' => 'city_name'
                ]);

                if($debug) {
                    echo 'Get group ID -'.$oneGroup." / offset: ".$offset." / itemsPerPage: ".$itemsPerPage."\n";
                }

                $itemsArr = [];
                if(is_object($resultQuery) && property_exists($resultQuery, 'items') && count($resultQuery->items)) {
                    $offset += $itemsPerPage;
                    $itemsArr = $resultQuery->items;
                    $postsCount = $resultQuery->count;

                    foreach ($resultQuery->items as $oneData) {
                        $tempPost = $tempPostUpdate = $tempAttach = $tempComment = $tempLike = [];

                        if (property_exists($oneData, 'id'))
                            $tempPost['post_id'] = $oneData->id;
                        if (property_exists($oneData, 'owner_id'))
                            $tempPost['owner_id'] = $oneData->owner_id;
                        if (property_exists($oneData, 'date'))
                            $tempPost['date'] = $oneData->date;
                        if (property_exists($oneData, 'text'))
                            $tempPost['post_text'] = HelperFunctions::replace4ByteCharacters($oneData->text);
                        if (property_exists($oneData, 'comments'))
                            $tempPost['comments'] = $tempPostUpdate['comments'] = $oneData->comments->count;
                        if (property_exists($oneData, 'likes'))
                            $tempPost['likes'] = $tempPostUpdate['likes'] = $oneData->likes->count;
                        if (property_exists($oneData, 'reposts'))
                            $tempPost['reposts'] = $tempPostUpdate['reposts'] = $oneData->reposts->count;
                        if (property_exists($oneData, 'views'))
                            $tempPost['views'] = $tempPostUpdate['views'] = $oneData->views->count;

                        $tempPost['soc_network_id'] = $socNetworkId;
                        $tempPost['group_id'] = $oneGroup;
                        if ($tempPost['date'] > $checkDate)
                            $tempPost['up_time'] = $tempPostUpdate['up_time'] = $upTime;
                        //репост, не имеет лайков, коментов и просмотров
                        if (property_exists($oneData, 'copy_history')) {
                            foreach ( array_reverse($oneData->copy_history) as $parentPost) {
                                $tempParentPost = [];
                                if (property_exists($parentPost, 'id'))
                                    $tempParentPost['post_id'] = $parentPost->id;
                                if (property_exists($parentPost, 'owner_id'))
                                    $tempParentPost['owner_id'] = $parentPost->owner_id;
                                if (property_exists($parentPost, 'date'))
                                    $tempParentPost['date'] = $parentPost->date;
                                if (property_exists($parentPost, 'text'))
                                    $tempParentPost['post_text'] = HelperFunctions::replace4ByteCharacters($parentPost->text);

                                $tempParentPost['soc_network_id'] = $socNetworkId;
                                $tempParentPost['group_id'] = $oneGroup;
                                $tempParentPost['is_parent'] = 1;

                                if (isset($tempPost['parent_post_id']) && isset($tempPost['parent_owner_id'])) {
                                    $tempParentPost['parent_post_id'] = $tempPost['parent_post_id'];
                                    $tempParentPost['parent_owner_id'] = $tempPost['parent_owner_id'];
                                }

                                if($debug) {
                                    if (isset($tempPost['parent_post_id']) && isset($tempPost['parent_owner_id'])) {
                                        $res = Yii::$app->db->CreateCommand("SELECT id FROM posts WHERE owner_id = {$tempPost['parent_owner_id']} AND post_id = {$tempPost['parent_post_id']} AND soc_network_id = {$socNetworkId}")->queryOne();
                                    }
                                }

                                if ($tempParentPost['owner_id'] > 0) {
                                    $internetUser = $tempParentPost['owner_id'];
                                    if (!isset($arCache['internet_users'][$internetUser])) {
                                        $iuId = (new Query())->select(['[[id]]'])
                                            ->from(InternetUser::tableName())
                                            ->where([
                                                '[[soc_network_id]]' => $socNetworkId,
                                                '[[soc_network_user_id]]' => $internetUser
                                            ])->one();
                                        if (isset($iuId['id']) && $iuId['id']) {
                                            $arCache['internet_users'][$internetUser] = $iuId['id'];
                                        }
                                    }
                                    if (isset($arCache['internet_users'][$internetUser])) {
                                        $tempParentPost['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                    }
                                }

                                /*DEBUG*/
                                if($debug) {
                                    if (isset($tempParentPost['internet_users_id'])) {
                                        echo "\e[1;32mInternet User: ".$tempParentPost['internet_users_id']."\e[0m\n";
                                    }
                                }
                                /*END DEBUG*/

                                Yii::$app->db->CreateCommand()->upsert('posts', $tempParentPost, false)->execute();

                                /*DEBUG*/
                                if($debug) {
                                    if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {

                                        if (isset($res['id']) && $res['id']) {
                                            $issetParentPosts[] = $res['id'];
                                        } else {
                                            $newParentPosts[] =  Yii::$app->db->getLastInsertID();
                                        }
                                        $postsGet++;
                                    }
                                }
                                /*END DEBUG*/

                                if (property_exists($parentPost, 'id') && property_exists($parentPost, 'owner_id')) {
                                    $tempPost['parent_post_id'] = $parentPost->id;
                                    $tempPost['parent_owner_id'] = $parentPost->owner_id;
                                }

                                self::loadVkAttachment($parentPost, $tempParentPost['post_id'], $tempParentPost['owner_id']);
                            }
                        }

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {
                                $res = Yii::$app->db->CreateCommand("SELECT id FROM posts WHERE owner_id = {$tempPost['owner_id']} AND post_id = {$tempPost['post_id']} AND soc_network_id = 1")->queryOne();
                            }
                        }
                        /*END DEBUG*/

                        if ($tempPost['owner_id'] > 0) {
                            $internetUser = $tempPost['owner_id'];
                            if (!isset($arCache['internet_users'][$internetUser])) {
                                $iuId = (new Query())->select(['[[id]]'])
                                    ->from(InternetUser::tableName())
                                    ->where([
                                        '[[soc_network_id]]' => $socNetworkId,
                                        '[[soc_network_user_id]]' => $internetUser
                                    ])->one();
                                if (isset($iuId['id']) && $iuId['id']) {
                                    $arCache['internet_users'][$internetUser] = $iuId['id'];
                                }
                            }
                            if (isset($arCache['internet_users'][$internetUser])) {
                                $tempPost['internet_users_id'] = $arCache['internet_users'][$internetUser];
                            }
                        }

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['internet_users_id'])) {
                                echo "\e[1;32mInternet User: ".$tempPost['internet_users_id']."\e[0m\n";
                            }
                        }
                        /*END DEBUG*/

                        Yii::$app->db->CreateCommand()->upsert('posts', $tempPost, $tempPostUpdate)->execute();

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {

                                if (isset($res['id']) && $res['id']) {
                                    $issetPosts[] = $res['id'];
                                } else {
                                    $newPosts[] =  Yii::$app->db->getLastInsertID();
                                }
                                $postsGet++;
                            }
                        }
                        /*END DEBUG*/

                        self::loadVkAttachment($oneData, $tempPost['post_id'], $tempPost['owner_id']);

                        //отработает в том случае если флаг update_wall_mode = 1, так прерывает проход ниже по ленте для режима одновления ленты
                        if ($tempPost['date'] < $checkDate) {
                            Yii::$app->db->CreateCommand()->update('vk_groups', ['posts_offset' => 0], ['vk_id' => $oneGroup] )->execute();

                            /*DEBUG*/
                            if($debug) {
                                echo "\n"."GET BRAKE because: ".$tempPost['date']." < ".$checkDate." OR ".$offset." > ".$maxOffset."\n\n";
                            }
                            /*END DEBUG*/

                            break 2;
                        }
                    }
                }

            } while (count($itemsArr) && $offset < $maxOffset);

            if ($postsCount != $arGroupsPostsCount[$oneGroup]) {
                Yii::$app->db->CreateCommand()->update('vk_groups', ['posts' => $postsCount], ['vk_id' => $oneGroup] )->execute();
            }

            if (!$arGroupsUpdateWall[$oneGroup]) {
                $countPosts = Yii::$app->db->createCommand("SELECT COUNT(id) AS posts_number FROM posts WHERE (group_id = " . $oneGroup . ") AND (is_parent = 0)")->queryOne();
                if ($postsCount <= $countPosts['posts_number']) {
                    Yii::$app->db->CreateCommand()->update('vk_groups', ['update_wall_mode' => 1], ['vk_id' => $oneGroup])->execute();
                }
            }

        }

        /*DEBUG*/
        if($debug) {
            echo "LOADED posts from api: " . $postsGet . " / parent posts: " . $postsGetParent . "\n";
            echo "INSET NEW POSTS: " ./*implode(',', $newPosts)*/ count($newPosts) . "\n";
            echo "INSET NEW PARENT POSTS:" ./*implode(',', $newParentPosts)*/ count($newParentPosts) . "\n";
            echo "UPDATE POSTS: " ./*implode(',', $issetPosts)*/ count($issetPosts) . "\n";
            echo "UPDATE PARENT POSTS: " ./*implode(',', $issetParentPosts)*/ count($issetParentPosts) . "\n";
        }
        /*END DEBUG*/

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    /**
     * Get comments from special internet users groups
     */
    public function actionGetCommentsOfVkPostsSpec()
    {
        exec("ps aux|grep 'vk/get-comments-of-vk-posts-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/

        $currentTime = time();
        $itemsPerPage = 50;
        $commentsCheck = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;
        $arCache = [];
        $counter = 0;

        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_get_posts_from_vk_groups_spec_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $oneGroup = $makeQuery['option_value'];
        } else {
            return ExitCode::OK;
        }

        //определяем новые комменты которых нет в таблице
        $commentsCheckTemp = Yii::$app->db->createCommand("SELECT posts.post_id, posts.owner_id, CONCAT(posts.owner_id, '_', posts.post_id) AS post_owner_id, posts.group_id AS post_group_id FROM posts LEFT JOIN post_comments ON (posts.post_id = post_comments.post_id AND posts.owner_id = post_comments.owner_id) WHERE (posts.soc_network_id = {$socNetworkId}) AND (posts.group_id = {$oneGroup}) AND (posts.comments > 0) AND (post_comments.owner_id IS NULL) AND (post_comments.post_id IS NULL) ORDER BY posts.date ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($commentsCheckTemp))
            $commentsCheck = array_merge($commentsCheck, $commentsCheckTemp);

        /*DEBUG*/
        if($debug) {
            $t = [];
            foreach ($commentsCheckTemp as $own_post) {
                $t[] = $own_post['post_owner_id'];
            }
            echo "Finded new posts count:\n\n".implode(' | ', $t); sleep(5);
        }
        /*END DEBUG*/

        //определяем записи которые есть в таблице, но информацию по которым нужно актуализировать
        $commentsCheckTemp = Yii::$app->db->createCommand("SELECT DISTINCT post_comments.post_id, post_comments.owner_id, CONCAT(post_comments.owner_id, '_', post_comments.post_id) AS post_owner_id, posts.group_id AS post_group_id FROM post_comments LEFT JOIN posts ON (posts.owner_id = post_comments.owner_id AND posts.post_id = post_comments.post_id AND posts.soc_network_id = post_comments.soc_network_id) WHERE (posts.up_time > {$currentTime}) AND (post_comments.soc_network_id = {$socNetworkId}) AND (post_comments.group_id = {$oneGroup}) ORDER BY post_comments.update_time ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($commentsCheckTemp))
            $commentsCheck = array_merge($commentsCheck, $commentsCheckTemp);

        /*DEBUG*/
        if($debug) {
            $t = [];
            foreach ($commentsCheckTemp as $own_post) {
                $t[] = $own_post['post_owner_id'];
            }
            echo "\n\nFinded Existing posts for comment update:\n\n" . implode(' | ', $t);
            sleep(5);
        }
        /*END DEBUG*/

        if (count($commentsCheck)) {
            foreach ($commentsCheck as $commentCheck) {
                $offsetComments = 0;
                do {

                    $resultComments = VkontakteSDK::makeQuery('wall.getComments', [
                        'owner_id' => $commentCheck['owner_id'],
                        'post_id' => $commentCheck['post_id'],
                        'offset' => $offsetComments,
                        'count' => 100,
                        'extended' => 1,
                        'need_likes' => 1
                    ]);

//                    if ($offsetComments)
//                        usleep(400000);

                    $commentsArr = [];
                    $commentsCount = 0;
                    if(is_object($resultComments) && property_exists($resultComments, 'items') && count($resultComments->items)) {
                        $offsetComments += 100;
                        $commentsArr = $resultComments->items;
                        $commentsCount = $resultComments->count;

                        foreach ($resultComments->items as $oneComment) {
                            $tempComment = $tempCommentUpdate = [];
                            $tempComment['soc_network_id'] = $socNetworkId;
                            if (property_exists($oneComment, 'post_id'))
                                $tempComment['post_id'] = $oneComment->post_id;
                            if (property_exists($oneComment, 'id'))
                                $tempComment['comment_id'] = $oneComment->id;
                            if (property_exists($oneComment, 'owner_id'))
                                $tempComment['owner_id'] = $oneComment->owner_id;
                            if (property_exists($oneComment, 'from_id'))
                                $tempComment['from_user_id'] = $oneComment->from_id;
                            if (property_exists($oneComment, 'date'))
                                $tempComment['date'] = $oneComment->date;
                            if (property_exists($oneComment, 'text'))
                                $tempComment['comment_text'] = HelperFunctions::replace4ByteCharacters($oneComment->text);
                            if (property_exists($oneComment, 'likes'))
                                $tempComment['likes'] = $tempCommentUpdate['likes'] = $oneComment->likes->count;
                            $tempComment['update_time'] = $tempCommentUpdate['update_time'] = $currentTime;
                            $tempComment['group_id'] = $commentCheck['post_group_id'];

                            if($debug) {
                                echo ++$counter;
                                echo ". Comment will be updated/inserted, owner_post_id: ".$commentCheck['owner_id']." ".$commentCheck['post_id']."\n";
                                print_r($tempComment);
                            }

                            if (isset($tempComment['from_user_id']) && intval($tempComment['from_user_id'])) {

                                if ($tempComment['from_user_id'] > 0) {
                                    $internetUser = $tempComment['from_user_id'];
                                    if (!isset($arCache['internet_users'][$internetUser])) {
                                        $iuId = (new Query())->select(['[[id]]'])
                                            ->from(InternetUser::tableName())
                                            ->where([
                                                '[[soc_network_id]]' => $socNetworkId,
                                                '[[soc_network_user_id]]' => $internetUser
                                            ])->one();
                                        if (isset($iuId['id']) && $iuId['id']) {
                                            $arCache['internet_users'][$internetUser] = $iuId['id'];
                                        }
                                    }
                                    if (isset($arCache['internet_users'][$internetUser])) {
                                        $tempComment['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                    }
                                }

                                Yii::$app->db->CreateCommand()->upsert('post_comments', $tempComment, $tempCommentUpdate)->execute();

                                self::loadVkAttachment($oneComment, $tempComment['post_id'], $tempComment['owner_id'], $tempComment['comment_id']);
                            }

                        }
                    } elseif (is_object($resultComments) && property_exists($resultComments, 'count') && ($resultComments->count == 0)) {
                        Yii::$app->db->CreateCommand()->update('posts', ['comments' => 0], ['post_id' => $commentCheck['post_id'], 'owner_id' => $commentCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                    }
                } while (count($commentsArr) && ($offsetComments <= $commentsCount));
            }
        }

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    /**
     * Get likes from special internet users groups
     */
    public function actionGetLikesOfVkPostsCommentsSpec()
    {
        exec("ps aux|grep 'vk/get-likes-of-vk-posts-comments-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        $currentTime = time();
        $itemsPerPage = 50;
        $likesCheck = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;
        $arCache = [];

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        if($debug) {
            $likesGet = $likesAll = 0;
            $startTime = $firstStartTime = microtime(true);
        }
        /*END DEBUG*/

        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_get_posts_from_vk_groups_spec_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $oneGroup = $makeQuery['option_value'];
        } else {
            return ExitCode::OK;
        }

        //определяем новые посты которых нет в таблице
        $likesCheckTemp = Yii::$app->db->createCommand("
            SELECT posts.post_id AS liked_item_id, posts.owner_id, CONCAT(posts.owner_id, '_', posts.post_id) AS post_owner_id, 'post' AS `type`, posts.group_id AS post_group_id 
            FROM posts 
            LEFT JOIN post_comment_likes ON (posts.owner_id = post_comment_likes.owner_id AND posts.post_id = post_comment_likes.liked_item_id) 
            WHERE (posts.soc_network_id = {$socNetworkId}) AND (posts.group_id = {$oneGroup}) 
                AND (posts.likes > 0) AND (post_comment_likes.owner_id IS NULL) 
                AND (post_comment_likes.liked_item_id IS NULL) 
                ORDER BY posts.date ASC
                LIMIT {$itemsPerPage}")->queryAll();//
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        //определяем новые коменты которых нет в таблице
        $likesCheckTemp = Yii::$app->db->createCommand("
            SELECT post_comments.comment_id AS liked_item_id, post_comments.owner_id, CONCAT(post_comments.owner_id, '_', post_comments.comment_id) AS post_owner_id, 'comment' AS type, post_comments.group_id AS post_group_id 
            FROM post_comments 
            LEFT JOIN post_comment_likes ON (post_comments.owner_id = post_comment_likes.owner_id AND post_comments.comment_id = post_comment_likes.liked_item_id) 
            WHERE (post_comments.soc_network_id = {$socNetworkId}) AND (post_comments.group_id = {$oneGroup}) 
                AND (post_comments.likes > 0) AND (post_comment_likes.owner_id IS NULL) 
                AND (post_comment_likes.liked_item_id IS NULL) 
                ORDER BY post_comments.date ASC 
                LIMIT {$itemsPerPage}")->queryAll();
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        //определяем записи которые есть в таблице, но информацию по которым нужно актуализировать
        $likesCheckTemp = Yii::$app->db->createCommand("
            SELECT DISTINCT post_comment_likes.liked_item_id, post_comment_likes.owner_id, CONCAT(post_comment_likes.owner_id, '_', post_comment_likes.liked_item_id) AS post_owner_id, post_comment_likes.type, posts.group_id AS post_group_id 
            FROM post_comment_likes 
            INNER JOIN posts ON (posts.owner_id = post_comment_likes.owner_id AND posts.post_id = post_comment_likes.liked_item_id AND posts.soc_network_id = post_comment_likes.soc_network_id) 
            WHERE (posts.up_time > {$currentTime}) AND (post_comment_likes.soc_network_id = {$socNetworkId}) AND (post_comment_likes.group_id = {$oneGroup}) 
            ORDER BY post_comment_likes.update_time ASC 
            LIMIT {$itemsPerPage}")->queryAll();
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        if (count($likesCheck)) {
            foreach ($likesCheck as $likeCheck) {

                /*DEBUG*/
                if($debug) {
                    $likesGet = 0;
                }
                /*END DEBUG*/

                $offsetLikes = 0;
                do {

                    $resultLikes = VkontakteSDK::makeQuery('likes.getList', [
                        'owner_id' => $likeCheck['owner_id'],
                        'item_id' => $likeCheck['liked_item_id'],
                        'offset' => $offsetLikes,
                        'count' => 1000,
                        'extended' => 0,
                        'type' => $likeCheck['type']
                    ]);

//                    if ($offsetLikes)
//                        usleep(400000);

                    $likesArr = [];
                    $likesCount = 0;
                    if (is_object($resultLikes) && property_exists($resultLikes, 'items') && count($resultLikes->items)) {
                        $offsetLikes += 1000;
                        $likesArr = $resultLikes->items;
                        $likesCount = $resultLikes->count;

                        foreach ($resultLikes->items as $oneLike) {
                            $tempLike = $tempLikeUpdate = [];
                            $tempLike['soc_network_id'] = $socNetworkId;
                            $tempLike['liked_item_id'] = $likeCheck['liked_item_id'];
                            $tempLike['owner_id'] = $likeCheck['owner_id'];
                            $tempLike['from_user_id'] = $oneLike->id;
                            $tempLike['type'] = $likeCheck['type'];
                            $tempLike['update_time'] = $tempLikeUpdate['update_time'] = $currentTime;
                            $tempLike['group_id'] = $likeCheck['post_group_id'];

                            /*DEBUG*/
                            if($debug) {
                                $likesGet++;
                                $likesAll++;
                            }
                            /*END DEBUG*/

                            if ($tempLike['from_user_id'] > 0) {
                                $internetUser = $tempLike['from_user_id'];
                                if (!isset($arCache['internet_users'][$internetUser])) {
                                    $iuId = (new Query())->select(['[[id]]'])
                                        ->from(InternetUser::tableName())
                                        ->where([
                                            '[[soc_network_id]]' => $socNetworkId,
                                            '[[soc_network_user_id]]' => $internetUser
                                        ])->one();
                                    if (isset($iuId['id']) && $iuId['id']) {
                                        $arCache['internet_users'][$internetUser] = $iuId['id'];
                                    }
                                }
                                if (isset($arCache['internet_users'][$internetUser])) {
                                    $tempLike['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                }
                            }

                            Yii::$app->db->CreateCommand()->upsert('post_comment_likes', $tempLike, $tempLikeUpdate)->execute();
                        }
                    } elseif (is_object($resultLikes) && property_exists($resultLikes, 'count') && ($resultLikes->count == 0)) {
                        if ($likeCheck['type'] == 'post') {
                            Yii::$app->db->CreateCommand()->update('posts', ['likes' => 0], ['post_id' => $likeCheck['liked_item_id'], 'owner_id' => $likeCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                        }
                        if ($likeCheck['type'] == 'comment') {
                            Yii::$app->db->CreateCommand()->update('post_comments', ['likes' => 0], ['comment_id' => $likeCheck['liked_item_id'], 'owner_id' => $likeCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                        }
                    }

                    /*DEBUG*/
                    if($debug) {
                        echo "\nQuery likes from API: OwnerID ".$likeCheck['owner_id']." / Liked ItemID ".$likeCheck['liked_item_id']." / Type ".$likeCheck['type']." / Offset ".$offsetLikes." / CountAll: ".$resultLikes->count."\n"."Time: ".gmdate("H:i:s", microtime(true) - $startTime)." / Time from start ".gmdate("H:i:s", microtime(true) - $firstStartTime)."\n";
                        $startTime = microtime(true);
                        echo "Get likes Per iteration: ".count($likesArr)."\n";
                        echo "Get likes ALL: ".$likesGet."\n";
                    }
                    /*END DEBUG*/

                } while (count($likesArr) && ($offsetLikes <= $likesCount));

                /*DEBUG*/
                if($debug) {
                    echo "Get likes SUMM: ".$likesGet."\n";
                }
                /*END DEBUG*/

            }
        }

        /*DEBUG*/
        if($debug) {
            echo "END: ".gmdate("H:i:s", microtime(true) - $startTime)."\n";
            echo "GET ALL LIKES FROM API:".$likesAll."\n";
        }
        /*END DEBUG*/

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    /**
     * Calc spec internet user groups posts - events link
     */
    public function actionCreatePostsEventsIndexSpec()
    {
        exec("ps aux|grep 'vk/create-posts-events-index-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $this->execTime('start', func_get_args());

        $itemsPerPage = 1;
        $offset = 0;
        $checkArray = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;

        /*DEBUG*/
        $arArgs = func_get_args();
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/

        /*Получаем настройку Оффсета*/
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_create_posts_events_index_spec_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $offset = intval($makeQuery['option_value']);
        } elseif (!isset($makeQuery['option_value'])) {
            Yii::$app->db->createCommand()->insert('options', [
                'option_name' => 'vk_create_posts_events_index_spec_offset',
                'option_value' => '0'
            ])->execute();
        }

        /*Обновляем оффсет*/
        $maxOffset = $offset + $itemsPerPage;
        $query = Yii::$app->db->CreateCommand("
            SELECT COUNT(id) AS items_count 
            FROM posts 
            WHERE (is_parent = 0) 
                AND (
                    group_id IN (
                        SELECT group_id 
                        FROM temp_spec_groups 
                        WHERE soc_network_id = {$socNetworkId}
                    )
                 )"
        )->queryOne();
        $itemsCount = $query['items_count'];
        if ($maxOffset < $itemsCount) {
            $saveOffset = $maxOffset;
        } else {
            $saveOffset = 0;
        }
        Yii::$app->db->createCommand()->update('options', ['option_value' => $saveOffset], ['option_name' => 'vk_create_posts_events_index_spec_offset'])->execute();

        /*Получаем данные для текущего шага*/
        $makeQuery = Yii::$app->db->createCommand("
                SELECT id, post_id, owner_id, soc_network_id, likes, comments, group_id, parent_post_id, parent_owner_id 
                FROM posts 
                WHERE (
                    (soc_network_id, group_id) IN (
                        SELECT {$socNetworkId} AS soc_network_id, group_id FROM temp_spec_groups WHERE soc_network_id = {$socNetworkId}  
                    )
                ) AND (is_parent = 0) ORDER BY id ASC LIMIT " . $itemsPerPage . " OFFSET " . $offset
        )->queryAll();
        if (is_array($makeQuery) && count($makeQuery)) {
            foreach ($makeQuery as $onePost) {

                $checkArray[] = ['item_id' => $onePost['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $onePost['owner_id'], 'type' => 'post'];

                if ($onePost['parent_post_id'] > 0 && $onePost['parent_owner_id'] > 0) {
                    $queryParentPostId = Yii::$app->db->createCommand("
                        SELECT id 
                        FROM posts 
                        WHERE soc_network_id = {$onePost['soc_network_id']} 
                            AND post_id = {$onePost['parent_post_id']} 
                                AND owner_id = {$onePost['parent_owner_id']}")->queryOne();
                    if ($queryParentPostId['id']) {
                        $checkArray[] = ['item_id' => $queryParentPostId['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $onePost['parent_owner_id'], 'type' => 'post', 'child_post' => $onePost['id']];
                    }
                }

                if ($onePost['comments'] > 0) {
                    $queryComments = Yii::$app->db->createCommand("
                        SELECT id, comment_id, from_user_id, likes 
                        FROM post_comments 
                            WHERE (soc_network_id = {$onePost['soc_network_id']}) 
                                AND (owner_id = {$onePost['owner_id']}) 
                                AND (post_id = {$onePost['post_id']})
                    ")->queryAll();
                    if (is_array($queryComments) && count($queryComments)) {
                        foreach ($queryComments as $oneComment) {

                            $checkArray[] = ['item_id' => $oneComment['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $oneComment['from_user_id'], 'type' => 'comment', 'parent_post' => $onePost['id']];

                            if ($oneComment['likes'] > 0) {
                                $queryLikes = Yii::$app->db->createCommand("
                                    SELECT id, from_user_id 
                                    FROM post_comment_likes 
                                    WHERE (soc_network_id = {$onePost['soc_network_id']}) 
                                    AND (owner_id = {$onePost['owner_id']}) 
                                    AND (liked_item_id = {$oneComment['comment_id']}) 
                                    AND type = 'comment'
                                ")->queryAll();

                                if (is_array($queryLikes) && count($queryLikes)) {
                                    foreach ($queryLikes as $oneLike) {
                                        $checkArray[] = ['item_id' => $oneLike['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $oneLike['from_user_id'], 'type' => 'like', 'parent_post' => $onePost['id'], 'parent_comment' => $oneComment['id']];
                                    }
                                }
                            }
                        }
                    }
                }

                if ($onePost['likes'] > 0) {
                    $queryLikes = Yii::$app->db->createCommand("
                        SELECT id, from_user_id 
                        FROM post_comment_likes 
                        WHERE (soc_network_id = {$onePost['soc_network_id']}) 
                            AND (owner_id = {$onePost['owner_id']}) 
                            AND (liked_item_id = {$onePost['post_id']}) 
                            AND type = 'post'")->queryAll();

                    if (is_array($queryLikes) && count($queryLikes)) {
                        foreach ($queryLikes as $oneLike) {
                            $checkArray[] = ['item_id' => $oneLike['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $oneLike['from_user_id'], 'type' => 'like', 'parent_post' => $onePost['id']];
                        }
                    }
                }
            }
        }

        if ($debug) {
            $this->stdout("Checked Items".PHP_EOL.print_r($checkArray), Console::FG_YELLOW);
            $this->stdout(print_r($checkArray));
        }

        /*Поиск и добавление связей через таблицы internet_users, internet_users_galleries, galleries*/
        if (count($checkArray)) {
            //$socNetworkId = [];
            $socNetworkUserId = [];
            $checkPostsEvents = [];
            foreach ($checkArray as $oneItem) {
               // $socNetworkId[] = $oneItem['soc_network_id'];
                $socNetworkUserId[] = $oneItem['soc_network_user_id'];
            }

           // $socNetworkId = array_unique($socNetworkId);
            $allSocNetworkUserId = array_chunk(array_unique($socNetworkUserId), $itemsPerPage);

            foreach ($allSocNetworkUserId as $socNetworkUserIds) {

                $queryInternetUsersIds = Yii::$app->db->createCommand("
                    SELECT id, soc_network_id, soc_network_user_id 
                    FROM internet_users WHERE (soc_network_id = {$socNetworkId}) AND (soc_network_user_id IN (" . implode(',', $socNetworkUserIds) . "))
                ")->queryAll();

                if (is_array($queryInternetUsersIds) && count($queryInternetUsersIds)) {

                    $internetUsersId = [];
                    foreach ($queryInternetUsersIds as $oneInternetUser) {
                        $internetUsersId[] = $oneInternetUser['id'];
                        foreach ($checkArray as &$oneItem) {
                            if ($oneInternetUser['soc_network_id'] == $oneItem['soc_network_id'] && $oneInternetUser['soc_network_user_id'] == $oneItem['soc_network_user_id']) {
                                $oneItem['internet_user_id'] = $oneInternetUser['id'];
                            }
                        }
                    }

                    if (count($internetUsersId)) {
                        $allInternetUsersId = array_chunk($internetUsersId, $itemsPerPage);
                        foreach ($allInternetUsersId as $internetUsersIds) {

                            $queryInternetUsersGalleriesIds = Yii::$app->db->createCommand("
                                select photos.gallery_id2, findface.internet_users_id
                                from findface 
                                inner join photos on photos.id = findface.photos_id
                                where findface.internet_users_id in (" . implode(',', $internetUsersIds) . ")  and findface.alignment != 'not alignment'
                                group by photos.gallery_id2, findface.internet_users_id
                            ")->queryAll();

                            if (is_array($queryInternetUsersGalleriesIds) && count($queryInternetUsersGalleriesIds)) {

                                $galleriesId = [];
                                foreach ($queryInternetUsersGalleriesIds as $oneInternetUsersGallery) {
                                    $galleriesId[] = $oneInternetUsersGallery['gallery_id2'];
                                    foreach ($checkArray as &$oneItem) {
                                        if (isset($oneItem['internet_user_id']) && $oneInternetUsersGallery['internet_users_id'] == $oneItem['internet_user_id']) {
                                            $oneItem['gallery_id2'] = $oneInternetUsersGallery['gallery_id2'];
                                        }
                                    }
                                }

                                if (count($galleriesId)) {
                                    $allGalleriesId = array_chunk($galleriesId, $itemsPerPage);
                                    foreach ($allGalleriesId as $galleriesIds) {

                                        $queryEventsIds = Yii::$app->db->createCommand("
                                                SELECT id, event_id 
                                                FROM galleries 
                                                WHERE (id IN (" . implode(',', $galleriesIds) . "))")->queryAll();

                                        if (is_array($queryEventsIds) && count($queryEventsIds)) {
                                            foreach ($queryEventsIds as $oneEventId) {
                                                foreach ($checkArray as $oneItem) {
                                                    if (isset($oneItem['gallery_id2']) && $oneItem['gallery_id2'] == $oneEventId['id'] && $oneEventId['event_id'] > 0) {
                                                        $checkPostsEvents[$oneItem['type']][$oneItem['item_id']][] = $oneEventId['event_id'];
                                                        Yii::$app->db->createCommand()->upsert('post_events', [
                                                            'item_id' => $oneItem['item_id'],
                                                            'event_id' => $oneEventId['event_id'],
                                                            'type' => $oneItem['type']
                                                        ], false)->execute();
                                                        if (isset($oneItem['child_post'])) {
                                                            $checkPostsEvents['post'][$oneItem['child_post']][] = $oneEventId['event_id'];
                                                            Yii::$app->db->createCommand()->upsert('post_events', [
                                                                'item_id' => $oneItem['child_post'],
                                                                'event_id' => $oneEventId['event_id'],
                                                                'type' => 'post'
                                                            ], false)->execute();
                                                        }
                                                        if (isset($oneItem['parent_post'])) {
                                                            $checkPostsEvents['post'][$oneItem['parent_post']][] = $oneEventId['event_id'];
                                                            Yii::$app->db->createCommand()->upsert('post_events', [
                                                                'item_id' => $oneItem['parent_post'],
                                                                'event_id' => $oneEventId['event_id'],
                                                                'type' => 'post'
                                                            ], false)->execute();
                                                        }
                                                        if (isset($oneItem['parent_comment'])) {
                                                            $checkPostsEvents['comment'][$oneItem['parent_comment']][] = $oneEventId['event_id'];
                                                            Yii::$app->db->createCommand()->upsert('post_events', [
                                                                'item_id' => $oneItem['parent_comment'],
                                                                'event_id' => $oneEventId['event_id'],
                                                                'type' => 'comment'
                                                            ], false)->execute();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /*Удаление невалидных связей*/ //проба закоментировать, неособо вроде как нужная штука
            foreach ($checkArray as $checkOneItem) {
                if (isset($checkPostsEvents[$checkOneItem['type']][$checkOneItem['item_id']]) &&
                    is_array($checkPostsEvents[$checkOneItem['type']][$checkOneItem['item_id']]) &&
                    count($checkPostsEvents[$checkOneItem['type']][$checkOneItem['item_id']])) {

                    $checkIsExistsLink = Yii::$app->db->createCommand("
                        SELECT id 
                        FROM post_events 
                        WHERE item_id = {$checkOneItem['item_id']} 
                            AND type = '{$checkOneItem['type']}' 
                            AND event_id NOT IN (".implode(',', array_unique($checkPostsEvents[$checkOneItem['type']][$checkOneItem['item_id']])).")")->queryAll();

                    $itemsForDelete = [];
                    if (count($checkIsExistsLink)) {
                        foreach ($checkIsExistsLink as $existsId) {
                            $itemsForDelete[] = $existsId['id'];
                        }
                    }
                    if (count($itemsForDelete)) {
                        Yii::$app->db->createCommand("DELETE FROM post_events WHERE id = ".implode(',', $itemsForDelete))->execute();
                    }
                } else {
                    $checkIsExistsLink = Yii::$app->db->createCommand("
                        SELECT COUNT(id) AS count 
                        FROM post_events 
                        WHERE (item_id = {$checkOneItem['item_id']}) AND (type = '{$checkOneItem['type']}')")->execute();
                    if (isset($checkIsExistsLink['count']) && $checkIsExistsLink['count']) {
                        Yii::$app->db->createCommand("DELETE FROM post_events WHERE (item_id = {$checkOneItem['item_id']}) AND (type = '{$checkOneItem['type']}')")->execute();
                    }
                }
            }
        }

        $this->execTime('end', func_get_args());

        return ExitCode::OK;
    }


    /**
     * Get posts from special internet users pages
     */
    public function actionGetPostsFromVkPagesSpec()
    {
        exec("ps aux|grep 'vk/get-posts-from-vk-pages-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);
        /*Скрипт работает в 2х режимах, 1. прохождение ленты до конца с попыткой забрать все посты; 2. обновление ленты только новыми
        Режимы управляются флагом update_wall_mode*/
        $arGroups = $arGroupsPostsOffset = $arGroupsPostsCount = $arGroupsUpdateWall = [];
        $arCache = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;


        /*нужно получить все ИД групп, с которых наши спец польхователи*/
        $makeQuery = Yii::$app->db->createCommand("SELECT id, soc_network_user_id, posts, update_wall_mode FROM temp_spec_internet_users WHERE soc_network_id = 1 ORDER BY id")->queryAll();

        foreach ($makeQuery as $oneGroup) {
            $arGroups[] = $oneGroup['soc_network_user_id'];
            $arGroupsPostsCount[$oneGroup['soc_network_user_id']] = !empty($oneGroup['posts'])?$oneGroup['posts']:0;
            $arGroupsUpdateWall[$oneGroup['soc_network_user_id']] = intval($oneGroup['update_wall_mode'])?1:0;
        }

        /*Получаем настройку Оффсета*/
        $oneGroup = reset($arGroups);
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_get_posts_from_vk_pages_spec_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $oneGroup = $makeQuery['option_value'];
        } elseif (!isset($makeQuery['option_value'])) {
            Yii::$app->db->createCommand()->insert('options', [
                'option_name' => 'vk_get_posts_from_vk_pages_spec_offset',
                'option_value' => reset($arGroups)
            ])->execute();
        }

        /*Обновляем оффсет*/
        $currentGroupKey = array_search($oneGroup, $arGroups);
        if ($currentGroupKey == (count($arGroups)-1)) {
            $oneGroup = reset($arGroups);
        } else {
            $oneGroup = $arGroups[$currentGroupKey+1];
        }
        Yii::$app->db->createCommand()->update('options', ['option_value' => $oneGroup], ['option_name' => 'vk_get_posts_from_vk_pages_spec_offset'])->execute();

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        if($debug) {
            $postsGet = $postsGetParent = 0;
            $issetPosts = $newPosts = $issetParentPosts = $newParentPosts = [];
        }
        /*END DEBUG*/

        $itemsPerPage = 100; //максимальное значение для данного метода!!!!
        $offsetLimit = 200;
        //время в будущее для того чтобы скрипты забора комментов и лайков знали какие посты акруальны и какие обновлять
        $upTime = HelperFunctions::utimeAdd(time(), 0, 0, 0, 0, 30, 0);

        if ($oneGroup) {

            $makeQuery = Yii::$app->db->createCommand("SELECT posts_offset FROM temp_spec_internet_users WHERE soc_network_user_id = ". $oneGroup)->queryOne();
            $arGroupsPostsOffset = !empty($makeQuery['posts_offset'])?$makeQuery['posts_offset']:0;

            $postsCount = $arGroupsPostsCount[$oneGroup];
            $offset = $arGroupsPostsOffset;
            $checkDate = 0;

            $maxOffset = $arGroupsPostsOffset + $offsetLimit;
            if ($maxOffset < $arGroupsPostsCount[$oneGroup] || ($arGroupsPostsCount[$oneGroup] == 0 && $arGroupsPostsOffset == 0))
                $saveCurrentOffset = $maxOffset;
            else
                $saveCurrentOffset = 0;
            Yii::$app->db->CreateCommand()->update('temp_spec_internet_users', ['posts_offset' => $saveCurrentOffset], ['soc_network_user_id' => $oneGroup] )->execute();

            if ($arGroupsUpdateWall[$oneGroup]) {
                $lastDate = Yii::$app->db->createCommand("SELECT date FROM posts WHERE owner_id = " . $oneGroup . " ORDER BY date DESC LIMIT 1")->queryOne();
                if (isset($lastDate['date'])) {
                    //предел времени по которому определяются актуальные посты
                    $checkDate = HelperFunctions::utimeAdd($lastDate['date'], 0, 0, 0, 0, -30, 0);
                }
            }

            do {

                $resultQuery = VkontakteSDK::makeQuery('wall.get', [
                    'owner_id' => $oneGroup,
                    'offset' => $offset,
                    'count' => $itemsPerPage,
                    'extended' => 1,
                    'fields' => 'city_name'
                ]);

                /*DEBUG*/
                if($debug) {
                    echo 'Get user ID '.$oneGroup." / offset: ".$offset." / itemsPerPage: ".$itemsPerPage."\n";
                }
                /*END DEBUG*/

//                if ($offset)
//                    usleep(400000);

                $itemsArr = [];
                if(is_object($resultQuery) && property_exists($resultQuery, 'items') && count($resultQuery->items)) {
                    $offset += $itemsPerPage;
                    $itemsArr = $resultQuery->items;
                    $postsCount = $resultQuery->count;

                    foreach ($resultQuery->items as $oneData) {
                        $tempPost = $tempPostUpdate = $tempAttach = $tempComment = $tempLike = [];

                        if (property_exists($oneData, 'id'))
                            $tempPost['post_id'] = $oneData->id;
                        if (property_exists($oneData, 'owner_id'))
                            $tempPost['owner_id'] = $oneData->owner_id;
                        if (property_exists($oneData, 'date'))
                            $tempPost['date'] = $oneData->date;
                        if (property_exists($oneData, 'text'))
                            $tempPost['post_text'] = HelperFunctions::replace4ByteCharacters($oneData->text);
                        if (property_exists($oneData, 'comments'))
                            $tempPost['comments'] = $tempPostUpdate['comments'] = $oneData->comments->count;
                        if (property_exists($oneData, 'likes'))
                            $tempPost['likes'] = $tempPostUpdate['likes'] = $oneData->likes->count;
                        if (property_exists($oneData, 'reposts'))
                            $tempPost['reposts'] = $tempPostUpdate['reposts'] = $oneData->reposts->count;
                        if (property_exists($oneData, 'views'))
                            $tempPost['views'] = $tempPostUpdate['views'] = $oneData->views->count;

                        $tempPost['soc_network_id'] = $socNetworkId;
                        //$tempPost['group_id'] = $oneGroup;
                        if ($tempPost['date'] > $checkDate)
                            $tempPost['up_time'] = $tempPostUpdate['up_time'] = $upTime;
                        //репост, не имеет лайков, коментов и просмотров
                        if (property_exists($oneData, 'copy_history')) {
                            foreach ( array_reverse($oneData->copy_history) as $parentPost) {
                                $tempParentPost = [];
                                if (property_exists($parentPost, 'id'))
                                    $tempParentPost['post_id'] = $parentPost->id;
                                if (property_exists($parentPost, 'owner_id'))
                                    $tempParentPost['owner_id'] = $parentPost->owner_id;
                                if (property_exists($parentPost, 'date'))
                                    $tempParentPost['date'] = $parentPost->date;
                                if (property_exists($parentPost, 'text'))
                                    $tempParentPost['post_text'] = HelperFunctions::replace4ByteCharacters($parentPost->text);

                                $tempParentPost['soc_network_id'] = $socNetworkId;
                                //$tempParentPost['group_id'] = $oneGroup;
                                $tempParentPost['is_parent'] = 1;

                                if (isset($tempPost['parent_post_id']) && isset($tempPost['parent_owner_id'])) {
                                    $tempParentPost['parent_post_id'] = $tempPost['parent_post_id'];
                                    $tempParentPost['parent_owner_id'] = $tempPost['parent_owner_id'];
                                }

                                /*DEBUG*/
                                if($debug) {
                                    if (isset($tempPost['parent_post_id']) && isset($tempPost['parent_owner_id'])) {
                                        $res = Yii::$app->db->CreateCommand("SELECT id FROM posts WHERE owner_id = {$tempPost['parent_owner_id']} AND post_id = {$tempPost['parent_post_id']} AND soc_network_id = {$socNetworkId}")->queryOne();
                                    }
                                }
                                /*END DEBUG*/

                                if ($tempParentPost['owner_id'] > 0) {
                                    $internetUser = $tempParentPost['owner_id'];
                                    if (!isset($arCache['internet_users'][$internetUser])) {
                                        $iuId = (new Query())->select(['[[id]]'])
                                            ->from(InternetUser::tableName())
                                            ->where([
                                                '[[soc_network_id]]' => $socNetworkId,
                                                '[[soc_network_user_id]]' => $internetUser
                                            ])->one();
                                        if (isset($iuId['id']) && $iuId['id']) {
                                            $arCache['internet_users'][$internetUser] = $iuId['id'];
                                        }
                                    }
                                    if (isset($arCache['internet_users'][$internetUser])) {
                                        $tempParentPost['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                    }
                                }

                                /*DEBUG*/
                                if($debug) {
                                    if (isset($tempParentPost['internet_users_id'])) {
                                        $this->stdout("Internet User: ".$tempParentPost['internet_users_id']. PHP_EOL, Console::FG_GREEN);
                                    }
                                }
                                /*END DEBUG*/

                                Yii::$app->db->CreateCommand()->upsert('posts', $tempParentPost, false)->execute();

                                /*DEBUG*/
                                if($debug) {
                                    if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {

                                        if (isset($res['id']) && $res['id']) {
                                            $issetParentPosts[] = $res['id'];
                                        } else {
                                            $newParentPosts[] =  Yii::$app->db->getLastInsertID();
                                        }
                                        $postsGet++;
                                    }
                                }
                                /*END DEBUG*/

                                if (property_exists($parentPost, 'id') && property_exists($parentPost, 'owner_id')) {
                                    $tempPost['parent_post_id'] = $parentPost->id;
                                    $tempPost['parent_owner_id'] = $parentPost->owner_id;
                                }

                                self::loadVkAttachment($parentPost, $tempParentPost['post_id'], $tempParentPost['owner_id']);
                            }
                        }

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {
                                $res = Yii::$app->db->CreateCommand("SELECT id FROM posts WHERE owner_id = {$tempPost['owner_id']} AND post_id = {$tempPost['post_id']} AND soc_network_id = 1")->queryOne();
                            }
                        }
                        /*END DEBUG*/

                        if ($tempPost['owner_id'] > 0) {
                            $internetUser = $tempPost['owner_id'];
                            if (!isset($arCache['internet_users'][$internetUser])) {
                                $iuId = (new Query())->select(['[[id]]'])
                                    ->from(InternetUser::tableName())
                                    ->where([
                                        '[[soc_network_id]]' => $socNetworkId,
                                        '[[soc_network_user_id]]' => $internetUser
                                    ])->one();
                                if (isset($iuId['id']) && $iuId['id']) {
                                    $arCache['internet_users'][$internetUser] = $iuId['id'];
                                }
                            }
                            if (isset($arCache['internet_users'][$internetUser])) {
                                $tempPost['internet_users_id'] = $arCache['internet_users'][$internetUser];
                            }
                        }

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['internet_users_id'])) {
                                echo "\e[1;32mInternet User: ".$tempPost['internet_users_id']."\e[0m\n";
                            }
                        }
                        /*END DEBUG*/

                        Yii::$app->db->CreateCommand()->upsert('posts', $tempPost, $tempPostUpdate)->execute();

                        /*DEBUG*/
                        if($debug) {
                            if (isset($tempPost['post_id']) && isset($tempPost['owner_id'])) {

                                if (isset($res['id']) && $res['id']) {
                                    $issetPosts[] = $res['id'];
                                } else {
                                    $newPosts[] =  Yii::$app->db->getLastInsertID();
                                }
                                $postsGet++;
                            }
                        }
                        /*END DEBUG*/

                        self::loadVkAttachment($oneData, $tempPost['post_id'], $tempPost['owner_id']);

                        //отработает в том случае если флаг update_wall_mode = 1, так прерывает проход ниже по ленте для режима одновления ленты
                        if ($tempPost['date'] < $checkDate) {
                            Yii::$app->db->CreateCommand()->update('temp_spec_internet_users', ['posts_offset' => 0], ['soc_network_user_id' => $oneGroup] )->execute();

                            /*DEBUG*/
                            if($debug) {
                                echo "\n"."GET BRAKE because: ".$tempPost['date']." < ".$checkDate." OR ".$offset." > ".$maxOffset."\n\n";
                            }
                            /*END DEBUG*/

                            break 2;
                        }
                    }
                }

            } while (count($itemsArr) && $offset < $maxOffset);

            if ($postsCount != $arGroupsPostsCount[$oneGroup]) {
                Yii::$app->db->CreateCommand()->update('temp_spec_internet_users', ['posts' => $postsCount], ['soc_network_user_id' => $oneGroup] )->execute();
            }

            if (!$arGroupsUpdateWall[$oneGroup]) {
                $countPosts = Yii::$app->db->createCommand("SELECT COUNT(id) AS posts_number FROM posts WHERE (owner_id = " . $oneGroup . ") AND (is_parent = 0)")->queryOne();
                if ($postsCount <= $countPosts['posts_number']) {
                    Yii::$app->db->CreateCommand()->update('temp_spec_internet_users', ['update_wall_mode' => 1], ['soc_network_user_id' => $oneGroup])->execute();
                }
            }

        }

        /*DEBUG*/
        if($debug) {
            echo "LOADED posts from api: " . $postsGet . " / parent posts: " . $postsGetParent . "\n";
            echo "INSET NEW POSTS: " ./*implode(',', $newPosts)*/ count($newPosts) . "\n";
            echo "INSET NEW PARENT POSTS:" ./*implode(',', $newParentPosts)*/ count($newParentPosts) . "\n";
            echo "UPDATE POSTS: " ./*implode(',', $issetPosts)*/ count($issetPosts) . "\n";
            echo "UPDATE PARENT POSTS: " ./*implode(',', $issetParentPosts)*/ count($issetParentPosts) . "\n";
        }
        /*END DEBUG*/

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    /**
     * Get comments from special internet users pages
     */
    public function actionGetCommentsOfVkPostsPagesSpec()
    {
        exec("ps aux|grep 'vk/get-comments-of-vk-posts-pages-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/

        $currentTime = time();
        $itemsPerPage = 50;
        $commentsCheck = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;
        $arCache = [];
        $counter = 0;

        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_get_posts_from_vk_pages_spec_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $oneGroup = $makeQuery['option_value'];
        } else {
            return ExitCode::OK;
        }

        //определяем новые комменты которых нет в таблице
        $commentsCheckTemp = Yii::$app->db->createCommand("SELECT posts.post_id, posts.owner_id, CONCAT(posts.owner_id, '_', posts.post_id) AS post_owner_id FROM posts LEFT JOIN post_comments ON (posts.post_id = post_comments.post_id AND posts.owner_id = post_comments.owner_id) WHERE (posts.soc_network_id = {$socNetworkId}) AND (posts.owner_id = {$oneGroup}) AND (posts.comments > 0) AND (post_comments.owner_id IS NULL) AND (post_comments.post_id IS NULL) ORDER BY posts.date ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($commentsCheckTemp))
            $commentsCheck = array_merge($commentsCheck, $commentsCheckTemp);

        /*DEBUG*/
        if($debug) {
            $t = [];
            foreach ($commentsCheckTemp as $own_post) {
                $t[] = $own_post['post_owner_id'];
            }
            echo "Finded new posts count:\n\n".implode(' | ', $t); sleep(5);
        }
        /*END DEBUG*/

        //определяем записи которые есть в таблице, но информацию по которым нужно актуализировать
        $commentsCheckTemp = Yii::$app->db->createCommand("SELECT DISTINCT post_comments.post_id, post_comments.owner_id, CONCAT(post_comments.owner_id, '_', post_comments.post_id) AS post_owner_id FROM post_comments LEFT JOIN posts ON (posts.owner_id = post_comments.owner_id AND posts.post_id = post_comments.post_id AND posts.soc_network_id = post_comments.soc_network_id) WHERE (posts.up_time > {$currentTime}) AND (post_comments.soc_network_id = {$socNetworkId}) AND (post_comments.owner_id = {$oneGroup}) ORDER BY post_comments.update_time ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($commentsCheckTemp))
            $commentsCheck = array_merge($commentsCheck, $commentsCheckTemp);

        /*DEBUG*/
        if($debug) {
            $t = [];
            foreach ($commentsCheckTemp as $own_post) {
                $t[] = $own_post['post_owner_id'];
            }
            echo "\n\nFinded Existing posts for comment update:\n\n" . implode(' | ', $t);
            sleep(5);
        }
        /*END DEBUG*/

        if (count($commentsCheck)) {
            foreach ($commentsCheck as $commentCheck) {
                $offsetComments = 0;
                do {

                    $resultComments = VkontakteSDK::makeQuery('wall.getComments', [
                        'owner_id' => $commentCheck['owner_id'],
                        'post_id' => $commentCheck['post_id'],
                        'offset' => $offsetComments,
                        'count' => 100,
                        'extended' => 1,
                        'need_likes' => 1
                    ]);

//                    if ($offsetComments)
//                        usleep(400000);

                    $commentsArr = [];
                    $commentsCount = 0;
                    if(is_object($resultComments) && property_exists($resultComments, 'items') && count($resultComments->items)) {
                        $offsetComments += 100;
                        $commentsArr = $resultComments->items;
                        $commentsCount = $resultComments->count;

                        foreach ($resultComments->items as $oneComment) {
                            $tempComment = $tempCommentUpdate = [];
                            $tempComment['soc_network_id'] = $socNetworkId;
                            if (property_exists($oneComment, 'post_id'))
                                $tempComment['post_id'] = $oneComment->post_id;
                            if (property_exists($oneComment, 'id'))
                                $tempComment['comment_id'] = $oneComment->id;
                            if (property_exists($oneComment, 'owner_id'))
                                $tempComment['owner_id'] = $oneComment->owner_id;
                            if (property_exists($oneComment, 'from_id'))
                                $tempComment['from_user_id'] = $oneComment->from_id;
                            if (property_exists($oneComment, 'date'))
                                $tempComment['date'] = $oneComment->date;
                            if (property_exists($oneComment, 'text'))
                                $tempComment['comment_text'] = HelperFunctions::replace4ByteCharacters($oneComment->text);
                            if (property_exists($oneComment, 'likes'))
                                $tempComment['likes'] = $tempCommentUpdate['likes'] = $oneComment->likes->count;
                            $tempComment['update_time'] = $tempCommentUpdate['update_time'] = $currentTime;
                            //$tempComment['group_id'] = $commentCheck['post_group_id'];

                            if($debug) {
                                echo ++$counter;
                                echo ". Comment will be updated/inserted, owner_post_id: ".$commentCheck['owner_id']." ".$commentCheck['post_id']."\n";
                                print_r($tempComment);
                            }

                            if (isset($tempComment['from_user_id']) && intval($tempComment['from_user_id'])) {

                                if ($tempComment['from_user_id'] > 0) {
                                    $internetUser = $tempComment['from_user_id'];
                                    if (!isset($arCache['internet_users'][$internetUser])) {
                                        $iuId = (new Query())->select(['[[id]]'])
                                            ->from(InternetUser::tableName())
                                            ->where([
                                                '[[soc_network_id]]' => $socNetworkId,
                                                '[[soc_network_user_id]]' => $internetUser
                                            ])->one();
                                        if (isset($iuId['id']) && $iuId['id']) {
                                            $arCache['internet_users'][$internetUser] = $iuId['id'];
                                        }
                                    }
                                    if (isset($arCache['internet_users'][$internetUser])) {
                                        $tempComment['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                    }
                                }

                                Yii::$app->db->CreateCommand()->upsert('post_comments', $tempComment, $tempCommentUpdate)->execute();

                                self::loadVkAttachment($oneComment, $tempComment['post_id'], $tempComment['owner_id'], $tempComment['comment_id']);
                            }

                        }
                    } elseif (is_object($resultComments) && property_exists($resultComments, 'count') && ($resultComments->count == 0)) {
                        Yii::$app->db->CreateCommand()->update('posts', ['comments' => 0], ['post_id' => $commentCheck['post_id'], 'owner_id' => $commentCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                    }
                } while (count($commentsArr) && ($offsetComments <= $commentsCount));
            }
        }

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    /**
     * Get likes from special internet users pages
     */
    public function actionGetLikesOfVkPostsCommentsPagesSpec()
    {
        exec("ps aux|grep 'vk/get-likes-of-vk-posts-comments-pages-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        $currentTime = time();
        $itemsPerPage = 50;
        $likesCheck = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;
        $arCache = [];

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        if($debug) {
            $likesGet = $likesAll = 0;
            $startTime = $firstStartTime = microtime(true);
        }
        /*END DEBUG*/

        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_get_posts_from_vk_pages_spec_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $oneGroup = $makeQuery['option_value'];
        } else {
            return ExitCode::OK;
        }

        //определяем новые посты которых нет в таблице
        $likesCheckTemp = Yii::$app->db->createCommand("SELECT posts.post_id AS liked_item_id, posts.owner_id, CONCAT(posts.owner_id, '_', posts.post_id) AS post_owner_id, 'post' AS `type`, posts.group_id AS post_group_id FROM posts LEFT JOIN post_comment_likes ON (posts.owner_id = post_comment_likes.owner_id AND posts.post_id = post_comment_likes.liked_item_id) WHERE (posts.soc_network_id = {$socNetworkId}) AND (posts.owner_id = {$oneGroup}) AND (posts.likes > 0) AND (post_comment_likes.owner_id IS NULL) AND (post_comment_likes.liked_item_id IS NULL) ORDER BY posts.date ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        //определяем новые коменты которых нет в таблице
        $likesCheckTemp = Yii::$app->db->createCommand("SELECT post_comments.comment_id AS liked_item_id, post_comments.owner_id, CONCAT(post_comments.owner_id, '_', post_comments.comment_id) AS post_owner_id, 'comment' AS type, post_comments.group_id AS post_group_id FROM post_comments LEFT JOIN post_comment_likes ON (post_comments.owner_id = post_comment_likes.owner_id AND post_comments.comment_id = post_comment_likes.liked_item_id) WHERE (post_comments.soc_network_id = {$socNetworkId}) AND (post_comments.owner_id = {$oneGroup}) AND (post_comments.likes > 0) AND (post_comment_likes.owner_id IS NULL) AND (post_comment_likes.liked_item_id IS NULL) ORDER BY post_comments.date ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        //определяем записи которые есть в таблице, но информацию по которым нужно актуализировать
        $likesCheckTemp = Yii::$app->db->createCommand("SELECT DISTINCT post_comment_likes.liked_item_id, post_comment_likes.owner_id, CONCAT(post_comment_likes.owner_id, '_', post_comment_likes.liked_item_id) AS post_owner_id, post_comment_likes.type, posts.group_id AS post_group_id FROM post_comment_likes LEFT JOIN posts ON (posts.owner_id = post_comment_likes.owner_id AND posts.post_id = post_comment_likes.liked_item_id AND posts.soc_network_id = post_comment_likes.soc_network_id) WHERE (posts.up_time > {$currentTime}) AND (post_comment_likes.soc_network_id = {$socNetworkId}) AND (post_comment_likes.owner_id = {$oneGroup}) ORDER BY post_comment_likes.update_time ASC LIMIT {$itemsPerPage}")->queryAll();
        if (count($likesCheckTemp))
            $likesCheck = array_merge($likesCheck, $likesCheckTemp);

        if (count($likesCheck)) {
            foreach ($likesCheck as $likeCheck) {

                /*DEBUG*/
                if($debug) {
                    $likesGet = 0;
                }
                /*END DEBUG*/

                $offsetLikes = 0;
                do {

                    $resultLikes = VkontakteSDK::makeQuery('likes.getList', [
                        'owner_id' => $likeCheck['owner_id'],
                        'item_id' => $likeCheck['liked_item_id'],
                        'offset' => $offsetLikes,
                        'count' => 1000,
                        'extended' => 0,
                        'type' => $likeCheck['type']
                    ]);

//                    if ($offsetLikes)
//                        usleep(400000);

                    $likesArr = [];
                    $likesCount = 0;
                    if (is_object($resultLikes) && property_exists($resultLikes, 'items') && count($resultLikes->items)) {
                        $offsetLikes += 1000;
                        $likesArr = $resultLikes->items;
                        $likesCount = $resultLikes->count;

                        foreach ($resultLikes->items as $oneLike) {
                            $tempLike = $tempLikeUpdate = [];
                            $tempLike['soc_network_id'] = $socNetworkId;
                            $tempLike['liked_item_id'] = $likeCheck['liked_item_id'];
                            $tempLike['owner_id'] = $likeCheck['owner_id'];
                            $tempLike['from_user_id'] = $oneLike->id;
                            $tempLike['type'] = $likeCheck['type'];
                            $tempLike['update_time'] = $tempLikeUpdate['update_time'] = $currentTime;
                            //$tempLike['group_id'] = $likeCheck['post_group_id'];

                            /*DEBUG*/
                            if($debug) {
                                $likesGet++;
                                $likesAll++;
                            }
                            /*END DEBUG*/

                            if ($tempLike['from_user_id'] > 0) {
                                $internetUser = $tempLike['from_user_id'];
                                if (!isset($arCache['internet_users'][$internetUser])) {
                                    $iuId = (new Query())->select(['[[id]]'])
                                        ->from(InternetUser::tableName())
                                        ->where([
                                            '[[soc_network_id]]' => $socNetworkId,
                                            '[[soc_network_user_id]]' => $internetUser
                                        ])->one();
                                    if (isset($iuId['id']) && $iuId['id']) {
                                        $arCache['internet_users'][$internetUser] = $iuId['id'];
                                    }
                                }
                                if (isset($arCache['internet_users'][$internetUser])) {
                                    $tempLike['internet_users_id'] = $arCache['internet_users'][$internetUser];
                                }
                            }

                            Yii::$app->db->CreateCommand()->upsert('post_comment_likes', $tempLike, $tempLikeUpdate)->execute();
                        }
                    } elseif (is_object($resultLikes) && property_exists($resultLikes, 'count') && ($resultLikes->count == 0)) {
                        if ($likeCheck['type'] == 'post') {
                            Yii::$app->db->CreateCommand()->update('posts', ['likes' => 0], ['post_id' => $likeCheck['liked_item_id'], 'owner_id' => $likeCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                        }
                        if ($likeCheck['type'] == 'comment') {
                            Yii::$app->db->CreateCommand()->update('post_comments', ['likes' => 0], ['comment_id' => $likeCheck['liked_item_id'], 'owner_id' => $likeCheck['owner_id'], 'soc_network_id' => $socNetworkId])->execute();
                        }
                    }

                    /*DEBUG*/
                    if($debug) {
                        echo "\nQuery likes from API: OwnerID ".$likeCheck['owner_id']." / Liked ItemID ".$likeCheck['liked_item_id']." / Type ".$likeCheck['type']." / Offset ".$offsetLikes." / CountAll: ".$resultLikes->count."\n"."Time: ".gmdate("H:i:s", microtime(true) - $startTime)." / Time from start ".gmdate("H:i:s", microtime(true) - $firstStartTime)."\n";
                        $startTime = microtime(true);
                        echo "Get likes Per iteration: ".count($likesArr)."\n";
                        echo "Get likes ALL: ".$likesGet."\n";
                    }
                    /*END DEBUG*/

                } while (count($likesArr) && ($offsetLikes <= $likesCount));

                /*DEBUG*/
                if($debug) {
                    echo "Get likes SUMM: ".$likesGet."\n";
                }
                /*END DEBUG*/

            }
        }

        /*DEBUG*/
        if($debug) {
            echo "END: ".gmdate("H:i:s", microtime(true) - $startTime)."\n";
            echo "GET ALL LIKES FROM API:".$likesAll."\n";
        }
        /*END DEBUG*/

        $this->execTime('end', $arArgs);

        return ExitCode::OK;

    }

    /**
     * Calc spec internet user pages posts - events link
     */
    public function actionCreatePostsEventsIndexPagesSpec()
    {
        exec("ps aux|grep 'vk/create-posts-events-index-pages-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $this->execTime('start', func_get_args());

        /*DEBUG*/
        $arArgs = func_get_args();
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/

        $itemsPerPage = 100;
        $offset = 0;
        $checkArray = [];
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;

        /*Получаем настройку Оффсета*/
        $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_create_posts_events_index_pages_spec_offset'")->queryOne();
        if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
            $offset = intval($makeQuery['option_value']);
        } elseif (!isset($makeQuery['option_value'])) {
            Yii::$app->db->createCommand()->insert('options', [
                'option_name' => 'vk_create_posts_events_index_pages_spec_offset',
                'option_value' => '0'
            ])->execute();
        }

        /*Обновляем оффсет*/
        $maxOffset = $offset + $itemsPerPage;
        $query = Yii::$app->db->CreateCommand("
            SELECT COUNT(id) AS items_count FROM posts WHERE (
                (soc_network_id, owner_id) IN (
                    SELECT soc_network_id, soc_network_user_id AS owner_id FROM temp_spec_internet_users WHERE posts > 0
                )
            ) AND (is_parent = 0)"
        )->queryOne();
        $itemsCount = $query['items_count'];
        if ($maxOffset < $itemsCount) {
            $saveOffset = $maxOffset;
        } else {
            $saveOffset = 0;
        }
        Yii::$app->db->createCommand()->update('options', ['option_value' => $saveOffset], ['option_name' => 'vk_create_posts_events_index_pages_spec_offset'])->execute();

        /*Получаем данные для текущего шага*/
        $makeQuery = Yii::$app->db->createCommand("
            SELECT id, post_id, owner_id, soc_network_id, likes, comments, group_id, parent_post_id, parent_owner_id FROM posts WHERE (
                (soc_network_id, owner_id) IN (
                    SELECT soc_network_id, soc_network_user_id AS owner_id FROM temp_spec_internet_users WHERE posts > 0
                )
            ) AND (is_parent = 0) ORDER BY id ASC LIMIT " . $itemsPerPage . " OFFSET " . $offset
        )->queryAll();
        if (is_array($makeQuery) && count($makeQuery)) {
            foreach ($makeQuery as $onePost) {

                $checkArray[] = ['item_id' => $onePost['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $onePost['owner_id'], 'type' => 'post'];

                if ($onePost['parent_post_id'] > 0 && $onePost['parent_owner_id'] > 0) {
                    $queryParentPostId = Yii::$app->db->createCommand("SELECT id FROM posts WHERE soc_network_id = {$onePost['soc_network_id']} AND post_id = {$onePost['parent_post_id']} AND owner_id = {$onePost['parent_owner_id']}")->queryOne();
                    if ($queryParentPostId['id']) {
                        $checkArray[] = ['item_id' => $queryParentPostId['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $onePost['parent_owner_id'], 'type' => 'post', 'child_post' => $onePost['id']];
                    }
                }

                if ($onePost['comments'] > 0) {
                    $queryComments = Yii::$app->db->createCommand("SELECT id, comment_id, from_user_id, likes FROM post_comments WHERE (soc_network_id = {$onePost['soc_network_id']}) AND (owner_id = {$onePost['owner_id']}) AND (post_id = {$onePost['post_id']})")->queryAll();
                    if (is_array($queryComments) && count($queryComments)) {
                        foreach ($queryComments as $oneComment) {

                            $checkArray[] = ['item_id' => $oneComment['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $oneComment['from_user_id'], 'type' => 'comment', 'parent_post' => $onePost['id']];

                            if ($oneComment['likes'] > 0) {
                                $queryLikes = Yii::$app->db->createCommand("SELECT id, from_user_id FROM post_comment_likes WHERE (soc_network_id = {$onePost['soc_network_id']}) AND (owner_id = {$onePost['owner_id']}) AND (liked_item_id = {$oneComment['comment_id']}) AND type = 'comment'")->queryAll();

                                if (is_array($queryLikes) && count($queryLikes)) {
                                    foreach ($queryLikes as $oneLike) {

                                        $checkArray[] = ['item_id' => $oneLike['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $oneLike['from_user_id'], 'type' => 'like', 'parent_post' => $onePost['id'], 'parent_comment' => $oneComment['id']];

                                    }
                                }
                            }
                        }
                    }
                }

                if ($onePost['likes'] > 0) {
                    $queryLikes = Yii::$app->db->createCommand("SELECT id, from_user_id FROM post_comment_likes WHERE (soc_network_id = {$onePost['soc_network_id']}) AND (owner_id = {$onePost['owner_id']}) AND (liked_item_id = {$onePost['post_id']}) AND type = 'post'")->queryAll();

                    if (is_array($queryLikes) && count($queryLikes)) {
                        foreach ($queryLikes as $oneLike) {

                            $checkArray[] = ['item_id' => $oneLike['id'], 'soc_network_id' => $onePost['soc_network_id'], 'soc_network_user_id' => $oneLike['from_user_id'], 'type' => 'like', 'parent_post' => $onePost['id']];

                        }
                    }
                }
            }
        }

        if ($debug) {
            $this->stdout("Checked Items ".print_r($checkArray), Console::FG_YELLOW);
        }

        /*Поиск и добавление связей через таблицы internet_users, internet_users_galleries, galleries*/
        if (count($checkArray)) {
            //$socNetworkId = [];
            $socNetworkUserId = [];
            $checkPostsEvents = [];
            foreach ($checkArray as $oneItem) {
                //$socNetworkId[] = $oneItem['soc_network_id'];
                $socNetworkUserId[] = $oneItem['soc_network_user_id'];
            }

            //$socNetworkId = array_unique($socNetworkId);
            $allSocNetworkUserId = array_chunk(array_unique($socNetworkUserId), $itemsPerPage);

            foreach ($allSocNetworkUserId as $socNetworkUserIds) {

                $queryInternetUsersIds = Yii::$app->db->createCommand("SELECT id, soc_network_id, soc_network_user_id FROM internet_users WHERE (soc_network_id  = {$socNetworkId}) AND (soc_network_user_id IN (" . implode(',', $socNetworkUserIds) . "))")->queryAll();

                if (is_array($queryInternetUsersIds) && count($queryInternetUsersIds)) {

                    $internetUsersId = [];
                    foreach ($queryInternetUsersIds as $oneInternetUser) {
                        $internetUsersId[] = $oneInternetUser['id'];
                        foreach ($checkArray as &$oneItem) {
                            if ($oneInternetUser['soc_network_id'] == $oneItem['soc_network_id'] && $oneInternetUser['soc_network_user_id'] == $oneItem['soc_network_user_id']) {
                                $oneItem['internet_user_id'] = $oneInternetUser['id'];
                            }
                        }
                    }

                    if (count($internetUsersId)) {
                        $allInternetUsersId = array_chunk($internetUsersId, $itemsPerPage);
                        foreach ($allInternetUsersId as $internetUsersIds) {
                            $queryInternetUsersGalleriesIds = Yii::$app->db->createCommand("
                                select photos.gallery_id2, findface.internet_users_id
                                from findface 
                                inner join photos on photos.id = findface.photos_id
                                where findface.internet_users_id in (" . implode(',', $internetUsersIds) . ")  and findface.alignment != 'not alignment'
                                group by photos.gallery_id2, findface.internet_users_id
                            ")->queryAll();

                            if (is_array($queryInternetUsersGalleriesIds) && count($queryInternetUsersGalleriesIds)) {

                                $galleriesId = [];
                                foreach ($queryInternetUsersGalleriesIds as $oneInternetUsersGallery) {
                                    $galleriesId[] = $oneInternetUsersGallery['gallery_id2'];
                                    foreach ($checkArray as &$oneItem) {
                                        if (isset($oneItem['internet_user_id']) && $oneInternetUsersGallery['internet_users_id'] == $oneItem['internet_user_id']) {
                                            $oneItem['gallery_id2'] = $oneInternetUsersGallery['gallery_id2'];
                                        }
                                    }
                                }

                                if (count($galleriesId)) {
                                    $allGalleriesId = array_chunk($galleriesId, $itemsPerPage);
                                    foreach ($allGalleriesId as $galleriesIds) {

                                        $queryEventsIds = Yii::$app->db->createCommand("SELECT id, event_id FROM galleries WHERE (id IN (" . implode(',', $galleriesIds) . "))")->queryAll();

                                        if (is_array($queryEventsIds) && count($queryEventsIds)) {
                                            foreach ($queryEventsIds as $oneEventId) {
                                                foreach ($checkArray as $oneItem) {
                                                    if (isset($oneItem['gallery_id2']) && $oneItem['gallery_id2'] == $oneEventId['id'] && $oneEventId['event_id'] > 0) {
                                                        $checkPostsEvents[$oneItem['type']][$oneItem['item_id']][] = $oneEventId['event_id'];
                                                        Yii::$app->db->createCommand()->upsert('post_events', [
                                                            'item_id' => $oneItem['item_id'],
                                                            'event_id' => $oneEventId['event_id'],
                                                            'type' => $oneItem['type']
                                                        ], false)->execute();
                                                        if (isset($oneItem['child_post'])) {
                                                            $checkPostsEvents['post'][$oneItem['child_post']][] = $oneEventId['event_id'];
                                                            Yii::$app->db->createCommand()->upsert('post_events', [
                                                                'item_id' => $oneItem['child_post'],
                                                                'event_id' => $oneEventId['event_id'],
                                                                'type' => 'post'
                                                            ], false)->execute();
                                                        }
                                                        if (isset($oneItem['parent_post'])) {
                                                            $checkPostsEvents['post'][$oneItem['parent_post']][] = $oneEventId['event_id'];
                                                            Yii::$app->db->createCommand()->upsert('post_events', [
                                                                'item_id' => $oneItem['parent_post'],
                                                                'event_id' => $oneEventId['event_id'],
                                                                'type' => 'post'
                                                            ], false)->execute();
                                                        }
                                                        if (isset($oneItem['parent_comment'])) {
                                                            $checkPostsEvents['comment'][$oneItem['parent_comment']][] = $oneEventId['event_id'];
                                                            Yii::$app->db->createCommand()->upsert('post_events', [
                                                                'item_id' => $oneItem['parent_comment'],
                                                                'event_id' => $oneEventId['event_id'],
                                                                'type' => 'comment'
                                                            ], false)->execute();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /*Удаление невалидных связей*/
            foreach ($checkArray as $checkOneItem) {
                if (isset($checkPostsEvents[$checkOneItem['type']][$checkOneItem['item_id']]) &&
                    is_array($checkPostsEvents[$checkOneItem['type']][$checkOneItem['item_id']]) &&
                    count($checkPostsEvents[$checkOneItem['type']][$checkOneItem['item_id']])) {

                    $checkIsExistsLink = Yii::$app->db->createCommand("SELECT id FROM post_events WHERE item_id = {$checkOneItem['item_id']} AND type = '{$checkOneItem['type']}' AND event_id NOT IN (".implode(',', array_unique($checkPostsEvents[$checkOneItem['type']][$checkOneItem['item_id']])).")")->queryAll();

                    $itemsForDelete = [];
                    if (count($checkIsExistsLink)) {
                        foreach ($checkIsExistsLink as $existsId) {
                            $itemsForDelete[] = $existsId['id'];
                        }
                    }
                    if (count($itemsForDelete)) {
                        Yii::$app->db->createCommand("DELETE FROM post_events WHERE id = ".implode(',', $itemsForDelete))->execute();
                    }
                } else {
                    $checkIsExistsLink = Yii::$app->db->createCommand("SELECT COUNT(id) AS count FROM post_events WHERE (item_id = {$checkOneItem['item_id']}) AND (type = '{$checkOneItem['type']}')")->execute();
                    if (isset($checkIsExistsLink['count']) && $checkIsExistsLink['count']) {
                        Yii::$app->db->createCommand("DELETE FROM post_events WHERE (item_id = {$checkOneItem['item_id']}) AND (type = '{$checkOneItem['type']}')")->execute();
                    }
                }
            }
        }

        $this->execTime('end', func_get_args());

        return ExitCode::OK;
    }




    public function actionCalculateGroupsStatisticsWithAlignmentUsersSpec()
    {
        exec("ps aux|grep 'vk/calculate-groups-statistics-with-alignment-users-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $this->execTime('start', func_get_args());

        /*Получаем настройку*/
        $arResult = $arGroups = $arGroupsData = $lastPostDates = $arGroupsBySocNetworks = [];
        $params = Yii::$app->params['arCityForFilter'];

        $events = (new Events)->getEventsWithFilterableGalleries();
        $events['all'] = 'All';

        /*Получим список просматриваемых групп*/
        $makeGroupsQuery = Yii::$app->db->createCommand("SELECT * FROM (
			(SELECT 1 AS soc_network_id, vk_id AS group_id, title, members FROM vk_groups AS vkg WHERE (watch = 1) AND (vk_id IN (SELECT group_id FROM temp_spec_groups WHERE soc_network_id = 1)) ORDER BY id)
            UNION
                (SELECT 2 AS soc_network_id, ok_id AS group_id, title, members FROM ok_groups WHERE (watch = 1) AND (ok_id IN (SELECT group_id FROM temp_spec_groups WHERE soc_network_id = 2)) ORDER BY id)
            UNION
                (SELECT 3 AS soc_network_id, FB_GROUP_ID AS group_id, TITLE AS title, (SELECT COUNT(DISTINCT ACCOUNT_ID) FROM fb_group_user WHERE fb_group_user.FB_GROUP_ID = fb_groups.FB_GROUP_ID) AS members FROM fb_groups WHERE (watch = 1) AND (FB_GROUP_ID IN (SELECT group_id FROM temp_spec_groups WHERE soc_network_id = 3)) ORDER BY id)
            ) AS groups"
        )->queryAll();

        if (is_array($makeGroupsQuery) && count($makeGroupsQuery)) {
            foreach ($makeGroupsQuery as $oneGroup) {
                $arGroups[] = $oneGroup['soc_network_id'].':'.$oneGroup['group_id'];
                $arGroupsData[$oneGroup['soc_network_id'].':'.$oneGroup['group_id']]['title'] = $oneGroup['title'];
                $arGroupsData[$oneGroup['soc_network_id'].':'.$oneGroup['group_id']]['members'] = $oneGroup['members'];
                $arGroupsBySocNetworks[$oneGroup['soc_network_id']][] = $oneGroup['group_id'];
            }
        }

        if (count($arGroups)) {

            /*Получаем настройку Оффсета*/
            $currentGroup = reset($arGroups);
            $makeQuery = Yii::$app->db->createCommand("SELECT option_value FROM options WHERE option_name = 'vk_calculate_groups_statistics_with_alignment_users_spec_offset'")->queryOne();
            if (isset($makeQuery['option_value']) && !empty($makeQuery['option_value'])) {
                $currentGroup = $makeQuery['option_value'];
            } elseif (!isset($makeQuery['option_value'])) {
                Yii::$app->db->createCommand()->insert('options', [
                    'option_name' => 'vk_calculate_groups_statistics_with_alignment_users_spec_offset',
                    'option_value' => reset($arGroups)
                ])->execute();
            }

            /*Обновляем оффсет*/
            $currentGroupKey = array_search($currentGroup, $arGroups);
            if ($currentGroupKey == (count($arGroups)-1)) {
                $saveNewGroup = reset($arGroups);
            } else {
                $saveNewGroup = $arGroups[$currentGroupKey+1];
            }
            Yii::$app->db->createCommand()->update('options', ['option_value' => $saveNewGroup], ['option_name' => 'vk_calculate_groups_statistics_with_alignment_users_spec_offset'])->execute();

            /*Получим дату последней публикации cо всех групп*/
            foreach ($arGroups as $oneGroup) {
                $socNetworkId = explode(':', $oneGroup)[0];
                $groupId = explode(':', $oneGroup)[1];

                $makeQuery = Yii::$app->db->createCommand("
                    SELECT date 
                    FROM posts 
                    WHERE (soc_network_id = {$socNetworkId}) 
                        AND (group_id = {$groupId}) 
                    ORDER BY date DESC LIMIT 1")->queryOne();

                if (isset($makeQuery['date'])&&intval($makeQuery['date'])) {
                    $postDate = floor($makeQuery['date']/(3600*24))*3600*24;
                } else {
                    $postDate = '';
                }
                $lastPostDates[$socNetworkId . '_' . $groupId] = $postDate;
            }
            /**/

            $socNetworkId = explode(':', $currentGroup)[0];
            $groupId = explode(':', $currentGroup)[1];

            /*Подсчет числа публикаций (Одинаково для всех вариантов фильтра так как в выборке не участвуют авторы публикаций)*/
            $queryPosts = Yii::$app->db->createCommand("
                SELECT COUNT(posts.id) AS count_posts 
                FROM posts 
                INNER JOIN temp_spec_internet_users ON (
						temp_spec_internet_users.soc_network_id = posts.soc_network_id 
                    AND temp_spec_internet_users.soc_network_user_id = posts.owner_id)
                WHERE (posts.soc_network_id = {$socNetworkId}) 
                    AND (posts.group_id = {$groupId});
            ")->queryOne();
            /*Подсчет числа репостов (Одинаково для всех вариантов фильтра так как в выборке не участвуют авторы публикаций)*/
            $queryReposts = Yii::$app->db->createCommand("
                SELECT COUNT(posts.id) AS count_posts 
				FROM posts 
                INNER JOIN temp_spec_internet_users ON (
						temp_spec_internet_users.soc_network_id = posts.soc_network_id  
					AND temp_spec_internet_users.soc_network_user_id = posts.owner_id)
				WHERE (posts.soc_network_id = {$socNetworkId}) 
                    AND (posts.group_id = {$groupId}) 
                    AND (posts.parent_owner_id IS NOT NULL) 
                    AND (posts.parent_post_id IS NOT NULL);
            ")->queryOne();

            if (count($params)) {
                foreach ($events as $oneEventFilter => $eventValue) {

                    /*Фильтрация по мероприятиям*/
                    $eventQuery = " AND (id IN (
                        SELECT internet_user_id FROM temp_spec_internet_users)
                    )";
                    $innerJoinEventQuery = " INNER JOIN (
                        SELECT soc_network_id AS spec_soc_network_id, 
                        soc_network_user_id AS spec_soc_network_user_id 
                        FROM temp_spec_internet_users
                        ) temp_spec_internet_users ON (
                              temp_spec_internet_users.spec_soc_network_id = internet_users.soc_network_id  
                          AND temp_spec_internet_users.spec_soc_network_user_id = internet_users.soc_network_user_id
                    )";
                    if ($oneEventFilter != "all") {
                        /*
                         * TODO: after testing work without internet_users_galleries refactor here too
                         */
                        $temp_query = (new Query())->from(Galleries::tableName())
                            ->select([
                                '`id`',
                            ])->andWhere(['event_id' => $oneEventFilter])->indexBy('id')->all();
                        if (!empty($temp_query)) {
                            $filter_galleries = array_keys($temp_query);
                            $eventQuery .= " AND (id IN 
                                (
                                select distinct(internet_users_id)
                                    from photos 
                                    inner join findface on findface.photos_id = photos.id
                                    where photos.gallery_id2 in (".implode(',', $filter_galleries).")
                                ))";
                            $innerJoinEventQuery .= " inner join (
                                    select distinct(internet_users_id)
                                        from photos 
                                        inner join findface on findface.photos_id = photos.id
                                        where photos.gallery_id2 in (".implode(',', $filter_galleries).")
                                ) galleries_users on galleries_users.internet_users_id = internet_users.id";
                        }
                    }

                    foreach ($params as $oneFilter => $value) {

                        /*Конструктор запросов*/
                        switch ($oneFilter) {
                            case '0,3':
                                $cityFilter = " AND (city_id IN (0,3))";

                                /*Рассчет ВСЕГО*/
                                $sqlUseActive = " AND (city_id IN (0,3))".$eventQuery;
                                $sqlUseActiveVk = " AND (vk_users_id IN (SELECT soc_network_user_id FROM internet_users WHERE soc_network_id = 1".$sqlUseActive."))";
                                $sqlUseActiveOk = " AND (ok_users_id IN (SELECT soc_network_user_id FROM internet_users WHERE soc_network_id = 2".$sqlUseActive."))";
                                $sqlUseActiveFb = " AND (ACCOUNT_ID IN (SELECT soc_network_user_id FROM internet_users WHERE soc_network_id = 3".$sqlUseActive."))";
                                break;
                            default:
                                $cityFilter = "";

                                /*Рассчет ВСЕГО*/
                                $sqlUseActive = $eventQuery;
                                $sqlUseActiveVk = $sqlUseActiveOk = $sqlUseActiveFb = "";
                                if (!empty($eventQuery)) {
                                    $sqlUseActiveVk = " AND (vk_users_id IN (SELECT soc_network_user_id FROM internet_users WHERE soc_network_id = 1".$sqlUseActive."))";
                                    $sqlUseActiveOk = " AND (ok_users_id IN (SELECT soc_network_user_id FROM internet_users WHERE soc_network_id = 2".$sqlUseActive."))";
                                    $sqlUseActiveFb = " AND (ACCOUNT_ID IN (SELECT soc_network_user_id FROM internet_users WHERE soc_network_id = 3".$sqlUseActive."))";
                                }
                        }
                        switch ($socNetworkId) {
                            case 1:
                                $socNetworkFilter = " AND (soc_network_user_id IN (SELECT vk_users_id FROM vk_users_groups WHERE vk_groups_id = {$groupId}))";
                                $alignmentFilter = " AND (soc_network_user_id IN (SELECT DISTINCT user_id FROM findface WHERE (soc_network = 'vk') AND (alignment = 'alignment')))";
                                $socNetwork = 'VK';
                                break;
                            case 2:
                                $socNetworkFilter = " AND (soc_network_user_id IN (SELECT ok_users_id FROM ok_users_groups WHERE ok_groups_id = {$groupId}))";
                                $alignmentFilter = " AND (soc_network_user_id IN (SELECT DISTINCT user_id FROM findface WHERE (soc_network = 'ok') AND (alignment = 'alignment')))";
                                $socNetwork = 'OK';
                                break;
                            case 3:
                                $socNetworkFilter = " AND (soc_network_user_id IN (SELECT DISTINCT ACCOUNT_ID FROM fb_group_user WHERE FB_GROUP_ID = {$groupId}))";
                                $alignmentFilter = " AND (soc_network_user_id IN (SELECT DISTINCT user_id FROM findface WHERE (soc_network = 'fb') AND (alignment = 'alignment')))";
                                $socNetwork = 'FB';
                                break;
                            default:
                                $socNetworkFilter = "";
                                $alignmentFilter = "";
                                $socNetwork = "";
                        }

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['title'] = $arGroupsData[$currentGroup]['title'];
                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['members'] = $arGroupsData[$currentGroup]['members'];
                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['soc_network_id'] = $socNetworkId;
                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['group_id'] = $groupId;

                        /*Получение числа всех*/
                        $rsGroups = Yii::$app->db->createCommand("
                            SELECT COUNT(id) AS count_all 
                            FROM internet_users 
                            ".$innerJoinEventQuery."
                            WHERE (soc_network_id = {$socNetworkId})".$cityFilter.$socNetworkFilter
                        )->queryOne();

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_all'] = $rsGroups['count_all']?$rsGroups['count_all']:0;

                        /*Получение числа кто в статусе подходит*/
                        $rsGroups = Yii::$app->db->createCommand("
                            SELECT COUNT(id) AS count_alignment 
                            FROM internet_users 
                            ".$innerJoinEventQuery."
                            WHERE (soc_network_id = {$socNetworkId})".$cityFilter.$socNetworkFilter.$alignmentFilter)->queryOne();

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_alignment'] = $rsGroups['count_alignment']?$rsGroups['count_alignment']:0;

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_procent'] = $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_all'] ? round($arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_alignment'] / $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_all'], 3) * 100 : 0;
                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['last_post_date'] = $lastPostDates[$socNetworkId . '_' . $groupId];

                        /*Подсчет числа комментариев всех*/
                        $queryComments = Yii::$app->db->createCommand("
                            SELECT COUNT(id) AS count_comments 
                            FROM post_comments 
                            WHERE (soc_network_id = {$socNetworkId}) 
                                AND (from_user_id IN (
                                    SELECT soc_network_user_id 
                                    FROM internet_users 
                                    ".$innerJoinEventQuery."
                                    WHERE (soc_network_id = {$socNetworkId})".$cityFilter.")
                                ) 
                                        AND (group_id = {$groupId})")
                            ->queryOne();

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_comments_all'] = $queryComments['count_comments']?$queryComments['count_comments']:0;
                        /*Подсчет числа комментариев в статусе подходит*/
                        $queryComments = Yii::$app->db->createCommand("
                            SELECT COUNT(id) AS count_comments 
                            FROM post_comments 
                            WHERE (soc_network_id = {$socNetworkId}) 
                                AND (from_user_id IN (
                                    SELECT soc_network_user_id 
                                    FROM internet_users 
                                     ".$innerJoinEventQuery."
                                    WHERE (soc_network_id = {$socNetworkId})".$cityFilter.$alignmentFilter.")) 
                                        AND (group_id = {$groupId}
                                )")
                            ->queryOne();

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_comments_alignment'] = $queryComments['count_comments']?$queryComments['count_comments']:0;

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_comments_procent'] = $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_comments_all'] ? round($arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_comments_alignment'] / $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_comments_all'], 3) * 100 : 0;

                        /*Подсчет числа лайков всех*/
                        $queryLikes = Yii::$app->db->createCommand("
                            SELECT COUNT(id) AS count_likes 
                            FROM post_comment_likes 
                            WHERE (soc_network_id = {$socNetworkId}) 
                                AND (from_user_id IN (
                                    SELECT soc_network_user_id 
                                    FROM internet_users 
                                    ".$innerJoinEventQuery."
                                    WHERE (soc_network_id = {$socNetworkId})".$cityFilter.")
                                ) AND (group_id = {$groupId})")->queryOne();

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_likes_all'] = $queryLikes['count_likes']?$queryLikes['count_likes']:0;

                        /*Подсчет числа лайков в статусе подходит*/
                        $queryLikes = Yii::$app->db->createCommand("SELECT COUNT(id) AS count_likes 
                            FROM post_comment_likes 
                            WHERE (soc_network_id = {$socNetworkId}) 
                                AND (from_user_id IN (
                                    SELECT soc_network_user_id 
                                    FROM internet_users 
                                    ".$innerJoinEventQuery."
                                    WHERE (soc_network_id = {$socNetworkId})".$cityFilter.$alignmentFilter.")
                                ) AND (group_id = {$groupId})")->queryOne();

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_likes_alignment'] = $queryLikes['count_likes']?$queryLikes['count_likes']:0;

                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_likes_procent'] = $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_likes_all'] ? round($arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_likes_alignment'] / $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_likes_all'], 3) * 100 : 0;

                        /*Подсчет числа публикаций*/
                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_posts'] = $queryPosts['count_posts']?$queryPosts['count_posts']:0;
                        /*Подсчет числа репостов*/
                        $arResult[$oneEventFilter][$oneFilter]['items'][$socNetwork . '_' . $groupId]['count_reposts'] = $queryReposts['count_posts']?$queryReposts['count_posts']:0;

                        if ($currentGroupKey == (count($arGroups)-1)) {

                            /*Рассчет ВСЕГО*/
                            $allMemebers = $allAlignment = 0;

                            $rsGroups = Yii::$app->db->createCommand("
                            SELECT COUNT(DISTINCT vk_users_id) as members 
                            FROM vk_users_groups 
                            WHERE vk_groups_id IN (
                                SELECT vk_id 
                                FROM vk_groups 
                                WHERE (watch = 1) 
                                    AND (vk_id IN (
                                        SELECT group_id 
                                        FROM temp_spec_groups 
                                        WHERE soc_network_id = 1)
                                    )
                                )
                            " . $sqlUseActiveVk)->queryOne();
                            $allMemebers += $rsGroups['members'];

                            $rsGroups = Yii::$app->db->createCommand("
                            SELECT COUNT(DISTINCT ok_users_id) as members 
                            FROM ok_users_groups 
                            WHERE ok_groups_id IN (
                                SELECT ok_id 
                                FROM ok_groups 
                                WHERE (watch = 1) 
                                    AND (ok_id IN (
                                        SELECT group_id 
                                        FROM temp_spec_groups 
                                        WHERE soc_network_id = 2)
                                    )
                                )
                            " . $sqlUseActiveOk)->queryOne();
                            $allMemebers += $rsGroups['members'];

                            $rsGroups = Yii::$app->db->createCommand("
                            SELECT COUNT(DISTINCT ACCOUNT_ID) as members 
                            FROM fb_group_user 
                            WHERE FB_GROUP_ID IN (
                                SELECT FB_GROUP_ID 
                                FROM fb_groups 
                                WHERE (watch = 1) 
                                    AND (FB_GROUP_ID IN (
                                        SELECT group_id 
                                        FROM temp_spec_groups 
                                        WHERE soc_network_id = 3)
                                    )
                                )
                            " . $sqlUseActiveFb)->queryOne();
                            $allMemebers += $rsGroups['members'];


                            $rsAlignCount = Yii::$app->db->createCommand("
                            SELECT COUNT(DISTINCT vk_users_id) as count 
                            FROM vk_users_groups 
                            WHERE vk_groups_id IN (
                                SELECT vk_id 
                                FROM vk_groups 
                                WHERE (watch = 1) 
                                    AND (vk_id IN (
                                        SELECT group_id 
                                        FROM temp_spec_groups 
                                        WHERE soc_network_id = 1)
                                    )
                                ) 
                                AND (vk_users_id IN (
                                    SELECT soc_network_user_id 
                                    FROM internet_users 
                                    WHERE (soc_network_id = 1)
                                    " . $sqlUseActive . ")
                                ) 
                                AND (vk_users_id IN (
                                    SELECT DISTINCT user_id 
                                    FROM findface 
                                    WHERE (soc_network = 'vk') 
                                        AND (alignment = 'alignment')
                                    )
                                )
                            ")->queryOne();
                            $allAlignment += $rsAlignCount['count'];

                            $rsAlignCount = Yii::$app->db->createCommand("
                            SELECT COUNT(DISTINCT ok_users_id) as count 
                            FROM ok_users_groups 
                            WHERE ok_groups_id IN (
                                SELECT ok_id 
                                FROM ok_groups 
                                WHERE (watch = 1) 
                                    AND (ok_id IN (
                                        SELECT group_id 
                                        FROM temp_spec_groups 
                                        WHERE soc_network_id = 2)
                                    )
                                ) 
                                AND (ok_users_id IN (
                                    SELECT soc_network_user_id 
                                    FROM internet_users 
                                    WHERE (soc_network_id = 2)
                                    " . $sqlUseActive . ")
                                ) 
                                AND (ok_users_id IN (
                                    SELECT DISTINCT user_id 
                                    FROM findface 
                                    WHERE (soc_network = 'ok') 
                                        AND (alignment = 'alignment')
                                    )
                                )
                            ")->queryOne();
                            $allAlignment += $rsAlignCount['count'];

                            $rsAlignCount = Yii::$app->db->createCommand("
                            SELECT COUNT(DISTINCT ACCOUNT_ID) as count 
                            FROM fb_group_user 
                            WHERE FB_GROUP_ID IN (
                                SELECT FB_GROUP_ID 
                                FROM fb_groups 
                                WHERE (watch = 1) 
                                    AND (FB_GROUP_ID IN (
                                        SELECT group_id 
                                        FROM temp_spec_groups 
                                        WHERE soc_network_id = 3)
                                    )
                                ) 
                                AND (ACCOUNT_ID IN (
                                    SELECT soc_network_user_id 
                                    FROM internet_users 
                                    WHERE (soc_network_id = 3)
                                    " . $sqlUseActive . ")
                                ) 
                                AND (ACCOUNT_ID IN (
                                    SELECT DISTINCT user_id 
                                    FROM findface 
                                    WHERE (soc_network = 'fb') 
                                        AND (alignment = 'alignment')
                                    )
                                )
                            ")->queryOne();
                            $allAlignment += $rsAlignCount['count'];

                            $allAlignmentProc = $allMemebers ? round($allAlignment / $allMemebers, 3) * 100 : 0;

                            $arResult[$oneEventFilter][$oneFilter]['all'] = [
                                'title' => 'Всего',
                                'members' => 0,
                                'count_all' => $allMemebers,
                                'count_alignment' => $allAlignment,
                                'count_procent' => $allAlignmentProc,
                            ];
                        }
                    }
                }
            }
        }

        if (file_exists(__DIR__ . '/cronTempFiles/cronGroupsStatisticsSpec.txt')) {
            $json = file_get_contents(__DIR__ . '/cronTempFiles/cronGroupsStatisticsSpec.txt');
            $result = json_decode($json, true);

            foreach ($arResult as $eventKey => $event) {
                foreach ($event as $cityKey => $city) {
                    if (isset($result[$eventKey][$cityKey]['items']) && is_array($result[$eventKey][$cityKey]['items'])) {
                        $arResult[$eventKey][$cityKey]['items'] = array_merge($result[$eventKey][$cityKey]['items'], $arResult[$eventKey][$cityKey]['items']);
                    }
                    if (isset($result[$eventKey][$cityKey]['all']) && is_array($result[$eventKey][$cityKey]['all']) && isset($arResult[$eventKey][$cityKey]['all']) && is_array($arResult[$eventKey][$cityKey]['all'])) {
                        $arResult[$eventKey][$cityKey]['all'] = array_merge($result[$eventKey][$cityKey]['all'], $arResult[$eventKey][$cityKey]['all']);
                    } elseif (isset($result[$eventKey][$cityKey]['all']) && is_array($result[$eventKey][$cityKey]['all'])){
                        $arResult[$eventKey][$cityKey]['all'] = $result[$eventKey][$cityKey]['all'];
                    }

                    if (isset($arResult[$eventKey][$cityKey]['all']) && is_array($arResult[$eventKey][$cityKey]['all'])) {
                        $arResult[$eventKey][$cityKey]['all']['count_comments_all'] = $arResult[$eventKey][$cityKey]['all']['count_likes_all'] = $arResult[$eventKey][$cityKey]['all']['count_comments_alignment'] = $arResult[$eventKey][$cityKey]['all']['count_likes_alignment'] = $arResult[$eventKey][$cityKey]['all']['count_likes_alignment'] = $arResult[$eventKey][$cityKey]['all']['count_posts'] = $arResult[$eventKey][$cityKey]['all']['count_reposts'] = 0;
                        foreach ($arResult[$eventKey][$cityKey]['items'] as $oneItem) {
                            $arResult[$eventKey][$cityKey]['all']['count_comments_all'] += $oneItem['count_comments_all'];
                            $arResult[$eventKey][$cityKey]['all']['count_likes_all'] += $oneItem['count_likes_all'];
                            $arResult[$eventKey][$cityKey]['all']['count_comments_alignment'] += $oneItem['count_comments_alignment'];
                            $arResult[$eventKey][$cityKey]['all']['count_likes_alignment'] += $oneItem['count_likes_alignment'];
                            $arResult[$eventKey][$cityKey]['all']['count_posts'] += $oneItem['count_posts'];
                            $arResult[$eventKey][$cityKey]['all']['count_reposts'] += $oneItem['count_reposts'];
                        }
                        $arResult[$eventKey][$cityKey]['all']['count_comments_procent'] = $arResult[$eventKey][$cityKey]['all']['count_comments_all'] ? round($arResult[$eventKey][$cityKey]['all']['count_comments_alignment'] / $arResult[$eventKey][$cityKey]['all']['count_comments_all'], 3) * 100 : 0;
                        $arResult[$eventKey][$cityKey]['all']['count_likes_procent'] = $arResult[$eventKey][$cityKey]['all']['count_likes_all'] ? round($arResult[$eventKey][$cityKey]['all']['count_likes_alignment'] / $arResult[$eventKey][$cityKey]['all']['count_likes_all'], 3) * 100 : 0;
                    }
                }
            }
        }
        if (count($lastPostDates)) {
            $arResult['last_post_dates'] = $lastPostDates;
        }

        if (!file_exists(__DIR__ . '/cronTempFiles/')) {
            mkdir(__DIR__ . '/cronTempFiles/');
        }

        $handle = @fopen(__DIR__ . '/cronTempFiles/cronGroupsStatisticsSpec.txt', "w+");
        if ($handle) {
            fwrite($handle, json_encode($arResult));
            fclose($handle);
        }

        $this->execTime('end', func_get_args());

        return ExitCode::OK;
    }

    /**
     * Calc spec internet users in groups
     */
    public function actionCountGroupsInternetUsersSpec()
    {
        exec("ps aux|grep 'vk/count-groups-internet-users-spec'|grep -v 'grep\|/bin/sh -c'", $output);
        if (count($output) > $this->processCount) {
            return ExitCode::OK;
        }
        $arArgs = func_get_args();
        $this->execTime('start', $arArgs);

        /*DEBUG*/
        if (isset($arArgs) && is_array($arArgs) && array_search("debug", $arArgs) !== false) {
            $debug = true;
        } else {
            $debug = false;
        }
        /*END DEBUG*/

        $itemsPerPage = 100;
        $index = 0;
        $socNetworkId = InternetUser::SOC_NETWORK_ID_VK;

        /*Получаем настройку Оффсета*/
        $optionsQuery = (new Query())->select(['[[option_value]]'])
            ->from(Options::tableName())
            ->where(['[[option_name]]' => 'vk_count_groups_internet_users_spec_index'])
            ->one();
        if (isset($optionsQuery['option_value']) && !empty($optionsQuery['option_value'])) {
            $index = intval($optionsQuery['option_value']);
        } elseif (!isset($optionsQuery['option_value'])) {
            $newOption = new Options();
            $newOption->option_name = 'vk_count_groups_internet_users_spec_index';
            $newOption->option_value = '0';
            $newOption->save();
        }

        $makeQuery = (new Query())->select([
            '[[id]]',
            '[[group_id]]',

        ])
            ->from('temp_spec_groups')
            ->where(['>', '[[id]]', $index])
            ->andWhere(['[[soc_network_id]]' => $socNetworkId])
            ->andWhere(['[[spec_internet_users_count]]' => 0])
            ->limit($itemsPerPage)
            ->orderBy('id')
            ->all();

        /*DEBUG*/
        if ($debug) {
            echo "arGroups\n";
            print_r($makeQuery);
        }
        /*END DEBUG*/

        if (is_array($makeQuery) && count($makeQuery)) {

            $makeQueryUsers = (new Query())->select([
                '[[soc_network_user_id]]',
            ])
                ->from('temp_spec_internet_users')
                ->where(['[[soc_network_id]]' => $socNetworkId])
                ->all();
            $arUsers = [];
            foreach ($makeQueryUsers as $users) {
                $arUsers[] = $users['soc_network_user_id'];
            }

            foreach ($makeQuery as $oneGroup) {

                $makeCount = (new Query())->select([
                    'COUNT(id)',
                ])
                    ->from('vk_users_groups')
                    ->where(['[[vk_groups_id]]' => $oneGroup['group_id']])
                    ->andWhere(['in', '[[vk_users_id]]', $arUsers])
                    ->one();

                /*DEBUG*/
                if ($debug) {
                    echo $oneGroup['group_id'] . ": " . $makeCount['COUNT(id)'] . PHP_EOL;
                }
                /*END DEBUG*/

                if ($makeCount['COUNT(id)']) {
                    (new Query())->createCommand()->update(
                        '{{%temp_spec_groups}}',
                        ['[[spec_internet_users_count]]' => $makeCount['COUNT(id)']],
                        ['[[id]]' => $oneGroup['id']]
                    )->execute();
                }

                $index = $oneGroup['id'];
            }
        } else {
            $index = 0;
        }

        Options::updateAll([
            '[[option_value]]' => $index
        ], [
            '[[option_name]]' => 'vk_count_groups_internet_users_spec_index'
        ]);

        $this->execTime('end', func_get_args());

        return ExitCode::OK;
    }

}
