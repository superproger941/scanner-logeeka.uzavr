<?php
return [
    'whiteCntTitle' => 'План',
    'blackCntTitle' => 'Факт',

    'Cancel' => 'Отменить',
    'Confirm' => 'Подтвердить',
    'Save' => 'Сохранить',
    'Merge' => 'Объединить',

    'success' => 'Успешно',
    'fail' => 'Ошибка',
];
