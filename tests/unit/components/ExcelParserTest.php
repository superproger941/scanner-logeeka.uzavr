<?php

use app\components\ExcelParser;

class ExcelParserTest extends \Codeception\Test\Unit
{
    const S1VK_WALL_VIDEO_FILEPATH = 'C:\Users\sarum\Downloads\OpenServer\domains\scanner-logeeka.uzavr.ru\tests\_data\excels\s1vk_wall_video.xlsx';
    const S2VK_VIDEO = 'C:\Users\sarum\Downloads\OpenServer\domains\scanner-logeeka.uzavr.ru\tests\_data\excels\s2vk_video.xlsx';
    const S3VK_PODCAST = 'C:\Users\sarum\Downloads\OpenServer\domains\scanner-logeeka.uzavr.ru\tests\_data\excels\s3vk_podcast.xlsx';
    const S7VK_CLIP = 'C:\Users\sarum\Downloads\OpenServer\domains\scanner-logeeka.uzavr.ru\tests\_data\excels\s7vk_clip.xlsx';
    const S8VK_CLIPS = 'C:\Users\sarum\Downloads\OpenServer\domains\scanner-logeeka.uzavr.ru\tests\_data\excels\s8vk_clips.xlsx';
    const S10VK_STICK = 'C:\Users\sarum\Downloads\OpenServer\domains\scanner-logeeka.uzavr.ru\tests\_data\excels\s10vk_stick.xlsx';
    const S11VK_GL_STR = 'C:\Users\sarum\Downloads\OpenServer\domains\scanner-logeeka.uzavr.ru\tests\_data\excels\s11vk_gl_str.xlsx';

    /**
     * @var ExcelParser
     */
    private $excel;

    protected function _before()
    {
        $token = '1e9ed4fa6d80b46acde7b120a9e192566048a4521e769dfd6dfca002bb9727c131bfb1275b4c9dfca2aa7';
        $this->excel = new ExcelParser($token);
    }

    public function testWallVideo()
    {
        $generator = $this->excel->parseAndGetFunctionsWithData(self::S1VK_WALL_VIDEO_FILEPATH);
        foreach ($generator as $value) {
            $funcStrIndex = $value[0];
            $data = $value[1];
            $this->assertIsString(ExcelParser::S1VK_WALL_VIDEO, $funcStrIndex);
            $this->assertEquals(200, $data['code'], 'Response is 200');
        }
    }

    public function testVideo()
    {
        $generator = $this->excel->parseAndGetFunctionsWithData(self::S2VK_VIDEO);
        foreach ($generator as $value) {
            $funcStrIndex = $value[0];
            $data = $value[1];
            $this->assertIsString(ExcelParser::S2VK_VIDEO, $funcStrIndex);
            $this->assertEquals(200, $data['code'], 'Response is 200');
        }
    }

    public function testClip()
    {
        $generator = $this->excel->parseAndGetFunctionsWithData(self::S7VK_CLIP);
        foreach ($generator as $value) {
            $funcStrIndex = $value[0];
            $data = $value[1];
            $this->assertIsString(ExcelParser::S7VK_CLIP, $funcStrIndex);
            $this->assertEquals(200, $data['code'], 'Response is 200');
        }
    }

    public function testClips()
    {
        $generator = $this->excel->parseAndGetFunctionsWithData(self::S8VK_CLIPS);
        foreach ($generator as $value) {
            $funcStrIndex = $value[0];
            $data = $value[1];
            $this->assertIsString(ExcelParser::S8VK_CLIPS, $funcStrIndex);
            $this->assertEquals(200, $data['code'], 'Response is 200');
        }
    }

    public function testStick()
    {
        $generator = $this->excel->parseAndGetFunctionsWithData(self::S10VK_STICK);
        foreach ($generator as $value) {
            $funcStrIndex = $value[0];
            $data = $value[1];
            $this->assertIsString(ExcelParser::S10VK_STICK, $funcStrIndex);
            $this->assertEquals(200, $data['code'], 'Response is 200');
        }
    }

    public function testGroup()
    {
        $generator = $this->excel->parseAndGetFunctionsWithData(self::S11VK_GL_STR);
        foreach ($generator as $value) {
            $funcStrIndex = $value[0];
            $data = $value[1];
            $this->assertIsString(ExcelParser::S11VK_GL_STR, $funcStrIndex);
            $this->assertEquals(200, $data['code'], 'Response is 200');
        }
    }
}