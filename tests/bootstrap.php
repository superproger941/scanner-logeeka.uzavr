<?php
\Codeception\Util\Autoload::addNamespace('app', './');
\Codeception\Util\Autoload::addNamespace('SocialApiLibrary', './social_api_library');

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');

require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ .'/../vendor/autoload.php';

$config = require __DIR__ . '/../config/console.php';

//
$application = new yii\console\Application( $config );
