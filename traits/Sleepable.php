<?php

namespace app\traits;

trait Sleepable
{
    /**
     * @var int Задержка в секундах между итерациями с пустым результатом запроса
     */
    public static $sleep = 2;

    /**
     * @var int Максимальная задержка между итерациями в секундах
     */
    public static $sleepLimit = 300;

    /**
     * @var int Аккумулятор количества итераций подряд с пустой выборкой
     */
    public static $accSuccessivelyEmptyIteration = 0;

    /**
     * @param int|null $accumulator Кастомный аккумулятор
     */
    public function sleep(int $accumulator = null)
    {
        $acc = $accumulator ?? (++static::$accSuccessivelyEmptyIteration);
        $seconds = ($tmp = static::$sleep * $acc) <= static::$sleepLimit
            ? $tmp
            : static::$sleepLimit;

        usleep($seconds * 1000000);
    }

    /**
     * Сброс аккумулятора
     */
    public function resetAccumulator()
    {
        static::$accSuccessivelyEmptyIteration = 0;
    }
}
