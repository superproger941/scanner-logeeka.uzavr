<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23.09.2019
 * Time: 17:42
 */

namespace app\models\search;

use app\models\data\Findface;
use app\models\data\InternetUser;
use app\models\data\Galleries;
use app\models\data\InternetUsersStatistics;
use app\models\data\OkUsersGroups;
use app\models\data\Photos;
use app\models\data\VkUsersGroups;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\db\Expression;


class InternetUsersSearch extends InternetUser
{
    public $page = 1;
    public $pageSize = 50;
    public $totalCount = 0;
    public $events_id;
    public $galleries_id;


    public function attributes()
    {
        $attributes = parent::attributes();
        $attributes[] = 'age_min';
        $attributes[] = 'age_max';
        $attributes[] = 'events_id';
        $attributes[] = 'galleries_id';

        return array_unique($attributes);
    }

    /**
     * add for correct active form work with attributes
     */
    public function rules()
    {
        return array_merge([[['events_id'], 'integer'], [['galleries_id'], 'integer']], parent::rules());
    }

    public function search($params)
    {
        $this->pageSize = $params['per-page'] ?? $this->pageSize;
        $this->page = $params['page'] ?? $this->page;

        $main_query = (new Query())->select([
            'int_users.[[id]]',
            'int_users.[[name]]',
            'int_users.[[avatar]]',
            'int_users.[[events_count]]',
            'int_users.[[groups_count]]',
            'int_users.[[tags_count]]',
            'int_users.[[soc_network_id]]',
            'int_users.[[soc_network_user_id]]',
            'int_users.[[confirmed]]',
            'int_users.[[username]]',
            'int_users.[[proc_average_statistics]]'
        ]);

        $main_query->limit($this->pageSize);
        $main_query->offset(($this->page - 1) * $this->pageSize);

        $main_query->from(['int_users' => InternetUser::tableName()]);
        $main_query->andWhere(['int_users.[[active]]' => InternetUser::ACTIVE]);

        if (!empty($params['sort'])) {
            if (substr_count($params['sort'], '-',0,1) > 0) {
                $main_query->orderBy(['int_users.'.substr($params['sort'],1) => SORT_DESC]);
            } else {
                $main_query->orderBy(['int_users.'.$params['sort'] => SORT_ASC]);
            }
        } else {
            $main_query->orderBy([
                'int_users.[[confirmed]]' => SORT_DESC,
                'int_users.[[groups_count]]' => SORT_DESC,
                'int_users.[[events_count]]' => SORT_DESC
            ]);
        }

        if (!empty($params['InternetUsersSearch'])) {
            $search = $params['InternetUsersSearch'];

            foreach ($search as $key => $val) {
                if (!empty($val) || (is_numeric($val) && $val == 0)) {
                    switch ($key) {
                        case 'id':
                            $main_query->andWhere(['`int_users`.[[id]]' => (integer)$val]);
                            break;
                        case 'events_count':
                            $main_query->andWhere(['`int_users`.[[events_count]]' => (integer)$val]);
                            break;
                        case 'tags_count':
                            $main_query->andWhere(['`int_users`.[[tags_count]]' => (integer)$val]);
                            break;
                        case 'name':
                            if (strpos($val, ' ') !== false) {
                                $explode = explode(' ', $val);
                                $main_query->andWhere(['or', '`int_users`.[[name]] LIKE "'.$explode[0].'%'.$explode[1].'%"', '`int_users`.[[name]] LIKE "'.$explode[1].'%'.$explode[0].'%"']);
                            } else {
                                $main_query->andWhere('`int_users`.[[name]] LIKE "'.$val.'%"');
                            }
                            break;
                        case 'events_id':
                            if (!empty($search['galleries_id'])) continue 2;
                            $main_query->innerJoin(
                                [
                                    'events_id_internet_users' => (new Query())
                                        ->select(['distinct(internet_users_id)'])
                                        ->from(['findface' => Findface::tableName()])
                                        ->innerJoin(
                                            ['photos' => Photos::tableName()],
                                            'findface.[[photos_id]] = photos.[[id]]'
                                        )
                                        ->andWhere(['in', 'photos.[[gallery_id2]]' , (new Query())
                                                            ->from(Galleries::tableName())
                                                            ->select([
                                                                '`id`',
                                                            ])
                                                            ->where(['event_id' => $val])
                                                            ->andWhere(['filterable_for_photos' => 1])
                                        ])
                                        ->andWhere(['>', 'findface.[[internet_users_id]]' , 0])
                                ], 'events_id_internet_users.[[internet_users_id]] = int_users.[[id]]');
                            break;
                        case 'galleries_id':
                            $main_query->innerJoin(
                                [
                                    'events_id_internet_users' => (new Query())
                                        ->select(['distinct(internet_users_id)'])
                                        ->from(['findface' => Findface::tableName()])
                                        ->innerJoin(
                                            ['photos' => Photos::tableName()],
                                            'findface.[[photos_id]] = photos.[[id]]'
                                        )
                                        ->andWhere(['photos.[[gallery_id2]]' => $val])
                                        ->andWhere(['>', 'findface.[[internet_users_id]]' , 0])
                                ], 'events_id_internet_users.[[internet_users_id]] = int_users.[[id]]');
                            break;
                        case 'groups_id':
                            $groups_filter_array = explode(':', $val);
                            if (count($groups_filter_array) == 2) {
                                switch ($groups_filter_array[0]) {
                                    case 1:
                                        $main_query->andWhere([
                                            '`int_users`.[[soc_network_id]]' => 1,
                                            '`int_users`.[[soc_network_user_id]]' => (new Query())->select('vk_users_id')
                                                ->from(VkUsersGroups::tableName())
                                                ->where(['vk_groups_id'=> $groups_filter_array[1]])
                                        ]);
                                        break;
                                    case 2:
                                        $main_query->andWhere([
                                            '`int_users`.[[soc_network_id]]' => 2,
                                            '`int_users`.[[soc_network_user_id]]' => (new Query())->select('ok_users_id')
                                                ->from(OkUsersGroups::tableName())
                                                ->where(['ok_groups_id'=> $groups_filter_array[1]])
                                        ]);
                                        break;
                                    case 3:
                                        $main_query->andWhere([
                                            '`int_users`.`soc_network_id`' => 3,
                                            '`int_users`.`soc_network_user_id`' => (new Query())->select('ACCOUNT_ID')
                                                ->from('fb_group_user')
                                                ->where(['FB_GROUP_ID' => $groups_filter_array[1]])
                                            ]);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if (isset($params['ffAlignment']) && !empty($params['ffAlignment'])) {
                                $main_query->andWhere([
                                    'IN', '`int_users`.`soc_network_user_id`', (new Query())->select('user_id')->distinct()
                                        ->from(Findface::tableName())
                                        ->where(['alignment' => $params['ffAlignment']])
                                ]);
                            }
                            break;
                        case 'tags_id':
                            $main_query->andWhere([
                                '`int_users`.`id`' => (new Query())->select('internet_users_id')
                                    ->from('internet_users_tags')
                                    ->where(['tags_id' => $val])
                                ]);
                            break;

                        default:
                            break;
                    }
                }
            }

            /*if(!empty($search['events_id']) || !empty($search['galleries_id'])) {
                $filter_galleries = false;

                if (!empty($search['galleries_id'])) {
                    $filter_galleries = $search['galleries_id'];
                } else {
                    $temp_query = (new Query())->from(Galleries::tableName())
                        ->select([
                            '`id`',
                        ])->andWhere(['event_id' => $search['events_id']])
                        ->andWhere(['filterable_for_photos' => 1])
                        ->indexBy('id')
                        ->all();

                    if (!empty($temp_query)) {
                        $filter_galleries = array_keys($temp_query);
                    }
                }
                
                $main_query->andWhere([
                    '`int_users`.`id`' => (new Query())->select('internet_users_id')
                        ->from('internet_users_galleries')
                        ->where(['gallery_id2' => $filter_galleries])
                ]);
            }*/
        }

        if (!empty($params['average'])) {
            $main_query->addSelect([
                'average' => (new Query())->select([
                    'ratio' => 'IFNULL(ius.[[cnt_friends_alignment]], 0) +
                        IFNULL(ius.[[cnt_followers_alignment]], 0) +
                        IFNULL(ius.[[cnt_comments]], 0) +
                        IFNULL(ius.[[cnt_likes]],0) +
                        int_users.[[groups_count]] +
                        int_users.[[events_count]] +
                        int_users.[[tags_count]]'
                ])->from(['ius' => InternetUsersStatistics::tableName()])
                ->where('ius.[[internet_users_id]] = int_users.[[id]]')
            ])->andWhere(['>','int_users.[[id]]',$params['InternetUsersId']])
                ->andWhere(['!=','events_count',0])
                ->orderBy([
                    'average' => SORT_DESC,
                    'int_users.[[id]]' => SORT_ASC
                ])
                ->having(['!=', 'average', 0]);

            $result = $main_query->indexBy('id')->all();
        } else {
            $result = $main_query->indexBy('id')->all();
            //$this->totalCount = $main_query->cache(3600)-> count('int_users.[[id]]');
            $this->totalCount = $main_query->limit($this->pageSize)
                ->offset($this->page * $this->pageSize)
                ->exists()
                ? $this->pageSize * $this->page + 1
                : $this->pageSize * $this->page;
        }

        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => $this->pageSize,
                'totalCount' => $this->totalCount
            ],
            'sort' => [
                'attributes' => [
                    'id',
                    'name',
                    'groups_count',
                    'events_count',
                    'tags_count'
                ]
            ]
        ]);
    }

    public function searchForPeopleApi(int $id, $params)
    {
        $main_query = (new Query())->select([
            'int_users.*',
            'ff.id AS findface_id',
            'ff.alignment AS findface_alignment',
            'ff.photo_url as findface_photo_url',
            'ff.confidence as findface_confidence',
            'ff.photos_id as findface_photos_id'
        ]);

        $main_query->from(['ff' => Findface::tableName()]);
        $main_query->innerJoin(['int_users' => InternetUser::tableName()], "int_users.[[id]] = ff.[[internet_users_id]]");
        $main_query->where(['ff.photos_id' => (new Query())->select('photos.{{id}}')
            ->from(['photos' => Photos::tableName()])
            ->where(['people_api' => $id])
        ]);

        if (!empty($params['InternetUsersSearch'])) {
            $search = $params['InternetUsersSearch'];
            foreach ($search as $key => $val) {
                if (!empty($val)) {
                    switch ($key) {
                        case 'sex':
                            $main_query->andWhere(['`int_users`.[[sex]]' => $val]);
                            break;
                        case 'age_min':
                            $main_query->andWhere(['<', '`int_users`.[[bday]]', strtotime("-" . ((int)$val) . " year", time())]);
                            break;
                        case 'age_max':
                            $main_query->andWhere(['>', '`int_users`.[[bday]]', strtotime("-" . ((int)$val) . " year", time())]);
                            break;
                    }
                }
            }
        }

        //$main_query->andWhere(['!=', 'int_users.avatar', 'https://vk.com/images/deactivated_200.png']);
        $main_query->andWhere([
            'or',
            ['is', 'int_users.avatar',  new Expression('null')],
            ['!=', 'int_users.avatar', 'https://vk.com/images/deactivated_200.png']
        ]);
        $main_query->andWhere(['!=', 'ff.alignment', 'not alignment']);
        $main_query->limit(200);
        $main_query->groupBy('ff.[[internet_users_id]]');
        if (!empty($params['sort'])) {
            if (substr_count($params['sort'], '-',0,1) > 0) {
                $main_query->orderBy(['`int_users`.'.substr($params['sort'],1) => SORT_DESC]);
            } else {
                $main_query->orderBy(['`int_users`.'.$params['sort'] => SORT_ASC]);
            }
        } else {
            $main_query->orderBy([
                new Expression(
                    'FIELD (ff.alignment, ' . implode(',',
                        [
                            '"alignment"',
                            '"probably"',
                            '"not choose"'
                        ]
                    ) . ')'),
                'int_users.[[groups_count]]' => SORT_DESC,
                'int_users.[[events_count]]' => SORT_DESC
            ]);
        }

        /*
         * TODO: delete this, and just indexById after making fix for setting alinment for all findfaces records to people-api photos related to user
         */
        $result = [];
        $temp_result = $main_query->all();

        $sorting_array = [
            'alignment' => 3,
            'probably' => 2,
            'not choose' => 1,
            'not alignment' => 0
        ];

        foreach ($temp_result as $record) {
            if (empty($result[$record['id']])
                || $sorting_array[$record['findface_alignment']] > $sorting_array[$result[$record['id']]['findface_alignment']]
            ) {
                $result[$record['id']] = $record;
            }
        }

        return new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);
    }

    public function searchAgeGraph()
    {
        //получение результав подсчетов из крона
        if (file_exists(__DIR__."/../../commands/cronTempFiles/cronAgeChart.txt")) {
            $json = file_get_contents(__DIR__."/../../commands/cronTempFiles/cronAgeChart.txt");
            $result = json_decode($json, true);
        } else {
            $result = [0 => 0];
        }

        return new ArrayDataProvider([
            'allModels' => $result,
        ]);
    }


    public function groupPeopleData($id, $params, &$arGroupsData = []){

        $dataProvider = $this->searchForPeopleApi($id, $params);
        ///получим айдишки групп пользователей
        $arGroupsData = $arVKUsers = $arFbUsers = $arOkUsers = [];
        foreach ($dataProvider->allModels as $model) {
            if ($model['soc_network_id'] == 1) {
                $arVKUsers[] = $model['soc_network_user_id'];
            } elseif($model['soc_network_id'] == 3) {
                $arFbUsers[] = $model['soc_network_user_id'];
            } elseif($model['soc_network_id'] == 2) {
                $arOkUsers[] = $model['soc_network_user_id'];
            }
        }

        if (count($arVKUsers) > 0) {
            //получим наблюдаемые группы
            $arGroupsData['VK_GROUPS_DATA'] = (new Query())->select([
                '`vk_id`, `title`'
            ])->from('vk_groups')->where(['watch' => 1])->indexBy('vk_id')->all();

            $rsGroups = (new Query())->select([
                '`vk_users_id`, `vk_groups_id`'
            ])->from('vk_users_groups')
                ->where(['vk_groups_id' => array_keys($arGroupsData['VK_GROUPS_DATA']), 'vk_users_id' => $arVKUsers])
                ->all();
            foreach ($rsGroups as $groupData) {
                if (empty($arGroupsData['VK_USERS_GROUPS'][$groupData['vk_users_id']]) || !in_array($groupData['vk_groups_id'], $arGroupsData['VK_USERS_GROUPS'][$groupData['vk_users_id']]))
                    $arGroupsData['VK_USERS_GROUPS'][$groupData['vk_users_id']][] = $groupData['vk_groups_id'];
            }
        }

        if (count($arOkUsers) > 0) {
            //получим наблюдаемые группы
            $arGroupsData['OK_GROUPS_DATA'] = (new Query())->select([
                '`ok_id`, `title`'
            ])->from('ok_groups')->where(['watch' => 1])->indexBy('ok_id')->all();

            $rsGroups = (new Query())->select([
                '`ok_users_id`, `ok_groups_id`'
            ])->from('ok_users_groups')->where(['ok_groups_id' => array_keys($arGroupsData['OK_GROUPS_DATA']), 'ok_users_id' => $arOkUsers])->all();
            foreach ($rsGroups as $groupData) {
                if (empty($arGroupsData['OK_USERS_GROUPS'][$groupData['ok_users_id']]) || !in_array($groupData['ok_groups_id'], $arGroupsData['OK_USERS_GROUPS'][$groupData['ok_users_id']]))
                    $arGroupsData['OK_USERS_GROUPS'][$groupData['ok_users_id']][] = $groupData['ok_groups_id'];
            }
        }

        if (count($arFbUsers) > 0) {
            //получим наблюдаемые группы
            $arGroupsData['FB_GROUPS_DATA'] = (new Query())->select([
                '`FB_GROUP_ID`, `TITLE`'
            ])->from('fb_groups')->where(['watch' => 1])->indexBy('FB_GROUP_ID')->all();

            $rsGroups = (new Query())->select([
                '`ACCOUNT_ID`, `FB_GROUP_ID`'
            ])->from('fb_group_user')->where(['FB_GROUP_ID' => array_keys($arGroupsData['FB_GROUPS_DATA']), 'ACCOUNT_ID' => $arFbUsers])->all();
            foreach ($rsGroups as $groupData) {
                if (empty($arGroupsData['FB_USERS_GROUPS'][$groupData['ACCOUNT_ID']]) || !in_array($groupData['FB_GROUP_ID'], $arGroupsData['FB_USERS_GROUPS'][$groupData['ACCOUNT_ID']]))
                    $arGroupsData['FB_USERS_GROUPS'][$groupData['ACCOUNT_ID']][] = $groupData['FB_GROUP_ID'];
            }
        }

        return $dataProvider;
    }
}