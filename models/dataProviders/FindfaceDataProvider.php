<?php

namespace app\models\dataProviders;

use yii\data\BaseDataProvider;

class FindfaceDataProvider extends BaseDataProvider
{
     public $allModels;

    /**
     * Prepares the data models that will be made available in the current page.
     * @return array the available data models
     */
    protected function prepareModels()
    {
        return $this->allModels;
    }

    /**
     * Prepares the keys associated with the currently available data models.
     * @param array $models the available data models
     * @return array the keys
     */
    protected function prepareKeys($models)
    {
        // TODO: Implement prepareKeys() method.
    }

    /**
     * Returns a value indicating the total number of data models in this data provider.
     * @return int total number of data models in this data provider.
     */
    protected function prepareTotalCount()
    {
        // TODO: Implement prepareTotalCount() method.
    }
}