<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "post_attachments".
 *
 * @property int $id
 * @property int $post_id
 * @property int $attachment_id
 * @property int $owner_id
 * @property string $title
 * @property string $type
 * @property int $date
 * @property int $soc_network_id
 * @property string $access_key
 * @property string $url
 * @property string $attachment_content
 * @property int $comment_id
 *
 * @property Posts $post
 */
class PostAttachments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_attachments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_id', 'attachment_id', 'owner_id', 'date', 'soc_network_id', 'comment_id'], 'integer'],
            [['title', 'url', 'attachment_content'], 'string'],
            [['type', 'access_key'], 'string', 'max' => 255],
            [['soc_network_id', 'attachment_id', 'owner_id'], 'unique', 'targetAttribute' => ['soc_network_id', 'attachment_id', 'owner_id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Posts::className(), 'targetAttribute' => ['post_id' => 'post_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'attachment_id' => 'Attachment ID',
            'owner_id' => 'Owner ID',
            'title' => 'Title',
            'type' => 'Type',
            'date' => 'Date',
            'soc_network_id' => 'Soc Network ID',
            'access_key' => 'Access Key',
            'url' => 'Url',
            'attachment_content' => 'Attachment Content',
            'comment_id' => 'Comment ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Posts::className(), ['post_id' => 'post_id']);
    }
}
