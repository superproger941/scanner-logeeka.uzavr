<?php

namespace app\models\data;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "galleries".
 *
 * @property int $id ID
 * @property string $title Название
 * @property string $title_lat Название на латинице
 * @property string $guid
 * @property int $event_id Мероприятие
 * @property int $cnt_people
 * @property int $cnt_soc
 * @property string $type_of_camera
 * @property bool $filterable_for_photos
 * @property int $gallery_type_id
 * @property bool $etalon_gallery
 * @property bool $lm_detects_gallery
 *
 * @property Events $event
 * @property Photos[] $photos
 */
class Galleries extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%galleries}}';
    }

    /**
     * @param array $galleries_ids
     * @return array
     */
    public static function getGalleriesByIds(array $galleries_ids)
    {
        return (new Query())->select([
            'g.`id`',
            'g.`title`',
            'g.`title_lat`',
            'e.`date`'
        ])->from(['g' => self::tableName()])
            ->leftJoin(['e' => Events::tableName()],'e.`id` = g.`event_id`')
            ->where(['IN','g.`id`',$galleries_ids])
            ->indexBy('id')
            ->all();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'event_id','id',
                    'gallery_type_id',
                    'cnt_people',
                    'cnt_soc'
                ],
                'integer'
            ],
            [
                [
                    'filterable_for_photos',
                    'etalon_gallery',
                    'lm_detects_gallery',
                ],
                'boolean'
            ],
            [['title','title_lat', 'type_of_camera'], 'string', 'max' => 255],
            [
                [
                    'filterable_for_photos',
                    'etalon_gallery',
                    'lm_detects_gallery',
                ],
                'default',
                'value' => 0,
            ],
            [
                [
                    'cnt_people',
                    'cnt_soc',
                ],
                'default',
                'value' => 0,
            ],
            [
                ['gallery_type_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => GalleriesTypes::class,
                'targetAttribute' => ['gallery_type_id' => 'id']
            ],
            [
                ['event_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Events::class,
                'targetAttribute' => ['event_id' => 'id']
            ],
        ];
    }

    /**
     * @return array|false
     */
    public function attributes()
    {
        return [
            'id',
            'title',
            'title_lat',
            'guid',
            'event_id',
            'cnt_people',
            'cnt_soc',
            'type_of_camera',
            'filterable_for_photos',
            'gallery_type_id',
            'etalon_gallery',
            'lm_detects_gallery',
            'event_title',
            'photos_count',
            'social_count',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'title'        => 'Название',
            'title_lat'    => 'Название на латинице',
            'event_id'     => 'Мероприятие',
            'event_title'  => 'Мероприятия',
            'photos_count' => 'Объекты наблюдения',
            'social_count' => 'Интернет-пользователи',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Events::class, ['id' => 'event_id']);
    }

    /**
     * @return string
     */
    public function getEventTitle()
    {
        return $this->event->title;
    }

    public function getTitleLat()
    {
        return $this->title_lat;
    }

    /**
     * @return int
     */
    public function getPhotosCount()
    {
        return count($this->photos);
    }

    public function getFindfaces()
    {
        $findfaces = [];

        /** @var Photos $photo */
        foreach ($this->photos as $photo) {
            foreach ($photo->findface as $findface) {
                $findfaces[$findface->id] = $findface;
            }
        }

        return $findfaces;
    }

    /**
     * @return int
     */
    public function getFindfaceCount()
    {
        return count($this->getFindfaces());
    }

    /**
     * @return int
     */
    public function getProfilesCount()
    {
        $profiles = [];

        /** @var Findface $findface */
        foreach ($this->getFindfaces() as $findface) {
            if (!empty($findface->profile_url)){
                $profiles[] = $findface->profile_url;
            }
        }

        return count($profiles);
    }

    /**
     * @return ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photos::class, ['gallery_id' => 'id']);
    }

    /**
     * @return array|ActiveRecord[]
     */
    public static function getListMonitoringGalleries()
    {
        return self::find()
            ->where(['lm_detects_gallery' => 1])
            ->all();
    }
}
