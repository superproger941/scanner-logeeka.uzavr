<?php

namespace app\models\data;

use yii\db\ActiveRecord;

class SocNetworksApiConfig extends ActiveRecord
{
    public static function tableName()
    {
        return 'soc_networks_api_config';
    }
}
