<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "ok_users_groups".
 *
 * @property int $id
 * @property int $ok_groups_id
 * @property int $ok_users_id
 */
class OkUsersGroups extends \yii\db\ActiveRecord
{
    /**
     * Наименование поля, содержащего id юзера соц.сети
     */
    const USER_FIELD = 'ok_users_id';

    /**
     * Наименование поля, содержащего id группы соц.сети
     */
    const GROUP_FIELD = 'ok_groups_id';

    /**
     * Наименование связи в модели Findface
     */
    const FINDFACE_LINK_NAME = 'okUsersGroups';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ok_users_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ok_groups_id', 'ok_users_id'], 'integer'],
            [['ok_groups_id', 'ok_users_id'], 'unique', 'targetAttribute' => ['ok_groups_id', 'ok_users_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ok_groups_id' => 'Ok Groups ID',
            'ok_users_id' => 'Ok Users ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkGroups()
    {
        return $this->hasOne(OkGroups::class, ['id' => 'ok_groups_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkUsers()
    {
        return $this->hasOne(InternetUser::class, ['soc_network_user_id' => 'ok_users_id'])
            ->andOnCondition(['soc_network_id' => 2]);
    }
}
