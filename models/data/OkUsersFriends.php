<?php

namespace app\models\data;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "fb_users_friends".
 *
 * @property int $id
 * @property int $user_id
 * @property int $friend_id
 */
class OkUsersFriends extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ok_users_friends';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'friend_id'], 'integer'],
            [['user_id', 'friend_id'], 'unique', 'targetAttribute' => ['user_id', 'friend_id']],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => OkUsers::class,
                'targetAttribute' => ['user_id' => 'ok_id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'friend_id' => 'Friend ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(OkUsers::class, ['ok_id' => 'user_id']);
    }
}