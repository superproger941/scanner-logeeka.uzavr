<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $title
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app\city', 'ID'),
            'title' => Yii::t('app\city', 'Title'),
        ];
    }
}
