<?php

namespace app\models\data;

use app\components\okapi\OdnoklassnikiSDK;
use app\components\vkapi\VkontakteSDK;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;

class InternetUser extends ActiveRecord
{
    const TITLE = 'Интернет-пользователи';

    const SOC_NETWORK_ID_VK = 1;
    const SOC_NETWORK_ID_OK = 2;
    const SOC_NETWORK_ID_FB = 3;
    const SOC_NETWORK_ID_NS = 4;
    const SOC_NETWORK_ID_INSTA = 5;

    const SOC_NETWORK_LINKS = [
        self::SOC_NETWORK_ID_VK => 'https://vk.com/id',
        self::SOC_NETWORK_ID_OK => 'https://ok.ru/profile/',
        self::SOC_NETWORK_ID_FB => 'https://www.facebook.com/',
        self::SOC_NETWORK_ID_NS => '',
        self::SOC_NETWORK_ID_INSTA => 'https://www.instagram.com/'
    ];

    const SOC_NETWORK_SHORT_TITLES = [
        self::SOC_NETWORK_ID_VK => 'vk',
        self::SOC_NETWORK_ID_OK => 'ok',
        self::SOC_NETWORK_ID_FB => 'fb',
        self::SOC_NETWORK_ID_NS => 'ns',
        self::SOC_NETWORK_ID_INSTA => 'in',
    ];

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    const ACTUALLY = 1;
    const NOT_ACTUALLY = 0;

    const TAGS_TYPE_COMPLETE_DOSSIER = 1;
    const TAGS_TYPE_AMBASSADORS = 2;

    const CONFIRMED = 1;
    const NOT_CONFIRMED = 0;

    const NEED_UPDATE = 1;
    const NOT_NEED_UPDATE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%internet_users}}';
    }


    public static function setNeedUpdateFromFindfaceId(int $findface_id)
    {
        $findface = Findface::findOne(['id' => $findface_id]);
        if (is_object($findface)) {
            if (!empty($findface->user_id) && !empty($findface->soc_network)) {
                $soc_network = array_search($findface->soc_network, self::SOC_NETWORK_SHORT_TITLES);
                if ($soc_network) {
                    $internet_user = InternetUser::findOne(['soc_network_id' => $soc_network, 'soc_network_user_id' => $findface->user_id]);
                    if (is_object($internet_user)) {
                        $internet_user->need_update = 1;
                        $internet_user->update();
                    }
                }
            }
        } else {
            throw new InvalidArgumentException('Not found findface element by id ' . $findface_id);
        }
    }

    public static function getVKSocInternetUsers(array $ids)
    {
        $j = 0;
        do {
            if ($j > 0) {
                sleep(1);
            }
            $users = VkontakteSDK::makeQuery(
                'users.get', [
                'user_ids' => implode(',',$ids)
            ]);

            if ($j > 5) {
                die(PHP_EOL.'Not find users in vk.');
            }
            ++$j;
        } while (empty($users));

        return $users;
    }

    public static function getOKSocInternetUsers(array $ids)
    {
        $j = 0;
        do {
            if ($j > 0) {
                sleep(1);
            }
            $users = OdnoklassnikiSDK::makeRequest(
                "users.getInfo",
                [
                    'uids' => implode(',', $ids),
                    'fields' => 'accessible,blocked'
                ]
            );

            if ($j > 5) {
                die(PHP_EOL.'Not find users in vk.');
            }
            ++$j;
        } while (empty($users));

        return $users;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['avatar', 'ext_data'], 'string'],
            [
                [
                    'bday',
                    'city_id',
                    'soc_network_id',
                    'soc_network_user_id',
                    'confirmed',
                    'is_parent',
                    'parent_user_id',
                    'cnt_friends',
                    'events_count',
                    'groups_count',
                    'need_update',
                    'active',
                    'tags_count',
                    'cnt_followers',
                    'linked_people_count'
                ],
                'integer'
            ],
            [['proc_average_statistics'], 'double', 'max' => 999, 'min' => 0.000],
            [['proc_average_statistics'], 'default', 'value' => 0.000],
            [['soc_network_id', 'soc_network_user_id'], 'required'],
            [['name', 'username'], 'string', 'max' => 255],
            [['sex'], 'string', 'max' => 1],
            [
                ['soc_network_id', 'soc_network_user_id'],
                'unique',
                'targetAttribute' => ['soc_network_id', 'soc_network_user_id']
            ],
            [['soc_network_id'], 'exist', 'skipOnError' => true, 'targetClass' => SocNetwork::className(), 'targetAttribute' => ['soc_network_id' => 'id']],
        ];
    }

    public function attributes()
    {
        return [
            'id',
            'name',
            'avatar',
            'bday',
            'sex',
            'city_id',
            'soc_network_id',
            'soc_network_user_id',
            'confirmed',
            'is_parent',
            'parent_user_id',
            'cnt_friends',
            'cnt_followers',
            'events_count',
            'tags_count',
            'groups_count',
            'need_update',
            'events_id',
            'galleries_id',
            'groups_id',
            'tags_id',
            'username',
            'ext_data',
            'proc_average_statistics',
            'linked_people_count',
            'active'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/internet-users', 'ID'),
            'name' => Yii::t('app/internet-users', 'Name'),
            'avatar' => Yii::t('app/internet-users', 'Avatar'),
            'bday' => Yii::t('app/internet-users', 'Bday'),
            'city_id' => Yii::t('app/internet-users', 'City ID'),
            'soc_network_id' => Yii::t('app/internet-users', 'Soc Network ID'),
            'soc_network_user_id' => Yii::t('app/internet-users', 'Soc Network User ID'),
            'confirmed' => Yii::t('app/internet-users', 'Confirmed'),
            'is_parent' => Yii::t('app/internet-users', 'Is Parent'),
            'parent_user_id' => Yii::t('app/internet-users', 'Parent User ID'),
            'cnt_friends' => Yii::t('app/internet-users', 'Cnt Friends'),
            'events_count' => Yii::t('app/internet-users', 'Events Count'),
            'groups_count' => Yii::t('app/internet-users', 'Groups Count'),
            'need_update' => Yii::t('app/internet-users', 'Need Update'),
            'active' => Yii::t('app/internet-users', 'Active'),
            'sex' => Yii::t('app/internet-users', 'Sex'),
            'tags_count' => Yii::t('app/internet-users', 'Tags Count'),
            'cnt_followers' => Yii::t('app/internet-users', 'Cnt Followers'),
            'ext_data' => Yii::t('app/internet-users', 'Ext Data'),
            'username' => Yii::t('app/internet-users', 'Username'),
            'profile_url' => Yii::t('app/internet-users', 'Profile'),
        ];
    }


    /**
     * @return ActiveQuery
     */
    public function getFindface()
    {
        return $this->hasMany(Findface::class, ['internet_users_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSocNetwork()
    {
        return $this->hasOne(SocNetworks::class, ['id' => 'soc_network_id']);
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getVkGroups(){
        return $this->hasMany(VkGroups::class, ['id' => 'vk_groups_id'])->viaTable('vk_users_groups',  ['vk_users_id' => 'soc_network_user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Posts::class, ['internet_users_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPostComments()
    {
        return $this->hasMany(PostComments::class, ['internet_users_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPostCommentLikes()
    {
        return $this->hasMany(PostCommentLikes::class, ['internet_users_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInstaUser()
    {
        return $this->hasOne(InstaUser::class, ['id' => 'soc_network_user_id']);
    }

    /**
     * @return array
     */
    public static function getAllowedSocNetworksNames()
    {
        return [
            self::SOC_NETWORK_ID_VK => 'Вконтакте',
            self::SOC_NETWORK_ID_OK => 'Одноклассники',
            self::SOC_NETWORK_ID_FB => 'Facebook',
            self::SOC_NETWORK_ID_INSTA => 'Instagram'
        ];
    }

    /**
     * @return array
     */
    public static function getAllowedEducationSocNetwork()
    {
        return [
            self::SOC_NETWORK_ID_VK,
            self::SOC_NETWORK_ID_FB,
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getVkUser()
    {
        return $this->hasOne(VkUsers::class, ['vk_id' => 'soc_network_user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFbUser()
    {
        return $this->hasOne(FbAccounts::class, ['ACCOUNT_ID' => 'soc_network_user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOkUser()
    {
        return $this->hasOne(OkUsers::class, ['ok_id' => 'soc_network_user_id']);
    }

    /**
     * @return array
     */
    public function getWatchedGroupForWorker()
    {
        /* @var VkGroups|OkGroups|FbGroups|InstaUser $model */
        /* @var VkUsersGroups|OkUsersGroups|FbGroupUser|InstaUsersFollowers $linkTable */
        list($model, $linkTable) = self::getGroupModel($this->soc_network_id);

        if ($model === null || $linkTable === null) {
            return [];
        }

        $q = (new Query())
            ->select([
                'user_id' => new Expression('ug.' . $linkTable::USER_FIELD)
            ])
            ->from([
                'g' => $model::tableName(),
                'ug' => $linkTable::tableName()
            ])
            ->andWhere([
                'g.' . $model::SOC_FIELD => new Expression('ug.' . $linkTable::GROUP_FIELD),
                'ug.' . $linkTable::USER_FIELD => $this->soc_network_user_id,
            ]);

        $q = $model === InstaUser::class
            ? $q->andWhere(['g.is_leader' => 1,]) //Признак группы в инста юзерах
            : $q->andWhere(['g.watch' => $model::WATCH]);

        return $q->all();
    }

    /**
     * @return array|InternetUser|null
     */
    public static function getNeedUpdateModel()
    {
        return self::find()
            ->select([
                'id',
                'soc_network_id',
                'soc_network_user_id',
                'city_id',
                'linked_people_count' => PeopleApi::find()
                    ->select([
                        new Expression('COUNT(pa.id)'),
                    ])
                    ->alias('pa')
                    ->where(['pa.active' => PeopleApi::ACTIVE])
                    ->andWhere([
                        'pa.id' => Photos::find()
                            ->select(new Expression('DISTINCT(p.people_api)'))
                            ->alias('p')
                            ->innerJoinWith('peopleApi pa1', false)
                            ->where([
                                'p.id' => Findface::find()
                                    ->select('f.photos_id')
                                    ->alias('f')
                                    ->where('f.internet_users_id = iu.id')
                            ])
                            ->andWhere(['pa1.active' => PeopleApi::ACTIVE])
                            ->andWhere(['!=', 'pa1.events_count', 0])
                    ])
            ])
            ->alias('iu')
            ->where(['need_update' => self::NEED_UPDATE])
            ->limit(1)
            ->one();
    }

    /**
     * @return array|null
     */
    public static function getMaxParamsForAveragePoint()
    {
        return self::find()
            ->select([
                'groups' => new Expression('MAX(groups_count)'),
                'events' => new Expression('MAX(events_count)'),
                'tags' => new Expression('MAX(tags_count)')
            ])->distinct()
            ->where(['active' => self::ACTIVE])
            ->limit(1)
            ->asArray()
            ->one();
    }

    /**
     * @param int $group
     * @return array
     */
    public static function getGroupModel($group)
    {
        $group == self::SOC_NETWORK_ID_VK
        and $model = VkGroups::class
        and $linkTable = VkUsersGroups::class;

        $group == self::SOC_NETWORK_ID_OK
        and $model = OkGroups::class
        and $linkTable = OkUsersGroups::class;

        $group == self::SOC_NETWORK_ID_FB
        and $model = FbGroups::class
        and $linkTable = FbGroupUser::class;

        $group == self::SOC_NETWORK_ID_INSTA
        and $model = InstaUser::class
        and $linkTable = InstaUsersFollowers::class;

        return [
            $model ?? null,
            $linkTable ?? null,
        ];
    }

    /**
     * @param int $networkId
     * @return array
     */
    public static function getSocUserFriendModel($networkId)
    {
        $networkId == InternetUser::SOC_NETWORK_ID_VK
        and $model = VkUsers::class
        and $linkTable = VkUsersFriends::class;

        $networkId == InternetUser::SOC_NETWORK_ID_OK
        and $model = OkUsers::class
        and $linkTable = OkUsersFriends::class;

        $networkId == InternetUser::SOC_NETWORK_ID_FB
        and $model = FbAccounts::class
        and $linkTable = FbUsersFriends::class;

        return [
            $model ?? null,
            $linkTable ?? null,
        ];
    }

    /**
     * @param int $networkId
     * @return array
     */
    public static function getSocUserFollowerModel($networkId)
    {
        /* @var VkUsers $model */
        /* @var VkUsersFollowers $linkTable */
        $networkId == InternetUser::SOC_NETWORK_ID_VK
        and $model = VkUsers::class
        and $linkTable = VkUsersFollowers::class;

        /* @var InstaUser $model */
        /* @var InstaUsersFollowers $linkTable */
        $networkId == InternetUser::SOC_NETWORK_ID_VK
        and $model = InstaUser::class
        and $linkTable = InstaUsersFollowers::class;

        return [
            $model ?? null,
            $linkTable ?? null,
        ];
    }

    /**
     * @return int|string|null
     */
    public function getSocNetworkFriendsCount()
    {
        /* @var VkUsers|OkUsers|FbAccounts $model */
        /* @var VkUsersFriends|OkUsersFriends|FbUsersFriends $userFriendModel */
        list($model, $userFriendModel) = self::getSocUserFriendModel($this->soc_network_id);

        if (!in_array((int)$this->soc_network_id, self::getAllowedFriendsNetwork(), true)
            || $model === null
            || $userFriendModel === null
        ) {
            return null;
        }

//        $q = $userFriendModel::find()
//            ->alias('uf')
//            ->where([
//                'or',
//                ['user_id' => $this->soc_network_user_id],
//                ['friend_id' => $this->soc_network_user_id],
//            ])
//            ->count();

        return $this->{$model::INTERNET_USER_LINK_NAME}->{$model::COUNT_FRIENDS_FIELD} ?? 0;
    }

    /**
     * @return int|string|null
     */
    public function getSocNetworkFollowersCount()
    {
        /* @var VkUsers|InstaUser $model */
        /* @var VkUsersFollowers|InstaUsersFollowers $userFollowerModel */
        list($model, $userFollowerModel) = self::getSocUserFollowerModel($this->soc_network_id);

        if (!in_array((int)$this->soc_network_id, self::getAllowedFollowersNetwork(), true)
            || $model === null
            || $userFollowerModel === null
        ) {
            return null;
        }

//        $q = $userFollowerModel::find()
//            ->alias('uf')
//            ->where([
//                $userFollowerModel::LEADER_FIELD => $this->soc_network_user_id
//            ])
//            ->count();

        return $this->{$model::INTERNET_USER_LINK_NAME}->{$model::COUNT_FOLLOWERS_FIELD} ?? 0;
    }

    /**
     * @return array
     */
    public static function getIdentSocNetworksNames()
    {
        return [
            self::SOC_NETWORK_ID_VK => 'vk',
            self::SOC_NETWORK_ID_FB => 'facebook',
            self::SOC_NETWORK_ID_INSTA => 'instagram'
        ];
    }

    /**
     * @return array
     */
    public static function getAllowedFriendsNetwork()
    {
        return [
            self::SOC_NETWORK_ID_VK,
            self::SOC_NETWORK_ID_OK,
            self::SOC_NETWORK_ID_FB,
        ];
    }

    /**
     * @return array
     */
    public static function getAllowedFollowersNetwork()
    {
        return [
            self::SOC_NETWORK_ID_VK,
            self::SOC_NETWORK_ID_INSTA
        ];
    }
}
