<?php

namespace app\models\data;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ok_users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $avatar
 * @property int $ok_id
 * @property int $cnt_friends
 * @property int $findface
 * @property int $date
 * @property int $groups
 * @property int $friends
 * @property int $bday
 * @property int $city_id
 */
class OkUsers extends ActiveRecord
{
    /**
     * Наименование связи в модели InternetUser
     */
    const INTERNET_USER_LINK_NAME = 'okUser';

    /**
     * Наименование поля, содержащего id в соц.сети
     */
    const SOC_FIELD = 'ok_id';

    /**
     * Наименование поля, содержащего количество друзей
     */
    const COUNT_FRIENDS_FIELD = 'cnt_friends';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ok_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ok_id', 'cnt_friends', 'findface', 'date', 'groups', 'friends', 'bday', 'city_id',], 'integer'],
            [['groups', 'friends'], 'required'],
            [['first_name', 'last_name', 'avatar'], 'string', 'max' => 255],
            [['ok_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app\ok-users', 'ID'),
            'first_name' => Yii::t('app\ok-users', 'First Name'),
            'last_name' => Yii::t('app\ok-users', 'Last Name'),
            'avatar' => Yii::t('app\ok-users', 'Avatar'),
            'ok_id' => Yii::t('app\ok-users', 'Vk ID'),
            'cnt_friends' => Yii::t('app\ok-users', 'Cnt Friends'),
            'findface' => Yii::t('app\ok-users', 'Findface'),
            'date' => Yii::t('app\ok-users', 'Date'),
            'groups' => Yii::t('app\ok-users', 'Groups'),
            'friends' => Yii::t('app\ok-users', 'Friends'),
            'bday' => Yii::t('app\ok-users', 'Bday'),
            'city_id' => Yii::t('app\ok-users', 'City ID'),
        ];
    }

    /**
     * Получение друзей-получателей заявки в друзья
     * (тех, кто получил заявку)
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getSenderFriends()
    {
        return $this->hasMany(OkUsers::class, ['ok_id' => 'friend_id'])
            ->viaTable(OkUsersFriends::tableName(), ['user_id' => 'ok_id']);
    }

    /**
     * Получение друзей-отправителей заявки в друзья
     * (тех, кто отправил заявку)
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getRecipientFriends()
    {
        return $this->hasMany(OkUsers::class, ['ok_id' => 'user_id'])
            ->viaTable(OkUsersFriends::tableName(), ['friend_id' => 'ok_id']);
    }
}
