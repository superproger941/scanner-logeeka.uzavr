<?php

namespace app\models\data;

use Yii;

/**
 * This is the model class for table "temp_users_alignment_users_link".
 *
 * @property int $id
 * @property string $type
 * @property int $internet_user_id
 * @property int $alignment_internet_user_id
 */
class TempUsersAlignmentUsersLink extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp_users_alignment_users_link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['internet_user_id', 'alignment_internet_user_id'], 'integer'],
            [['type'], 'string', 'max' => 2],
            [['type', 'internet_user_id', 'alignment_internet_user_id'], 'unique', 'targetAttribute' => ['type', 'internet_user_id', 'alignment_internet_user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'internet_user_id' => 'Internet User ID',
            'alignment_internet_user_id' => 'Alignment Internet User ID',
        ];
    }
}
