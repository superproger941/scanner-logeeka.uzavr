<?php

namespace app\models\data;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class VkUsers extends ActiveRecord
{
    /**
     * Наименование связи в модели InternetUser
     */
    const INTERNET_USER_LINK_NAME = 'vkUser';

    /**
     * Наименование поля, содержащего количество друзей
     */
    const COUNT_FRIENDS_FIELD = 'cnt_friends';

    /**
     * Наименование поля, содержащего количество подписчиков
     */
    const COUNT_FOLLOWERS_FIELD = 'cnt_followers';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vk_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vk_id', 'cnt_friends', 'findface', 'date', 'groups', 'friends', 'bday', 'city_id', 'job_id', 'univ_id', 'cnt_followers'], 'integer'],
            [['groups', 'error_groups', 'friends'], 'required'],
            [['first_name', 'last_name', 'avatar'], 'string', 'max' => 255],
            [['error_groups', 'error_friends'], 'string', 'max' => 100],
            [['vk_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app\vk-users', 'ID'),
            'first_name' => Yii::t('app\vk-users', 'First Name'),
            'last_name' => Yii::t('app\vk-users', 'Last Name'),
            'avatar' => Yii::t('app\vk-users', 'Avatar'),
            'vk_id' => Yii::t('app\vk-users', 'Vk ID'),
            'cnt_friends' => Yii::t('app\vk-users', 'Cnt Friends'),
            'findface' => Yii::t('app\vk-users', 'Findface'),
            'date' => Yii::t('app\vk-users', 'Date'),
            'groups' => Yii::t('app\vk-users', 'Groups'),
            'error_groups' => Yii::t('app\vk-users', 'Error Groups'),
            'friends' => Yii::t('app\vk-users', 'Friends'),
            'error_friends' => Yii::t('app\vk-users', 'Error Friends'),
            'bday' => Yii::t('app\vk-users', 'Bday'),
            'city_id' => Yii::t('app\vk-users', 'City ID'),
            'job_id' => Yii::t('app\vk-users', 'Job ID'),
            'univ_id' => Yii::t('app\vk-users', 'Univ ID'),
            'cnt_followers' => Yii::t('app\vk-users', 'Cnt Followers'),
        ];
    }

    public function getInternetUser()
    {
        return $this->hasOne(InternetUser::class, ['soc_network_user_id' => 'vk_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEducation()
    {
        return $this->hasOne(University::class, ['id' => 'univ_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWork()
    {
        return $this->hasOne(Jobs::class, ['id' => 'job_id']);
    }

    /**
     * Получение друзей-получателей заявки в друзья
     * (тех, кто получил заявку)
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getSenderFriends()
    {
        return $this->hasMany(VkUsers::class, ['vk_id' => 'friend_id'])
            ->viaTable(VkUsersFriends::tableName(), ['user_id' => 'vk_id']);
    }

    /**
     * Получение друзей-отправителей заявки в друзья
     * (тех, кто отправил заявку)
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getRecipientFriends()
    {
        return $this->hasMany(VkUsers::class, ['vk_id' => 'user_id'])
            ->viaTable(VkUsersFriends::tableName(), ['friend_id' => 'vk_id']);
    }

    /**
     * Подписки на этого юзера
     *
     * @return ActiveQuery
     */
    public function getFollowersUsers()
    {
        return $this->hasMany(VkUsersFollowers::class, ['user_id' => 'vk_id']);
    }

    /**
     * Подписка на других юзеров
     *
     * @return ActiveQuery
     */
    public function getFollowersUsersBySelfFollower()
    {
        return $this->hasMany(VkUsersFollowers::class, ['follower_id' => 'vk_id']);
    }

    public static function saveUsers($users)
    {
        foreach ($users as $user) {
            $obj = self::find()
                ->where(['vk_id' => $user['id']])
                ->one();
            if (!$obj) {
                $obj = new self();
            }
            //id,first_name,last_name,photo_max_orig,
            $obj->vk_id = $user['id'];
            $obj->first_name = $user['first_name'];
            $obj->last_name = $user['last_name'];
            $obj->avatar = $user['photo_max_orig'] ?? '';
            //TODO
            $obj->cnt_friends = !empty($user['counters']['friends']) ? $user['counters']['friends'] : 0;
            //TODO
            $obj->findface = 0;//$user['id'];
            //TODO
            $obj->date = (int)($user['bdate'] ?? 0);
            $obj->groups = !empty($user['counters']['groups']) ? $user['counters']['groups'] : 1;
            //TODO
            $obj->error_groups = 'none';
            //TODO
            $obj->friends = !empty($user['counters']['friends']) ? $user['counters']['friends'] : 0;
            //TODO
            $obj->error_friends = 'none';
            //TODO
            $obj->bday = (int)($user['bdate'] ?? 0);
            $obj->city_id = !empty($user['city']['id']) ? $user['city']['id'] : null;
            //TODO
            $obj->job_id = '';
            //TODO
            $obj->univ_id = '';
            $obj->cnt_followers = !empty($user['counters']['followers']) ? $user['counters']['followers'] : 0;
            $obj->save();
        }
    }
}
