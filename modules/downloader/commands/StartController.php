<?php

namespace app\modules\downloader\commands;


use app\modules\downloader\models\data\Downloader;
use Throwable;
use yii\console\Controller;
use yii\console\ExitCode;

class StartController extends Controller
{
    /**
     * Run curl request to download files from downloaders table
     * @return int
     */
    public function actionIndex()
    {
        $this->stdout('start'.PHP_EOL);
        $models = Downloader::find()->where(['startTime' => null])->all();
        $this->stdout('get model'.PHP_EOL);
        foreach ($models as $model) {
            $this->stdout('foreach model'.PHP_EOL);

            $this->stdout('check start'.PHP_EOL);

            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $this->stdout('start transaction'.PHP_EOL);

                $model->startTime = time();
                $this->stdout('setup model'.PHP_EOL);

                $this->stdout('start download'.PHP_EOL);
                exec('curl '.$model->from.' --output /data/downloader/'.$model->to);
                $this->stdout('save model'.PHP_EOL);

                $model->save();
                $transaction->commit();
            } catch (Throwable $e) {
                $this->stdout('cant download'.PHP_EOL);

                $transaction->rollBack();
                return ExitCode::CANTCREAT;
            }
        }
        $this->stdout('end conversation'.PHP_EOL);

        return ExitCode::OK;
    }
}