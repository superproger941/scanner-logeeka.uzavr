<?php

namespace app\modules\downloader\controllers;

use app\modules\downloader\models\data\Downloader;
use app\modules\downloader\models\search\DownloaderSearch;
use Yii;
use yii\web\Controller;

class DefaultController extends Controller
{
    public $layout = 'main';

    public function actionIndex()
    {
        $model = new Downloader();
        if (!empty(Yii::$app->request->post())) {
            $model->load(Yii::$app->request->bodyParams);

            if (substr($model->to, 0, 1) == '/') {
                $model->to = substr($model->to, 1);
            }

            $model->validate();
            if (!$model->hasErrors()) {
                $model->save();
            }
        }

        $searchModel = new DownloaderSearch();
        $dataProvider = $searchModel->search();

        return $this->render('index',[
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionDelete($id)
    {
        $item = Downloader::findOne($id);
        $item->delete();

        return $this->redirect('/downloader');
    }
}