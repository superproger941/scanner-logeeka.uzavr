<?php

namespace app\modules\downloader\models\data;

use yii\db\ActiveRecord;

class Downloader extends ActiveRecord
{
    public static function tableName()
    {
        return 'downloaders';
    }

    public function attributes()
    {
        return [
            'id',
            'from',
            'to',
            'addTime',
            'startTime',
            'fileSize'
        ];
    }

    public function attributeLabels()
    {
        return [
            'from' => 'Путь для скачивания',
            'to' => 'Где сохранить',
            'addTime' => 'Добавлена запись',
            'startTime' => 'Началось скачивание',
            'fileSize' => 'Размер файла'
        ];
    }


    public function rules()
    {
        return [
            [['from'], 'required'],
            [['to','from'], 'string'],
        ];
    }


}