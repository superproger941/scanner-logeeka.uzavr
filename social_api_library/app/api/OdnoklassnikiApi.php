<?php

namespace SocialApiLibrary\api;

use SocialApiLibrary\sdk\OdnoklassnikiSDK;

class OdnoklassnikiApi extends SocialApi
{
    protected $anchor;
    protected $pagingAnchor;
    private $sdk;

    public function __construct($options)
    {
        parent::__construct($options);
        $this->sdk = new OdnoklassnikiSDK($this->options);
    }

    public function getGroupUsers(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $res = $this->sdk->makeRequest(
                'group.getMembers',
                $params
            );

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getGroupById(array $params = []): ?array
    {
        return $this->methodDoesNotExists();
    }

    public function getUserInfo(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $params['uids'] = implode(',', $params['uids']);
            $params['fields'] = 'first_name,last_name,age,birthday,pic600x600,pic_max,location,current_location,common_friends_count,gender';
            $request = $this->sdk->makeRequest(
                'users.getInfo',
                $params
            );

            return $this->response($request);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getUserGroups(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $this->sdk->setIsInternalSetting(true);
            $res = $this->sdk->makeRequest('group.getUserGroupsV2', $params);

            return $this->response($res);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getUserFriends(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $request = $this->sdk->makeRequest(
                'friends.get',
                $params
            );

            return $this->response($request);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getUserFollowers(array $params = []): ?array
    {
        return $this->methodDoesNotExists();
    }

    public function getGroupPosts(array $params = []): ?array
    {
        return $this->methodDoesNotExists();
    }

    public function getGroupPostsById(array $params = []): ?array
    {
        return $this->methodDoesNotExists();
    }

    public function getPostLikes(array $params = [], $type = self::LIKES_TYPE_POST): ?array
    {
        return $this->methodDoesNotExists();
    }

    public function getPostComments(array $params = []): ?array
    {
        return $this->methodDoesNotExists();
    }

    public function getPostAttachments(array $params = []): ?array
    {
        return $this->methodDoesNotExists();
    }

    public function getAlbums(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $request = $this->sdk->makeRequest(
                'photos.getAlbums',
                $params
            );

            return $this->response($request);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getPhotos(array $params = []): ?array
    {
        try {
            $params = $this->addOffsetAndCountToSendData($params);
            $request = $this->sdk->makeRequest(
                'photos.getPhotos',
                $params
            );

            return $this->response($request);
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function getVideo(array $params = []): ?array
    {
        return $this->methodDoesNotExists();
    }

    private function methodDoesNotExists(): ?array
    {
        try {
            throw new \Exception('Method doesn\'t exists');
        } catch (\Exception $e) {
            return $this->error($e->getMessage() . ' ' . $e->getTraceAsString(), $e->getCode());
        }
    }

    public function setOffsetAndCount($offset, $count)
    {
        if (!empty($context->count)) {
            $this->count = $context->count;
        }
        if (!empty($context->offset)) {
            $this->anchor = $context->offset;
            $this->pagingAnchor = $context->offset;
        }
    }

    public function addOffsetAndCountToSendData($params)
    {
        if (!empty($this->count)) {
            $params['count'] = $this->count;
        }
        if (!empty($this->offset)) {
            $params['anchor'] = $this->offset;
            $params['pagingAnchor'] = $this->offset;
        }

        return $params;
    }

    public function response($data): ?array
    {
        return [
            'success' => true,
            'result' => $data,
            'code' => 200,
            'offset' => $data['anchor'] ?? $data['pagingAnchor'] ?? 0,
            'has_more_pages' => $data['has_more'] ?? false
        ];
    }
}