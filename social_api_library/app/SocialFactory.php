<?php

namespace SocialApiLibrary;

use SocialApiLibrary\api\OdnoklassnikiApi;
use SocialApiLibrary\api\SocialApi;
use SocialApiLibrary\api\VkontakteApi;

final class SocialFactory
{
    public static function factory($type, $options = []): SocialApi
    {
        switch ($type) {
            case SocialApi::ID_VK:
                return new VkontakteApi($options);
            case SocialApi::ID_OK:
                return new OdnoklassnikiApi($options);
        }

        throw new \InvalidArgumentException('Unknown social network type: ' . $type);
    }
}
