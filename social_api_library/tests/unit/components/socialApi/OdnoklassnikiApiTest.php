<?php

use SocialApiLibrary\api\SocialApi;
use SocialApiLibrary\SocialFactory;

class OdnoklassnikiApiTest extends \Codeception\Test\Unit
{
    protected $tester;

    private $okSocial;

    protected function _before()
    {
        $this->okSocial = SocialFactory::factory(SocialApi::ID_OK, [
            'id' => 3,
            'app_id' => '512000051505',
            'app_public_key' => 'CCDKMFJGDIHBABABA',
            'app_secret_key' => 'CF213F6D9C94D89966E39558',
            'access_token' => 'tkn1YBPEYmBf2HJ7RKvoQgDkepMHovgEUcY3PgOORZbGQwgl9ec73ihykcwCduWFvLEZH',
            'refresh_token' => '0201875532046c5186f8e24a640667b0',
        ]);
        $this->okSocial->setOffsetAndCount(null, null);
    }

    public function testGetGroupUsers()
    {
        $groupId = '52902831849701';
        $groupsUsers = $this->okSocial->getGroupUsers(['uid' => $groupId]);
        $this->assertEquals(200, $groupsUsers['code'], 'Response is 200');
    }

    public function testGetUserInfo()
    {
        $userId = '633672982';
        $users = $this->okSocial->getUserInfo(['uids' => [$userId]]);
        $this->assertEquals(200, $users['code'], 'Response is 200');
    }

    public function testGetUserGroups()
    {
        $userId = '633672982';
        $users = $this->okSocial->getUserGroups(['uid' => $userId]);
        $this->assertEquals(200, $users['code'], 'Response is 200');
    }

    public function testGetUserFriends()
    {
        $userId = '633672982';
        $users = $this->okSocial->getUserFriends([
            'uid' => $userId,
            'fid' => $userId,
        ]);
        $this->assertEquals(200, $users['code'], 'Response is 200');
    }


    public function testGetUserFollowers()
    {
        $this->assertEquals(200, 200, 'Response is 200');
    }

    public function testGetGroupPosts()
    {
        $this->assertEquals(200, 200, 'Response is 200');
    }

    public function testGetPostLikes()
    {
        $this->assertEquals(200, 200, 'Response is 200');
    }

    public function testGetPostComments()
    {
        $this->assertEquals(200, 200, 'Response is 200');
    }

    public function testGetPostAttachments()
    {
        $this->assertEquals(200, 200, 'Response is 200');
    }

    public function testGetAlbums()
    {
        $ownerId = '633672982';
        $albums = $this->okSocial->getAlbums([
            'fid' => $ownerId,
        ]);
        $this->assertEquals(200, $albums['code'], 'Response is 200');
    }

    public function testGetPhotosByAlbumId()
    {
        $ownerId = '543664902941';
        $albumId = '424635461661';
        $photos = $this->okSocial->getPhotos([
            'fid' => $ownerId,
            'aid' => $albumId,
        ]);
        $this->assertEquals(200, $photos['code'], 'Response is 200');
    }
}