<?php

use SocialApiLibrary\api\SocialApi;
use SocialApiLibrary\SocialFactory;

class VkontakteApiTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    private $vkSocial;

    protected function _before()
    {
        $this->vkSocial = SocialFactory::factory(
            SocialApi::ID_VK,
            [
                'offset' => 0,
                'access_token' => 'ff5ae2d8a1b8b7eecd4ccf678d5e76501a83da5a15884e622df96957f85366527d2f236772f28ee8bd25b',
            ]
        );
        $this->vkSocial->setOffsetAndCount(null, null);
    }

    protected function _after()
    {
    }

    public function testGetGroupUsers()
    {
        $groupId = '150681028';
        $groupsUsers = $this->vkSocial->getGroupUsers(['group_id' => $groupId]);
        $this->assertEquals(200, $groupsUsers['code'], 'Response is 200');
    }

    public function testGetUserInfo()
    {
        $userId = '181892800';
        $users = $this->vkSocial->getUserInfo(['user_ids' => [$userId]]);
        $this->assertEquals(200, $users['code'], 'Response is 200');
    }

    public function testGetUserGroups()
    {
        $userId = '181892800';
        $users = $this->vkSocial->getUserGroups(['user_id' => $userId]);
        $this->assertEquals(200, $users['code'], 'Response is 200');
    }

    public function testGetUserFriends()
    {
        $userId = '181892800';
        $users = $this->vkSocial->getUserFriends(['user_id' => $userId]);
        $this->assertEquals(200, $users['code'], 'Response is 200');
    }

    public function testGetUserFollowers()
    {
        $userId = '181892800';
        $users = $this->vkSocial->getUserFollowers(['user_id' => $userId]);
        $this->assertEquals(200, $users['code'], 'Response is 200');
    }

    public function testGetGroupPosts()
    {
        $groupId = '150681028';
        $groupsUsers = $this->vkSocial->getGroupPosts([
            'owner_id' => '-' . $groupId,
        ]);
        $this->assertEquals(200, $groupsUsers['code'], 'Response is 200');
    }

    public function testGetGroupPostById()
    {
        $groupId = '195731806';
        $postId = '13';
        $posts = $this->vkSocial->getGroupPostsById([
            'posts' => '-' . $groupId . '_' . $postId,
        ]);
        $this->assertEquals(200, $posts['code'], 'Response is 200');
    }


    public function testGetPostLikes()
    {
        $groupId = '150681028';
        $postId = '70617';
        $likes = $this->vkSocial->getPostLikes([
            'owner_id' => '-' . $groupId,
            'post_id' => $postId,
        ]);
        $this->assertEquals(200, $likes['code'], 'Response is 200');
    }

    public function testGetPostComments()
    {
        $groupId = '150681028';
        $postId = '70617';
        $comments = $this->vkSocial->getPostComments([
            'owner_id' => '-' . $groupId,
            'post_id' => $postId,
        ]);
        $this->assertEquals(200, $comments['code'], 'Response is 200');
    }

    public function testGetPostAttachments()
    {
        $groupId = '150681028';
        $postId = '70617';
        $attachments = $this->vkSocial->getPostAttachments([
            'owner_id' => '-' . $groupId,
            'post_id' => $postId,
        ]);
        $this->assertEquals(200, $attachments['code'], 'Response is 200');
    }

    public function testGetAlbums()
    {
        $ownerId = '181892800';
        $albums = $this->vkSocial->getAlbums([
            'owner_id' => '-' . $ownerId,
        ]);
        $this->assertEquals(200, $albums['code'], 'Response is 200');
    }

    public function testGetPhotos()
    {
        $ownerId = '181892800';
        $albumId = '4880451';
        $photos = $this->vkSocial->getPhotos([
            'owner_id' => '-' . $ownerId,
            'album_id' => $albumId,
        ]);
        $this->assertEquals(200, $photos['code'], 'Response is 200');
    }
}