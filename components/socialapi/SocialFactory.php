<?php

namespace app\components\socialapi;

use app\models\data\InternetUser;

final class SocialFactory
{
    public static function factory($type): SocialInterface
    {
        $socNetworkId = array_search($type, InternetUser::SOC_NETWORK_SHORT_TITLES);
        if (is_int($type) && $socNetworkId === false) {
            $socNetworkId = $type;
        }
        switch ($socNetworkId) {
            case InternetUser::SOC_NETWORK_ID_VK:
                return new Vkontakte();
                break;
            case InternetUser::SOC_NETWORK_ID_OK:
                return new Odnoklassniki();
                break;
        }

        throw new \InvalidArgumentException('Unknown social network type: '.$type);
    }
}
