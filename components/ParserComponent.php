<?php

namespace app\components;

use SocialApiLibrary\api\SocialApi;
use SocialApiLibrary\api\VkontakteApi;
use SocialApiLibrary\SocialFactory;

class ParserComponent
{
    /**
     * @var VkontakteApi
     */
    private $vkSocial;

    public function __construct($token)
    {
        $this->vkSocial = SocialFactory::factory(
            SocialApi::ID_VK,
            [
                'access_token' => $token,
            ]
        );
    }

    public function getUnparsedPostLikesVk($ownerId, $itemId)
    {
        return $this->vkSocial->getPostLikes([
            'owner_id' => $ownerId,
            'item_id' => $itemId,
        ]);
    }

    public function getUnparsedPostCommentsLikesVk($ownerId, $itemId, $commentId = 0)
    {
        return $this->vkSocial->getPostLikes([
            'owner_id' => $ownerId,
            'item_id' => $itemId,
            'comment_id' => $commentId,
        ], VkontakteApi::LIKES_TYPE_COMMENT);
    }

    public function getUnparsedPostCommentsVk($ownerId, $postId)
    {
        return $this->vkSocial->getPostComments([
            'owner_id' => $ownerId,
            'post_id' => $postId,
        ]);
    }

    public function getUnparsedLikesUsers($users)
    {
        return $this->vkSocial->getUserInfo(['user_ids' => $users]);
    }
}
