<?php

namespace app\components\helpers;

use app\models\data\InternetUser;
use Exception;
use PhpOffice\PhpSpreadsheet\Cell\Hyperlink;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Html;
use yii\base\View;
use yii\base\ViewRenderer;
use yii\db\ActiveRecord;
use yii\helpers\Console;
use ZipArchive;

class ExportHelper
{
    const EXPORT_TYPE_CSV = 1;
    const EXPORT_TYPE_XLSX = 2;
    const EXPORT_TYPE_HTML = 3;
    const EXPORT_TYPE_XLS = 4;

    const EXPORT_TYPES = [
        self::EXPORT_TYPE_CSV  => 'CSV',
        self::EXPORT_TYPE_XLS  => 'Excel 1995+',
        //self::EXPORT_TYPE_XLSX => 'Excel 2007+',
        self::EXPORT_TYPE_HTML => 'Html',
    ];
    const EXPORT_FILE_TYPES = [
        self::EXPORT_TYPE_CSV  => 'csv',
        self::EXPORT_TYPE_XLS  => 'xls',
        //self::EXPORT_TYPE_XLSX => 'xlsx',
        self::EXPORT_TYPE_HTML => 'html',
    ];
    const EXPORT_XLS_MAX_ROW_ONE_SPREAD = 50000;
    const EXPORT_XLSX_MAX_ROW_ONE_SPREAD = 50000;
    const EXPORT_HTML_MAX_ROW_ONE_SPREAD = 1000;

    /**
     * @var Spreadsheet
     */
    private static $__spreadSheet;

    /**
     * @param string $filePath
     * @param array $data
     * @param int $i
     * @param string $pass
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public static function saveXlsx(string $filePath, array $data, int $i, $pass = '')
    {
        $archive = new ZipArchive();
        $zipPath = $filePath.'.zip';
        $ret = $archive->open($zipPath,ZipArchive::CREATE);
        if ($ret !== TRUE) {
            Console::stdout('Ошибка '.$ret);
            die();
        }

        $spreadSheet = ExportHelper::generateData($data);

        $filePath = $filePath.'_part' . $i.'.xlsx';
        $writer = new Xlsx($spreadSheet);
        $writer->setPreCalculateFormulas(false);
        $writer->setUseDiskCaching(true, \Yii::$app->getRuntimePath().'/export/');
        $writer->save($filePath);
        $spreadSheet->disconnectWorksheets();
        unset($spreadSheet);

        $filePathData = explode('/',$filePath);
        $fileName = $filePathData[count($filePathData) - 1];
        Console::stdout('Add file '.$fileName.' in archive '.PHP_EOL);
        $pass = empty($pass) ? self::generate_password(8) : $pass;
        $archive->addFile($filePath, $fileName);
        $archive->setPassword($pass);
        $archive->setEncryptionName($fileName, ZipArchive::EM_AES_256, $pass);
        $archive->close();

        unlink($filePath);
        return $pass;
    }

    /**
     * @param string $filePath
     * @param array $data
     * @param int $i
     * @param string $pass
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public static function saveXls(string $filePath, array $data, int $i, $pass = '')
    {
        $archive = new ZipArchive();
        $zipPath = $filePath.'.zip';
        $ret = $archive->open($zipPath,ZipArchive::CREATE);
        if ($ret !== TRUE) {
            Console::stdout('Ошибка '.$ret);
            die();
        }

        $spreadSheet = ExportHelper::generateData($data);
        $filePath = $filePath.'_part' . $i.'.xls';
        $writer = new Xls($spreadSheet);
        $writer->setPreCalculateFormulas(false);
        $writer->setUseDiskCaching(true, \Yii::$app->getRuntimePath().'/export/');
        $writer->setIncludeCharts(false);
        $writer->save($filePath);
        $spreadSheet->disconnectWorksheets();
        unset($spreadSheet);

        $filePathData = explode('/',$filePath);
        $fileName = $filePathData[count($filePathData) - 1];
        Console::stdout('Add file '.$fileName.' in archive '.PHP_EOL);
        $pass = empty($pass) ? self::generate_password(8) : $pass;
        $archive->addFile($filePath, $fileName);
        $archive->setPassword($pass);
        $archive->setEncryptionName($fileName, ZipArchive::EM_AES_256, $pass);
        $archive->close();

        unlink($filePath);
        return $pass;
    }

    /**
     * @param string $filePath
     * @param array $data
     * @param array $pagination
     * @param $context
     * @param string $pass
     * @return string
     * @throws Exception
     */
    public static function saveHtml(string $filePath, array $data, array $pagination, $context, $pass = '')
    {
        $archive = new ZipArchive();
        $zipPath = $filePath.'.zip';
        $ret = $archive->open($zipPath,ZipArchive::CREATE);
        if ($ret !== TRUE) {
            Console::stdout('Ошибка '.$ret);
            die();
        }

        Console::stdout('Generate new sheet number '.$pagination['pageNumber'].PHP_EOL);
        $view = new View();
        $html = $view->render(
            'index',
            [
                'data' => $data,
                'fileName' => basename($filePath),
                'pageNumber' => $pagination['pageNumber'],
                'totalPages' => $pagination['totalPages']
            ],
            $context
        );

        $filePath = $filePath.'_part' . $pagination['pageNumber'] . '.html';

        $fileHandle = fopen($filePath, 'wb+');
        if ($fileHandle === false) {
            throw new Exception("Could not open file $filePath for writing.");
        }
        fputs($fileHandle, $html);
        fclose($fileHandle);

        $fileName = basename($filePath);
        Console::stdout('Add file '.$fileName.' in archive '.PHP_EOL);
        $archive->addFile($filePath, $fileName);
        $pass = empty($pass) ? self::generate_password(8) : $pass;
        $archive->setPassword($pass);
        $archive->setEncryptionName($fileName, ZipArchive::EM_AES_256, $pass);
        $archive->close();

        unlink($filePath);
        return $pass;
    }

    /**
     * @param string $filePath
     * @param array $data
     * @param int $fileCount
     * @param string $pass
     * @return string
     * @throws Exception
     */
    public static function saveCSV(string $filePath,array $data,int $fileCount = 1, $pass = '')
    {
        Console::stdout('Create file number ' . $fileCount . PHP_EOL);

        $archive = new ZipArchive();
        $zipPath = $filePath.'.zip';
        $ret = $archive->open($zipPath,ZipArchive::CREATE);
        if ($ret !== TRUE) {
            Console::stdout('Ошибка '.$ret);
            die();
        }
        // Open file
        $filePath = $filePath.'_part' . $fileCount . '.csv';

        $rownum = 0;
        $totalRowCount = count($data) - 1;
        Console::startProgress(0, $totalRowCount,'Progress row :');
        foreach ($data as $rowId => $row) {
            Console::updateProgress($rownum, $totalRowCount);
            ++$rownum;
            $rowArr = [];

            foreach ($row as $key => &$value) {
                if ($rowId !== 'labels' && !key_exists($key,$data['labels'])) {
                    continue;
                }

                switch ($key) {
                    case 'findfaces':
                        if ($rowId !== 'labels' && !empty($value) &&
                            !empty($value[$row['people_id']]['vk']) &&
                            !empty(array_shift($value[$row['people_id']]['vk'])[0]['confidence'])){
                            $value = array_shift($value[$row['people_id']]['vk'])[0]['confidence'];
                        }

                        break;
                    case 'soc_network_user_id':
                        if ($rowId !== 'labels' && !empty($value)) {
                            switch ($row['soc_network_id']) {
                                case InternetUser::SOC_NETWORK_ID_VK:
                                    $value = 'https://vk.com/id' . $row['soc_network_user_id'];

                                    break;
                                case InternetUser::SOC_NETWORK_ID_OK:
                                    $value = 'https://ok.ru/profile/' . $row['soc_network_user_id'];

                                    break;
                                case InternetUser::SOC_NETWORK_ID_FB:
                                    $value = 'https://www.facebook.com/' . $row['soc_network_user_id'];

                                    break;
                                default:
                                    break;
                            }
                        }

                        break;
                    default:
                        break;
                }

                $rowArr[$key] = '"'.$value.'"';
            }

            file_put_contents($filePath, implode(';',mb_convert_encoding($rowArr,'cp1251')).PHP_EOL, FILE_APPEND);
        }
        Console::endProgress('done.',PHP_EOL);

        $fileName = basename($filePath);
        Console::stdout('Add file '.$fileName.' in archive '.PHP_EOL);
        $pass = empty($pass) ? self::generate_password(8) : $pass;
        $archive->addFile($filePath, $fileName);
        $archive->setPassword($pass);
        $archive->setEncryptionName($fileName, ZipArchive::EM_AES_256, $pass);
        $archive->close();

        unlink($filePath);
        return $pass;
    }

    private static function generate_password($number)
    {
        $arr = array('a','b','c','d','e','f',
                     'g','h','i','j','k','l',
                     'm','n','o','p','r','s',
                     't','u','v','x','y','z',
                     'A','B','C','D','E','F',
                     'G','H','I','J','K','L',
                     'M','N','O','P','R','S',
                     'T','U','V','X','Y','Z',
                     '1','2','3','4','5','6',
                     '7','8','9','0','.',',',
                     '(',')','[',']','!','?',
                     '&','^','%','@','*','$',
                     '<','>','/','|','+','-',
                     '{','}','`','~');
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }

    /**
     * @param array $data
     * @return Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private static function generateData(array $data)
    {
        $spreadSheet = new Spreadsheet();
        $spreadSheet->getDefaultStyle()->getAlignment()
            ->setHorizontal(Alignment::HORIZONTAL_CENTER)->setVertical(Alignment::VERTICAL_CENTER);
        $spreadSheet->setActiveSheetIndex(0);
        $sheet = $spreadSheet->getActiveSheet();

        $rownum = 0;
        $totalRowCount = count($data) - 1;
        Console::startProgress(0, $totalRowCount,'Progress row :');
        foreach ($data as $id => $columns) {
            Console::updateProgress($rownum, $totalRowCount);

            ++$rownum;
            $colnum = 1;
            $sheet->getRowDimension($rownum)->setRowHeight(27);

            foreach ($columns as $key => $value) {
                if ($id !== 'labels' && !key_exists($key,$data['labels'])) {
                    continue;
                }

                $col = chr(64 + $colnum);
                $cellCoordinate = $col.$rownum;

                if ($rownum == 1) {
                    if (strlen($value) < 10) {
                        $width = 15;
                    } else {
                        $width = strlen($value) / 1.5;
                    }

                    $sheet->getColumnDimension($col)->setWidth($width+4);
                }

                switch ($key) {
                    case 'findfaces':
                        if ($id !== 'labels'){
                            if (!empty($value[$columns['people_id']]['vk']) &&
                                !empty(array_shift($value[$columns['people_id']]['vk'])[0]['confidence'])) {
                                $sheet->setCellValue(
                                    $cellCoordinate,
                                    array_shift($value[$columns['people_id']]['vk'])[0]['confidence']
                                );
                            } else {
                                $sheet->setCellValue($cellCoordinate, 'Здесь должно быть значение');
                            }
                        } else {
                            $sheet->setCellValue($cellCoordinate, $value);
                        }

                        break;
                    case 'avatar':
                        if ($id !== 'labels'){
                            if (!empty($value) && $value !== '/resources/admin/img/noimg.png') {
//                                Console::stdout('Not empty avatar value: '.$value.PHP_EOL);
                                $sheet->setCellValue($cellCoordinate, 'ссылка');
                                $sheet->getCell($cellCoordinate)
                                    ->setHyperlink((new Hyperlink)->setUrl(htmlspecialchars($value)));
                            } else {
                                $sheet->setCellValue($cellCoordinate, 'нет изображения');
                            }
                        } else {
                            $sheet->setCellValue($cellCoordinate, $value);
                        }

                        break;
                    case 'soc_network_user_id':
                        if ($id !== 'labels'){
                            if (!empty($columns['soc_network_id']) && !empty($columns['soc_network_user_id'])) {
                                switch ($columns['soc_network_id']) {
                                    case InternetUser::SOC_NETWORK_ID_VK:
                                        $link = 'https://vk.com/id' . $columns['soc_network_user_id'];

                                        break;
                                    case InternetUser::SOC_NETWORK_ID_OK:
                                        $link = 'https://ok.ru/profile/' . $columns['soc_network_user_id'];

                                        break;
                                    case InternetUser::SOC_NETWORK_ID_FB:
                                        $link = 'https://www.facebook.com/' . $columns['soc_network_user_id'];

                                        break;
                                    default:
                                        break;
                                }
                                $sheet->setCellValue($cellCoordinate, 'ссылка');
                                $sheet->getCell($cellCoordinate)
                                    ->setHyperlink((new Hyperlink)->setUrl(htmlspecialchars($link)));
                            } else {
                                $sheet->setCellValue($cellCoordinate, 'нет ссылки');
                            }
                        } else {
                            $sheet->setCellValue($cellCoordinate, $value);
                        }

                        break;
                    case 'preview_link':
                        if (!empty($value) && $id !== 'labels') {
                            $sheet->setCellValue($cellCoordinate, 'ссылка');
                            $link = '/tasks/'.$columns['title_lat'].'/thumbs/'.$columns['preview_link'];
                            $sheet->getCell($cellCoordinate)
                                ->setHyperlink((new Hyperlink)->setUrl(htmlspecialchars($link)));
                        } else {
                            $sheet->setCellValue($cellCoordinate, $value);
                        }

                        break;
                    case 'groups_count':
                    case 'tags_count':
                        $sheet->setCellValue($cellCoordinate, $value);
//                        Console::stdout('Counters:' . $key . ' = ' . $value . PHP_EOL);

                        break;
                    default:
                        $sheet->setCellValue($cellCoordinate, $value);

                        break;
                }

                $sheet->getStyle($cellCoordinate)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $colnum++;
            }
        }
        Console::endProgress('done.',PHP_EOL);

        return $spreadSheet;
    }
}