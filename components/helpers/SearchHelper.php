<?php

namespace app\components\helpers;

/**
 * Class SearchHelper
 * @package app\components\helpers
 */
class SearchHelper
{
    /**
     * @param string $sort
     *
     * @return array ['column' => 'asc|desc']
     */
    public static function parseSort(string $sort): array
    {
        $field = $sort;
        if (strncmp($sort, '-', 1) === 0) {
            $field = substr($sort, 1, strlen($sort));
        }

        if (!$field || $field === 'null') {
            return [];
        }
        return [$field => (strpos($sort, '-') === 0) ? SORT_ASC : SORT_DESC];
    }
}
