<?php

use yii\helpers\ArrayHelper;

$paramsLocal = file_exists(__DIR__ . '/params-local.php')
    ? require __DIR__ . '/params-local.php'
    : [];

$paramsRealtime = [];

return ArrayHelper::merge([
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'loading_preview' => '/resources/admin/img/loader.svg',
    'loader' => '/resources/admin/img/loader.svg',
    'ageGraphArray' => [
        "меньше 18" => [0,18],
        "18-24" => [18,25],
        "25-34" => [25,35],
        "35-44" => [35,45],
        "45-54" => [45,55],
        "55+" => [55,200],
    ],
    'arCityForFilter' => [
        'all' => 'Все города',
        '0,3' => 'Москва и "не указано"'
    ]
], $paramsLocal);
